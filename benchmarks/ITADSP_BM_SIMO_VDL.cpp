/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * Benchmark SIMO VDL I/O
 *
 */

#include <ITAAudiofileWriter.h>
#include <ITAConfigUtils.h>
#include <ITAFileDataSource.h>
#include <ITASIMOVariableDelayLine.h>
#include <ITASampleBuffer.h>
#include <ITASampleFrame.h>
#include <ITAStopWatch.h>
#include <ITAStreamFunctionGenerator.h>
#include <ITAStreamInfo.h>
#include <ITAStringUtils.h>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <vector>

using namespace std;
const std::string sBMFilePath = "ITADSP_BM_SIMO_VDL.ini";

struct ParameterSet
{
	unsigned int iBlockLength;
	double dSampleRate;
	float fMaxReservedDelaySamples;
	float fInitialDelaySamples;
	int iNumCursors;
	string sWAVOutFilePath;
	EVDLAlgorithm eAlgorithm;
	int iInterpolationType;
	int iNumFrames;
	float fDelayIncrementSample;

	inline void SetSomeDefaults( )
	{
		iNumCursors = 256;
#ifdef DEBUG
		iNumFrames = 100;
#else
		iNumFrames = 1e3;
#endif
	};

	inline void SetSomeDependentParameters( )
	{
		dSampleRate              = 44.1e3;
		fMaxReservedDelaySamples = 100 * iBlockLength;
		fInitialDelaySamples     = 10 * 2 * iBlockLength - 1;
	};

	inline void WriteToINIFile( const std::string& sSection ) const
	{
		INIFileWriteInt( "BlockLength", iBlockLength );
		INIFileWriteDouble( "SampleRate", dSampleRate );
		INIFileWriteDouble( "MaxReservedDleaySamples", fMaxReservedDelaySamples );
		INIFileWriteDouble( "InitialDelaySamples", fInitialDelaySamples );
		INIFileWriteDouble( "DelayIncrementSample", fDelayIncrementSample );
		INIFileWriteInt( "NumCursors", iNumCursors );
		INIFileWriteInt( "NumFrames", iNumFrames );
		INIFileWriteString( "WAVOutFilePath", sWAVOutFilePath );
		INIFileWriteString( "InterpolationType", ( iInterpolationType == ITABase::InterpolationFunctions::CUBIC_SPLINE ) ? "cubicspline" : "linear" );
		INIFileWriteDouble( "BlockTimeSeconds", iBlockLength / dSampleRate );
		INIFileWriteString( "BlockTime", timeToString( iBlockLength / dSampleRate ) );
	};
};

void run_benchmark( const ParameterSet&, const std::string& );

int main( int, char** )
{
	cout << "Starting single-input multiple-output variable delay line benchmark" << endl;

	INIFileUseFile( sBMFilePath );

	ParameterSet pm;
	pm.SetSomeDefaults( );

	for( auto BL: { 32, 64, 128, 256 } )
	{
		for( auto SI: { 1, 2, 4, 8, 16, 32, 64, 128 } )
		{
			for( auto IT: { EVDLAlgorithm::CUBIC_SPLINE_INTERPOLATION, EVDLAlgorithm::LINEAR_INTERPOLATION } )
			{
				if( BL < SI * 2 )
					continue;

				string sInterp = ( IT == EVDLAlgorithm::CUBIC_SPLINE_INTERPOLATION ) ? "cubicspline" : "linear";

				// Streach
				stringstream ss;
				ss << "ITADSP_BM_SIMO_VDL_L" << BL << "_stretch_" << SI << "_" << sInterp;
				string sBMID             = ss.str( );
				pm.iBlockLength          = BL;
				pm.fDelayIncrementSample = SI;
				pm.eAlgorithm            = IT;
				pm.iInterpolationType    = (int)IT;
				pm.sWAVOutFilePath       = sBMID + ".wav";
				pm.SetSomeDependentParameters( );
				cout << "\tStarting " << sBMID << " ";
				run_benchmark( pm, sBMID );
				cout << "\tdone." << endl;

				// Squeeze
				ss = stringstream( );
				ss << "ITADSP_BM_SIMO_VDL_L" << BL << "_squeeze_" << SI << "_" << sInterp;
				sBMID                    = ss.str( );
				pm.iBlockLength          = BL;
				pm.fDelayIncrementSample = ( -1.0f ) * SI;
				pm.eAlgorithm            = IT;
				pm.iInterpolationType    = (int)IT;
				pm.sWAVOutFilePath       = sBMID + ".wav";
				pm.SetSomeDependentParameters( );
				cout << "\tStarting " << sBMID << " ";
				run_benchmark( pm, sBMID );
				cout << "\tdone." << endl;
			}
		}
	}

	cout << "All done." << endl;

	return 255;
}

void run_benchmark( const ParameterSet& pm, const std::string& sName )
{
	INIFileUseSection( sName );
	pm.WriteToINIFile( sName );

	ITAStreamFunctionGenerator sinesignal( 1, pm.dSampleRate, pm.iBlockLength, ITAStreamFunctionGenerator::SINE, 500.0f, 0.9f, true );
	ITADatasource* pIntputStream        = &sinesignal;
	CITASIMOVariableDelayLine* pSIMOVDL = new CITASIMOVariableDelayLine( pm.dSampleRate, pm.iBlockLength, pm.fMaxReservedDelaySamples, pm.eAlgorithm );

	std::vector<int> viCursors;
	for( int i = 0; i < pm.iNumCursors; i++ )
	{
		viCursors.push_back( pSIMOVDL->AddCursor( ) );
		pSIMOVDL->SetDelaySamples( viCursors[i], float( pm.iBlockLength * ( 50 + i ) ) );
	}

	ITAAudiofileProperties props_out;
	props_out.iChannels            = 1;
	props_out.dSampleRate          = pm.dSampleRate;
	props_out.eQuantization        = ITAQuantization::ITA_FLOAT;
	props_out.eDomain              = ITADomain::ITA_TIME_DOMAIN;
	props_out.iLength              = pm.iNumFrames * (unsigned int)( pm.iBlockLength );
	props_out.iChannels            = 1;
	ITAAudiofileWriter* writer_out = ITAAudiofileWriter::create( pm.sWAVOutFilePath, props_out );

	ITAStreamInfo oState;

	ITASampleBuffer* psbInput = new ITASampleBuffer( pm.iBlockLength, true );
	ITASampleFrame* psfOutput = new ITASampleFrame( pSIMOVDL->GetNumCursors( ), pm.iBlockLength, true );
	ITASampleFrame sfTemp( 1, pm.iBlockLength, true );

	ITAStopWatch swBenchmark;

	unsigned int n = 0;
	while( n < pm.iNumFrames )
	{
		// Add new samples
		psbInput->write( pIntputStream->GetBlockPointer( 0, &oState ), pm.iBlockLength );

		pSIMOVDL->WriteBlock( psbInput );

		for( size_t i = 0; i < viCursors.size( ); i++ )
		{
			const float fCurrentDelay = pSIMOVDL->GetCurrentDelaySamples( viCursors[i] );
			pSIMOVDL->SetDelaySamples( viCursors[i], fCurrentDelay + pm.fDelayIncrementSample ); // Keep Doppler ratio constant
		}

		swBenchmark.start( );
		pSIMOVDL->ReadBlockAndIncrement( psfOutput );
		swBenchmark.stop( );
		INIFileWriteDouble( "ComputationMean", swBenchmark.mean( ) );
		INIFileWriteDouble( "ComputationStdDev", swBenchmark.std_deviation( ) );
		INIFileWriteDouble( "ComputationMinimum", swBenchmark.minimum( ) );
		INIFileWriteDouble( "ComputationMaximum", swBenchmark.maximum( ) );

		sfTemp.zero( );
		for( int i = 0; i < psfOutput->GetNumChannels( ); i++ )
			sfTemp[0].add_buf( ( *psfOutput )[i] );

		writer_out->write( &sfTemp, sfTemp.GetLength( ) );
		n++;

		pIntputStream->IncrementBlockPointer( );

		if( n % ( pm.iNumFrames / 10 ) == 0 )
			cout << ".";
	}

	delete writer_out;

	delete psbInput;
	delete psfOutput;
}
