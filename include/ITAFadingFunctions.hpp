/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2023
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_ITA_FADING_FUNCTIONS
#define IW_ITA_FADING_FUNCTIONS

#include <ITAConstants.h>

namespace ITA
{
	namespace DSP
	{
		namespace FadingFunction
		{
			///
			/// \brief Fading functions for crossfading and fading in/out audio signals
			///
			enum class FadingFunction
			{
				LINEAR = 0,    ///< Linear fading
				COSINE,        ///< Cosine fading
				COSINE_SQUARE, ///< Cosine square fading
				QUADRATIC,     ///< Quadratic fading
				CUBIC,         ///< Cubic fading
				SQUARE_ROOT,   ///< Square root fading
				EXPONENTIAL,   ///< Exponential fading
				LOGARITHMIC    ///< Logarithmic fading
			};

			///
			/// \brief Enum for determining the direction of the fading process
			///
			enum class FadingDirection
			{
				FADE_IN,
				FADE_OUT
			};

			namespace detail
			{
				inline float linear_fading( float fading_fraction, FadingDirection sign );
				inline float cosine_fading( float fading_fraction, FadingDirection sign );
				inline float cosine_square_fading( float fading_fraction, FadingDirection sign );
				inline float quadratic_fading( float fading_fraction, FadingDirection sign );
				inline float cubic_fading( float fading_fraction, FadingDirection sign );
				inline float square_root_fading( float fading_fraction, FadingDirection sign );
				inline float exponential_fading( float fading_fraction, FadingDirection sign, float exp_steepness = 4 );
				inline float logarithmic_fading( float fading_fraction, FadingDirection sign, float log_steepness = 10 );
			} // namespace detail

			///
			/// \brief Fading function for fading in/out audio signals
			///
			/// This can also be used for crossfading audio signals.
			/// The fading_fraction must be in the range [0, 1].
			/// If the fading_fraction is outside this range, the function will return 0.
			/// \param fading_fraction the factor for the fading process (0 = no fading, 1 = full fading)
			/// \param sign the direction of the fading process
			/// \param func the fading function to be used
			/// \return  float the fading factor for the given fading function
			///
			inline float fading_func( float fading_fraction, FadingDirection sign, FadingFunction func = FadingFunction::LINEAR )
			{
				if( fading_fraction < 0 || fading_fraction > 1 )
				{
					return 0;
				}

				switch( func )
				{
					case FadingFunction::LINEAR:
						return detail::linear_fading( fading_fraction, sign );
					case FadingFunction::COSINE:
						return detail::cosine_fading( fading_fraction, sign );
					case FadingFunction::COSINE_SQUARE:
						return detail::cosine_square_fading( fading_fraction, sign );
					case FadingFunction::QUADRATIC:
						return detail::quadratic_fading( fading_fraction, sign );
					case FadingFunction::CUBIC:
						return detail::cubic_fading( fading_fraction, sign );
					case FadingFunction::SQUARE_ROOT:
						return detail::square_root_fading( fading_fraction, sign );
					case FadingFunction::EXPONENTIAL:
						return detail::exponential_fading( fading_fraction, sign );
					case FadingFunction::LOGARITHMIC:
						return detail::logarithmic_fading( fading_fraction, sign );
				}

				return 0;
			}

			///
			/// \brief Concrete implementations of the fading functions
			///
			namespace detail
			{
				float linear_fading( float fading_fraction, FadingDirection sign )
				{
					return sign == FadingDirection::FADE_IN ? fading_fraction : 1.f - fading_fraction;
				}

				float cosine_fading( float fading_fraction, FadingDirection sign )
				{
					// This implementations requires the factor to be clamped using max to avoid negative values.
					// The float implementation of cosf can return negative values due to floating point inaccuracies.
					if( sign == FadingDirection::FADE_IN )
					{
						return std::max( std::cosf( ( 1.f - fading_fraction ) * ITAConstants::HALF_PI_F ), 0.f );
					}
					else
					{
						return std::max( std::cosf( fading_fraction * ITAConstants::HALF_PI_F ), 0.f );
					}
				}

				float cosine_square_fading( float fading_fraction, FadingDirection sign )
				{
					// This implementations requires the factor to be clamped using max to avoid negative values.
					// The float implementation of cosf can return negative values due to floating point inaccuracies.
					const auto tmp_value = std::max( std::cosf( fading_fraction * ITAConstants::HALF_PI_F ), 0.f );
					if( sign == FadingDirection::FADE_IN )
					{
						return std::max( 1.f - ( tmp_value * tmp_value ), 0.f );
					}
					else
					{
						return std::max( tmp_value * tmp_value, 0.f );
					}
				}

				float quadratic_fading( float fading_fraction, FadingDirection sign )
				{
					if( sign == FadingDirection::FADE_OUT )
					{
						fading_fraction = 1.f - fading_fraction;
					}
					return fading_fraction * fading_fraction;
				}

				float cubic_fading( float fading_fraction, FadingDirection sign )
				{
					if( sign == FadingDirection::FADE_OUT )
					{
						fading_fraction = 1.f - fading_fraction;
					}
					return fading_fraction * fading_fraction * fading_fraction;
				}

				float square_root_fading( float fading_fraction, FadingDirection sign )
				{
					if( sign == FadingDirection::FADE_IN )
					{
						return std::sqrtf( fading_fraction );
					}
					else
					{
						return std::sqrtf( 1 - fading_fraction );
					}
				}

				float exponential_fading( float fading_fraction, FadingDirection sign, float exp_steepness )
				{
					auto exp_func = [exp_steepness]( float x )
					{
						return ( std::expf( exp_steepness * x ) - 1 ) / ( std::expf( exp_steepness ) - 1 );
					};

					if( sign == FadingDirection::FADE_IN )
					{
						return exp_func( fading_fraction );
					}
					else
					{
						return exp_func( 1 - fading_fraction );
					}
				}

				float logarithmic_fading( float fading_fraction, FadingDirection sign, float log_steepness )
				{
					auto log_func = [log_steepness]( float x )
					{
						return std::logf( log_steepness * x + 1 ) / std::logf( log_steepness + 1 );
					};

					if( sign == FadingDirection::FADE_IN )
					{
						return log_func( fading_fraction );
					}
					else
					{
						return log_func( 1 - fading_fraction );
					}
				}
			} // namespace detail

		} // namespace FadingFunction
	} // namespace DSP
} // namespace ITA

#endif // IW_ITA_STATEFUL_SMOOTHED_GAIN
