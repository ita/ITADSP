/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_ITA_VARIABLE_DELAY_LINE_CONFIG
#define IW_ITA_VARIABLE_DELAY_LINE_CONFIG

#include <ITADSPDefinitions.h>

/// Algorithm used when propagation delay is changed
enum class EVDLAlgorithm
{
	SWITCH = 0,                  //!< Hard switch of read cursor
	CROSSFADE,                   //!< Crossfading in time domain using cosine-square functions
	LINEAR_INTERPOLATION,        //!< Stretching and compression in time-domain using linear interpolation
	WINDOWED_SINC_INTERPOLATION, //!< Stretching and compression in time-domain using windowed sinc interpolation
	CUBIC_SPLINE_INTERPOLATION,  //!< Stretching and compression in time-domain using cubic spline interpolation
};

//// Config for ITAVariableDelayLine and ITASIMOVariableDelayLine
struct ITA_DSP_API CITAVDLConfig
{	
	double dSampleRate;
	int iBlockSize;
	float fMaxDelaySamples;
	EVDLAlgorithm eAlgorithm;

	CITAVDLConfig( double dSampleRate_, int iBlockSize_, double dMaxDelaySeconds_ = 3.0, EVDLAlgorithm eSwitchingAlgorithm_ = EVDLAlgorithm::CUBIC_SPLINE_INTERPOLATION )
	    : dSampleRate( dSampleRate_ )
	    , iBlockSize( iBlockSize_ )
	    , fMaxDelaySamples( float( dMaxDelaySeconds_* dSampleRate_ ) )
	    , eAlgorithm( eSwitchingAlgorithm_ ) { };
};

#endif // IW_ITA_VARIABLE_DELAY_LINE_CONFIG
