/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef IW_ITA_STATEFUL_SMOOTHED_GAIN
#define IW_ITA_STATEFUL_SMOOTHED_GAIN

#include <ITADSPDefinitions.h>
#include <ITASampleBuffer.h>
#include "ITAFadingFunctions.hpp"

namespace ITA
{
	namespace DSP
	{
		/// @brief This DSP element applies a gain factor in a smoothed way to avoid sudden volume jumps
		///
		/// This DSP element stores the gain applied to the last audio block. When applying a different gain for the next audio block, the gain factor is interpolated
		/// between samples (over the duration of one audio block) in order to avoid sudden volume jumps.
		///
		/// This class also implements a "smart" cosine fading. This means, that the gain factor interpolation is dependent on the gain factors.
		/// - If the new gain factor or the current gain factor is smaller than 120dBFS, the gain factor is interpolated using a raised cosine function.
		/// - If neither of the gain factors is smaller than 120dBFS, the gain factor is interpolated using a cosine function.
		class ITA_DSP_API StatefulSmoothedGain
		{
		public:
			enum class FadingMode
			{
				LINEAR,        ///< Linear fading
				COSINE_SQUARE, ///< Cosine square fading
				SMART_COSINE   ///< "Smart" cosine fading using both cosine and cosine square fading
			};

			/// @brief Constructor for StatefulSmoothedGain
			/// @param initialGain Initial gain factor
			/// @param fadingMode Fading mode for gain interpolation
			StatefulSmoothedGain( double initialGain = 0.0, FadingMode fadingMode = FadingMode::COSINE_SQUARE );

			/// @brief Sets the gain factor for the next processing step
			/// @param gain
			/// @param forceInstant Directly changes to new gain without smoothing
			void SetGain( double gain, bool forceInstant = false );

			/// @brief Applies a smoothed / interpolated gain to given audio block and writes result to new block
			/// @param inputBlock Audio block with input samples
			/// @param outputBlock Audio block with output samples, can be the same as inputBlock
			void Process( const ITASampleBuffer& inputBlock, ITASampleBuffer& outputBlock );

			/// @brief Applies a smoothed / interpolated gain to given audio block
			/// @param audioBlock Audio block with input samples (will be overwritten)
			void Process( ITASampleBuffer& audioBlock );

		private:
			/// @brief Applies new gain factor to all samples of given audio block
			void ApplyConstantGain( ITASampleBuffer& audioBlock );

			/// @brief Applies interpolated gain to audio block.
			void ApplyInterpolatedGain( ITASampleBuffer& audioBlock );

			/// @brief Gain factor which is currently active
			double m_currentGain;
			/// @brief Gain factor which will be active after processing the next audio block
			double m_newGain;

			/// @brief If true, use linear fading, otherwise "smart" cosine fading
			FadingMode m_fadingMode;
		};
	} // namespace DSP
} // namespace ITA

#endif // IW_ITA_STATEFUL_SMOOTHED_GAIN
