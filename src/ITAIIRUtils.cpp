﻿#include <ITAIIRUtils.h>
#include <ITAException.h>
#include <fstream>

#ifdef WITH_JSON_SUPPORT
#	include <nlohmann/json.hpp>
#endif

void ITADSP::ExportIIRCoefficientsToJSON( const std::string& sJSONFilePath, const CFilterCoefficients& oCoefficients )
{
#ifdef WITH_JSON_SUPPORT
	// TODO This has to be tested and checked, if the format is correct!
	nlohmann::json jnCoefficients;
	jnCoefficients["order"] = oCoefficients.uiOrder;

	nlohmann::json jnNumerator;

	for( int i = 0; i < oCoefficients.vfNumerator.size( ); i++ )
		jnNumerator["b" + std::to_string( i )] = oCoefficients.vfNumerator[i];

	jnCoefficients["numerator"] = jnNumerator;

	nlohmann::json jnDenominator;

	for( int i = 0; i < oCoefficients.vfDenominator.size( ); i++ )
		jnDenominator["a" + std::to_string( i + 1 )], oCoefficients.vfDenominator[i];

	jnCoefficients["denominator"] = jnDenominator;

	jnCoefficients["design_algorithm"] = oCoefficients.iDesignAlgorithm;

	jnCoefficients["is_ARMA"] = oCoefficients.bIsARMA;


	std::ofstream fsOut( sJSONFilePath );
	fsOut << jnCoefficients.dump( );
	fsOut.close( );

#else

	ITA_EXCEPT1( NOT_IMPLEMENTED, "Export function not implemented or no JSON support activated for ITA DSP library" );

#endif
}
