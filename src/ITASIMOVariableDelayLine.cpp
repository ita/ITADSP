#include <ITAException.h>
#include <ITAFastMath.h>
#include <ITAInterpolation.h>
#include <ITANumericUtils.h>
#include <ITASIMOVariableDelayLine.h>
#include <ITASampleBuffer.h>
#include <ITASampleFrame.h>
#include <cassert>
#include <cmath>
#include <spline.h>

// Interpolation resampling factor range (otherwise crossfading is used)
#define MIN_RESAMPLING_FACTOR 0.0f // [0, inf) ... aber sinnvoll z.B. 0, 0.1, 0.5 (< 1)
#define MAX_RESAMPLING_FACTOR 2.5f // [0, inf) ... aber sinnvoll z.B. 1, 1.5, 2, 3 (> 1)


CITASIMOVariableDelayLine::CITASIMOVariableDelayLine( const double dSamplerate, const int iBlocklength, const float fReservedMaxDelaySamples,
                                                      const EVDLAlgorithm eAlgorithm )
    : CITASIMOVariableDelayLineBase( (int)ceil( fReservedMaxDelaySamples ) )
    , m_dSampleRate( dSamplerate )
    , m_iBlockLength( iBlocklength )
    , m_eSwitchingAlgorithm( eAlgorithm )
    , m_pInterpolationRoutine( nullptr )
    , m_bBenchmark( false )
{
	assert( dSamplerate > 0 );
	assert( iBlocklength > 0 );

	m_iFadeLength = std::min( m_iBlockLength, 32 );

	if( m_eSwitchingAlgorithm == EVDLAlgorithm::SWITCH )
	{
		ITA_EXCEPT_INVALID_PARAMETER( "SWITCH is not a valid algorithm for SIMO VDL." );
	}
	else if( m_eSwitchingAlgorithm == EVDLAlgorithm::CROSSFADE )
		m_pInterpolationRoutine = nullptr;
	else if( m_eSwitchingAlgorithm == EVDLAlgorithm::LINEAR_INTERPOLATION )
		m_pInterpolationRoutine = std::make_shared<CITASampleLinearInterpolation>( );
	else if( m_eSwitchingAlgorithm == EVDLAlgorithm::CUBIC_SPLINE_INTERPOLATION )
		m_pInterpolationRoutine = std::make_shared<CITASampleCubicSplineInterpolation>( );
	else if( m_eSwitchingAlgorithm == EVDLAlgorithm::WINDOWED_SINC_INTERPOLATION )
		m_pInterpolationRoutine = std::make_shared<CITASampleWindowedSincInterpolation>( );
	else
		ITA_EXCEPT_INVALID_PARAMETER( "Unknown switching algorithm for SIMO VDL." );
}

CITASIMOVariableDelayLine::CITASIMOVariableDelayLine( const CITAVDLConfig& oConfig )
    : CITASIMOVariableDelayLine( oConfig.dSampleRate, oConfig.iBlockSize, oConfig.fMaxDelaySamples, oConfig.eAlgorithm )
{
}


EVDLAlgorithm CITASIMOVariableDelayLine::GetAlgorithm( ) const
{
	return m_eSwitchingAlgorithm;
}

void CITASIMOVariableDelayLine::SetAlgorithm( EVDLAlgorithm iAlgorithm )
{
	m_eSwitchingAlgorithm = iAlgorithm;
}

float CITASIMOVariableDelayLine::GetReservedMaximumDelayTime( ) const
{
	return GetReservedMaximumDelaySamples( ) / (float)m_dSampleRate;
}

int CITASIMOVariableDelayLine::GetMinimumDelaySamples( ) const
{
	if( !m_pInterpolationRoutine )
		return 0;

	int iLeft, iRight;
	m_pInterpolationRoutine->GetOverlapSamples( iLeft, iRight );

	return ( iLeft + iRight );
}

float CITASIMOVariableDelayLine::GetMinimumDelayTime( ) const
{
	return GetMinimumDelaySamples( ) / (float)m_dSampleRate;
}

void CITASIMOVariableDelayLine::ReserveMaximumDelayTime( const float fMaxDelaySecs )
{
	ReserveMaximumDelaySamples( int( (double)fMaxDelaySecs * m_dSampleRate ) );
}

void CITASIMOVariableDelayLine::SetDelayTime( const int iCursorID, const float fDelaySeconds )
{
	SetDelaySamples( iCursorID, (int)round( (double)fDelaySeconds * m_dSampleRate ) );
}

void CITASIMOVariableDelayLine::WriteBlock( const ITASampleBuffer* psbInput )
{
	CITASIMOVariableDelayLineBase::Write( m_iBlockLength, psbInput->GetData( ) );
}

void CITASIMOVariableDelayLine::ReadBlockAndIncrement( ITASampleFrame* psfOutput )
{
	assert( psfOutput );
	const std::vector<int> viIDs = GetCursorIDs( );

	if( psfOutput->GetNumChannels( ) != GetNumCursors( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Mismatching channel number, provide a sample frame with same number of channels as cursors available" );

	for( int i = 0; i < GetNumCursors( ); i++ )
		ReadBlock( viIDs[i], &( *psfOutput )[i] );

	Increment( );
}

void CITASIMOVariableDelayLine::ReadBlock( const int iCursorID, ITASampleBuffer* psbOutput )
{
	if( !CursorExists( iCursorID ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Could not find requested read cursor" );

	m_swProcess.start( );

	EVDLAlgorithm eAlgorithmLocalCopy = m_eSwitchingAlgorithm;


	int iCurrentDelayLocalCopy = GetCurrentDelaySamples( iCursorID );
	int iNewDelayLocalCopy     = GetNewDelaySamples( iCursorID );

	// Use a fast-forward copy in case of no new delay (static situation)
	if( iNewDelayLocalCopy == iCurrentDelayLocalCopy )
	{
		// No change of delay, simply copy samples from read cursor [ NO INTERPOLATION REQUIRED ]s
		CITASIMOVariableDelayLineBase::Read( iCursorID, m_iBlockLength, psbOutput->GetData( ) );
		return;
	}


	// --= �nderung der Verz�gerung =--

	// Zerlegen in Ganzzahl und Kommazahl
	int iCurrentIntDelay    = (int)ceil( iCurrentDelayLocalCopy );
	float fCurrentFracDelay = (float)iCurrentIntDelay - iCurrentDelayLocalCopy;
	assert( fCurrentFracDelay >= 0.0f ); // Subsample darf nicht negativ sein
	int iNewIntDelay    = (int)ceil( iNewDelayLocalCopy );
	float fNewFracDelay = (float)iNewIntDelay - iNewDelayLocalCopy;
	assert( fNewFracDelay >= 0.0f ); // Subsample darf nicht negativ sein
	int iDeltaDelay = iNewIntDelay - iCurrentIntDelay;

	// Falls Interpolation gew�nscht ist, Grenzen pr�fen
	if( ( eAlgorithmLocalCopy == EVDLAlgorithm::LINEAR_INTERPOLATION ) || ( eAlgorithmLocalCopy == EVDLAlgorithm::CUBIC_SPLINE_INTERPOLATION ) ||
	    ( eAlgorithmLocalCopy == EVDLAlgorithm::WINDOWED_SINC_INTERPOLATION ) )
	{
		/*
		 * Folgendes Problem kann bei der Abstandsverkleinerung auftreten:
		 * Ist die Abstandsverkleinerung zu schnell, werden Frequenzen so stark gestaucht,
		 * dass sie �ber die Nyquist-Grenze der Soundkarte hinaus wandern (22.05 kHz) und
		 * bei der Abtastung zu Alias-Fehlern f�hren. Es muss hierf�r das Eingangssignal
		 * entsprechend tiefpass gefiltert werden. Eine synchrone Abtastratenerh�hung
		 * kann auch sinnvoll sein. Diese muss nach der Interpolation erneut tp-gefiltert
		 * und entsprechend runtergesetzt werden.
		 *
		 * Folgendes Problem kann bei der Abstandsvergr��erung auftreten:
		 * Ist die Abstandsvergr��erung sehr schnell, werden im Grenzfall lediglich
		 * einige St�tzsamples zur Interpolation vieler Ausgangssamples genutzt,
		 * aber ein systembedingte Grenze existiert bis zur Schallgeschwindigkeit
		 * nicht.
		 *
		 * Kann die Interpolation die entsprechenden Frequenzen nicht nachbilden,
		 * treten Verzerrungen auf, bei der Linearinterpolation nicht-lineare Ver-
		 * zerrungen. Diese Fehler Verst�rken sich zunehmend.
		 *
		 */

		// Resamplingfaktor auf dem Eingangsstream bezogen auf eine Blockl�nge berechnen
		float fResamplingFactor = 1 - iDeltaDelay / (float)m_iBlockLength;

		// Wenn Voraussetzungen verletzt werden, f�r diesen Bearbeitungsschritt auf weiches Umschalten wechseln
		if( ( fResamplingFactor <= MIN_RESAMPLING_FACTOR ) || ( fResamplingFactor > MAX_RESAMPLING_FACTOR ) )
		{
			eAlgorithmLocalCopy = EVDLAlgorithm::CROSSFADE;
		}
	}


	// --= Lesek�pfe =--

	// VDL-Puffergr��e wird nur addiert, damit
	int iReadCursorCurrent = ( m_iWriteCursor - iCurrentIntDelay + m_iVDLBufferSize ) % m_iVDLBufferSize;
	int iReadCursorNew     = ( m_iWriteCursor - iNewIntDelay + m_iVDLBufferSize ) % m_iVDLBufferSize;
	assert( iReadCursorCurrent >= 0 ); // Cursor darf nicht negativ sein
	assert( iReadCursorNew >= 0 );     // Cursor darf nicht negativ sein


	// --= Umschaltverfahren =--

	int iSize;
	int iLeft, iRight; // �berlappung an den Grenzen
	switch( eAlgorithmLocalCopy )
	{
		// o Umschalten mittels Kreuzblende
		case EVDLAlgorithm::CROSSFADE:
		{
			// Kreuzblende mittels tempor�rem Puffer
			assert( m_iFadeLength <= m_psbTemp->length( ) ); // Zu gro�e Blende f�r diesen Puffer
			m_psbTemp->cyclic_write( m_psbVDLBuffer, m_iFadeLength, iReadCursorCurrent, 0 );
			psbOutput->cyclic_write( m_psbVDLBuffer, m_iBlockLength, iReadCursorNew, 0 );

			psbOutput->Crossfade( m_psbTemp, 0, m_iFadeLength, ITABase::CrossfadeDirection::FROM_SOURCE, ITABase::FadingFunction::COSINE_SQUARE );

			break;
		}

		// o Umschalten durch Stauchen oder Strecken: Lineare Interpolation, Kubische Spline-Interpolation oder gefensterte Sinc-Interpolation
		default:
		{
			/* Zur Erkl�rung:

			    Verringerung der Verz�gerung => Samples stauchen
			    (Size > Blocklength), "Zeit" muss eingeholt werden
			    Resamplingfaktor > 1

			    Vergr��ern der Verz�gerung => Samples strecken
			    (Size < Blocklength), "Zeit" muss gedehnt werden
			    Resamplingfaktor < 1
			    */

			// �berlappung an den Grenzen holen
			m_pInterpolationRoutine->GetOverlapSamples( iLeft, iRight );

			// Neuer Abstand
			if( iNewIntDelay < iLeft )
				iSize = m_iBlockLength; // Kausalit�t erzwingen, wenn der Abstand kleiner als linker �berlappbereich ist
			else
				iSize = iCurrentIntDelay - iNewIntDelay + m_iBlockLength;

			// Sicherstellen, dass Daten des rechten �berlappungsbereichs vorhanden sind
			// (VDL Puffer sollte immer mindestens eine Blockl�nge gr��er sein als die
			//  aktuelle Verz�gerung)
			assert( iLeft < m_iBlockLength );                   // �berlapp links bereits gr��er als Blockl�nge
			assert( iRight < m_iBlockLength );                  // �berlapp rechts bereits gr��er als Blockl�nge
			assert( m_iVDLBufferSize > iNewIntDelay + iRight ); // Neue Verz�gerung und rechter �berlapp gr��er als VDL L�nge

			// "Size" Daten plus �berlappung aus dem Hauptpuffer vom momentanen Lesekopf an
			// den Anfang des Tempor�rpuffers kopieren
			assert( iLeft + iSize + iRight <= m_psbTemp->length( ) ); // iTempBufferBlockLength zu klein
			m_psbTemp->cyclic_write( m_psbVDLBuffer, iLeft + iSize + iRight, iReadCursorCurrent - iLeft, 0 );

			// Interpolieren: Tempor�rpuffer (Gr��e Size+Offset) --> Output (Gr��e Blockl�nge)
			// Input Offset = iLeft -> effektive Inputl�nge = iSize
			// But: iInputLength is the length of sample to be interpolated from! (in & out length determines resampling factor)
			// iRight wird implizit durch die Interpolationsroutine verwendet, d.h.
			// bei iInputLength von iLeft+iSize wird auf weitere Daten zugegriffen, die
			// rechts davon liegen und im Tempor�rpuffer vorhanden sein m�ssen!
			int iInputLength      = iSize;
			int iInputStartOffset = iLeft;
			m_pInterpolationRoutine->Interpolate( m_psbTemp, iInputLength, iInputStartOffset, psbOutput, m_iBlockLength );

			break;
		}

	} // end case switch

	// Neue Verz�gerung speichern
	m_lUserCursors[iCursorID].iOldReadCursorSamples = iNewDelayLocalCopy;

	// Zeitnahme
	double t = m_swProcess.stop( );

	if( m_bBenchmark && t > double( m_iBlockLength ) / m_dSampleRate )
		std::cerr << "[ SIMOVDL ] Dropout detected, reading block for cursor " << iCursorID << " took too long: " << t << std::endl;

	return;
}

void CITASIMOVariableDelayLine::Increment( )
{
	CITASIMOVariableDelayLineBase::Increment( m_iBlockLength );
}
