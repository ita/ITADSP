#include <ITAConstants.h>
#include <ITAFastMath.h>
#include <ITANumericUtils.h>
#include <ITAStringUtils.h>
#include <ITAThirdOctaveFIRFilterGenerator.h>
#include <ITAThirdOctaveMagnitudeSpectrum.h>
#include <spline.h>

using namespace ITABase;

CITAThirdOctaveFIRFilterGenerator::CITAThirdOctaveFIRFilterGenerator( const double dSampleRate, const int iFilterLength )
    : m_dSamplerate( dSampleRate )
    , m_iFilterLength( iFilterLength )
{
	m_iNumInputFreqs = CThirdOctaveGainMagnitudeSpectrum::GetNumBands( ) + 2;
	m_vfInputFreqVector.resize( m_iNumInputFreqs );
	m_vfInputFreqVector[0] = 0; // Left margin
	for( int i = 0; i < CThirdOctaveGainMagnitudeSpectrum::GetNumBands( ); i++ )
		m_vfInputFreqVector[i + 1] = CThirdOctaveGainMagnitudeSpectrum::GetCenterFrequencies( )[i];
	m_vfInputFreqVector[m_iNumInputFreqs - 1] = (float)dSampleRate / 2; // Right margin: Nyquist frequency

	m_vfInputFreqData.resize( m_iNumInputFreqs );

	// DFT frequency bandwidth
	m_fDeltaF = (float)dSampleRate / (float)iFilterLength;

	// Number of symetric DFT coefficients;
	m_iDFTCoeffs = iFilterLength / 2 + 1;

	m_vfFreqDataInterpolated.resize( 2 * m_iDFTCoeffs );
	m_vfImpulseResponse.resize( iFilterLength );

	m_ifft.plan( ITAFFT::IFFT_C2R, iFilterLength, &m_vfFreqDataInterpolated[0], &m_vfImpulseResponse[0] );
}

CITAThirdOctaveFIRFilterGenerator::~CITAThirdOctaveFIRFilterGenerator( ) {}

int CITAThirdOctaveFIRFilterGenerator::GetFilterLength( ) const
{
	return m_iFilterLength;
}

double CITAThirdOctaveFIRFilterGenerator::GetSampleRate( ) const
{
	return m_dSamplerate;
}

int CITAThirdOctaveFIRFilterGenerator::GetLatency( ) const
{
	// Latency = Half DFT period (ceil)
	return uprdiv( m_iFilterLength, 2 );
}


void CITAThirdOctaveFIRFilterGenerator::GenerateFilter( const ITABase::CThirdOctaveGainMagnitudeSpectrum& oTOGainMagnitudes, ITASampleBuffer& sbImpulseResponse,
                                                        bool bMinimumPhase /*=false*/ ) const
{
	if( sbImpulseResponse.GetLength( ) < m_iFilterLength )
		ITA_EXCEPT_INVALID_PARAMETER( "Given impulse response is shorter than initial filter size of generator, cannot proceed" );
	GenerateFilter( oTOGainMagnitudes, sbImpulseResponse.GetData( ), bMinimumPhase );
}

void CITAThirdOctaveFIRFilterGenerator::GenerateFilter( const ITABase::CThirdOctaveGainMagnitudeSpectrum& oTOGainMagnitudes, float* pfFilterCoeffs,
                                                        bool bMinimumPhase /*=false*/ ) const
{
	if( oTOGainMagnitudes.IsZero( ) )
	{
		for( int i = 0; i < m_iFilterLength; i++ )
			pfFilterCoeffs[i] = 0.0f;
		return;
	}

	if( oTOGainMagnitudes.IsIdentity( ) )
	{
		for( int i = 0; i < m_iFilterLength; i++ )
			pfFilterCoeffs[i] = 0.0f;
		if( bMinimumPhase )
			pfFilterCoeffs[0] = 1.0f;
		else
			pfFilterCoeffs[int( m_iFilterLength / 2 )] = 1.0f;
		return;
	}

	// 1st step: Interpolate the magnitudes

	for( int i = 0; i < CThirdOctaveGainMagnitudeSpectrum::GetNumBands( ); i++ )
		m_vfInputFreqData[1 + i] = float( oTOGainMagnitudes[i] );

	// Bounndaries must be defined in a meaningful way.
	m_vfInputFreqData[0]                    = m_vfInputFreqData[1];
	m_vfInputFreqData[m_iNumInputFreqs - 1] = 0.0f;

	// Initialize cubic spline interpolation
	m_ypp = spline_cubic_set( m_iNumInputFreqs, const_cast<float*>( &m_vfInputFreqVector[0] ), &m_vfInputFreqData[0],
	                          1, // Left boundary condition => 1st derivative m=0
	                          0,
	                          1, // Right boundary condition => 1st derivative m=0
	                          0 );
	float fDummy;
	const float fScale = 1 / (float)m_iFilterLength;

	// No DC offset, ever!
	m_vfFreqDataInterpolated[0] = 0;
	m_vfFreqDataInterpolated[1] = 0;

	if( bMinimumPhase )
	{
		for( int i = 1; i < m_iDFTCoeffs; i++ )
		{
			float x = spline_cubic_val( m_iNumInputFreqs, const_cast<float*>( &m_vfInputFreqVector[0] ), &m_vfInputFreqData[0], m_ypp, i * m_fDeltaF, &fDummy, &fDummy );

			// Phase-shift by half the FFT-period
			m_vfFreqDataInterpolated[2 * i]     = x * fScale; // minimum phase
			m_vfFreqDataInterpolated[2 * i + 1] = 0;
		}
	}
	else
	{
		for( int i = 1; i < m_iDFTCoeffs; i++ )
		{
			float x = spline_cubic_val( m_iNumInputFreqs, const_cast<float*>( &m_vfInputFreqVector[0] ), &m_vfInputFreqData[0], m_ypp, i * m_fDeltaF, &fDummy, &fDummy );

			// Phase-shift by half the FFT-period: Negate all odd DFT coefficients
			m_vfFreqDataInterpolated[2 * i]     = ( ( i % 2 ) == 0 ) ? x * fScale : -x * fScale;
			m_vfFreqDataInterpolated[2 * i + 1] = 0;
		}
	}

	// 2nd step: Convert into time-domain (out-of-place C2R-IFFT)
	m_ifft.execute( ); // populates m_vfImpulseResponse

	// Copy to output
	for( int i = 0; i < m_iFilterLength; i++ )
		pfFilterCoeffs[i] = m_vfImpulseResponse[i];
}
