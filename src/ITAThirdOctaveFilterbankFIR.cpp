/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include "ITAThirdOctaveFilterbankFIR.h"

#include <ITAUPConvolution.h>
#include <algorithm>

CITAThirdOctaveFilterbankFIR::CITAThirdOctaveFilterbankFIR( const double dSampleRate, const int iBlockLength )
    : m_pGenerator( nullptr )
    , m_pConvolver( nullptr )
    , m_iBlocklength( iBlockLength )
{
	// [fwe] Hier wird die Filterl�nge f�r Directivities festgelegt, jst: dont take too short lengths as the spline interp requires zero DC offset and will cut low freq
	// amplitudes
	m_iGeneratorFilterLength = int( 4 * ceil( dSampleRate / ITABase::CThirdOctaveMagnitudeSpectrum::GetCenterFrequencies( )[0] ) );
	m_vfGeneratedFIR.resize( m_iGeneratorFilterLength );
	m_pGenerator = new CITAThirdOctaveFIRFilterGenerator( dSampleRate, m_iGeneratorFilterLength );

	m_iConvolutionTaps = ( std::max )( 128, iBlockLength );
	m_pConvolver       = new ITAUPConvolution( iBlockLength, m_iConvolutionTaps );
	m_pConvolver->SetFilterExchangeFadingFunction( ITABase::FadingFunction::COSINE_SQUARE );
	m_pConvolver->SetFilterCrossfadeLength( 32 );

	SetIdentity( false );
}

CITAThirdOctaveFilterbankFIR::~CITAThirdOctaveFilterbankFIR( )
{
	delete m_pGenerator;
	delete m_pConvolver;
}

void CITAThirdOctaveFilterbankFIR::SetIdentity( const bool bSmoothChangeover /*=true*/ )
{
	int iLatency = m_pGenerator->GetLatency( );
	if( iLatency > m_iGeneratorFilterLength )
		ITA_EXCEPT_INVALID_PARAMETER( "Latency exceeds filter length in FIR filterbank identity setter" );

	// Set a dirac with given latency
	fm_zero( &m_vfGeneratedFIR[0], m_iGeneratorFilterLength );
	m_vfGeneratedFIR[iLatency] = 1;

	// Window (with rect), if convolution filter is shorter than generated filter
	int iShift  = std::max( 0, iLatency - m_iConvolutionTaps / 2 );
	int iLength = std::min( m_iConvolutionTaps, m_iGeneratorFilterLength - iShift );

	ITAUPFilter* pFilter = m_pConvolver->RequestFilter( );
	pFilter->Load( &m_vfGeneratedFIR[iShift], iLength );

	m_pConvolver->ExchangeFilter( pFilter, ( bSmoothChangeover ? ITABase::FadingFunction::COSINE_SQUARE : ITABase::FadingFunction::SWITCH ) );
	pFilter->Release( ); // Auto-release
}

void CITAThirdOctaveFilterbankFIR::SetMagnitudes( const ITABase::CThirdOctaveGainMagnitudeSpectrum& oMags, const bool bSmoothChangeover /*= true*/ )
{
	m_pGenerator->GenerateFilter( oMags, &m_vfGeneratedFIR[0], false );

	int iRange  = ( std::min )( m_iGeneratorFilterLength, m_iConvolutionTaps );
	int iShift  = std::max( 0, m_pGenerator->GetLatency( ) - iRange / 2 );
	int iLength = std::min( m_iConvolutionTaps, m_iGeneratorFilterLength - iShift );

	ITAUPFilter* pFilter = m_pConvolver->RequestFilter( );
	pFilter->Load( &m_vfGeneratedFIR[iShift], iLength );

	m_pConvolver->ExchangeFilter( pFilter, ( bSmoothChangeover ? ITABase::FadingFunction::COSINE_SQUARE : ITABase::FadingFunction::SWITCH ) );
	pFilter->Release( ); // Auto-release
}

int CITAThirdOctaveFilterbankFIR::GetLatency( ) const
{
	return m_pGenerator->GetLatency( );
}

void CITAThirdOctaveFilterbankFIR::Clear( )
{
	m_pConvolver->clear( );
	SetIdentity( true );
}

void CITAThirdOctaveFilterbankFIR::Process( const float* pfInputSamples, float* pfOutputSamples )
{
	m_pConvolver->Process( pfInputSamples, m_iBlocklength, pfOutputSamples, m_iBlocklength );
}