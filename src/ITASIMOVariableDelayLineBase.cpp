#include <ITAFastMath.h>
#include <ITANumericUtils.h>
#include <ITASIMOVariableDelayLine.h>
#include <ITASampleBuffer.h>
#include <ITASampleFrame.h>
#include <algorithm>
#include <cassert>
#include <cmath>
#include <spline.h>


CITASIMOVariableDelayLineBase::CITASIMOVariableDelayLineBase( const int iReservedMaxDelaySamples ) : m_psbVDLBuffer( nullptr ), m_psbTemp( nullptr )
{
	Initialise( iReservedMaxDelaySamples );
}

CITASIMOVariableDelayLineBase::CITASIMOVariableDelayLineBase( ) : m_psbVDLBuffer( nullptr ), m_psbTemp( nullptr ) {}

void CITASIMOVariableDelayLineBase::Clear( )
{
	m_psbVDLBuffer->Zero( ); // Very important, because on small delays the potentially unititialized end of VDL is also read
	m_psbTemp->Zero( );

	m_iWriteCursor = 0;
	m_lUserCursors.clear( );
	m_bStarted = false;
}

CITASIMOVariableDelayLineBase::~CITASIMOVariableDelayLineBase( )
{
	delete m_psbVDLBuffer;
	delete m_psbTemp;
}

void CITASIMOVariableDelayLineBase::Initialise( const int ReservedMaxDelaySamples )
{
	assert( ReservedMaxDelaySamples > 0 );

	m_psbTemp = new ITASampleBuffer( ReservedMaxDelaySamples, true );

	ReserveMaximumDelaySamples( ReservedMaxDelaySamples );

	Clear( );
}

int CITASIMOVariableDelayLineBase::GetReservedMaximumDelaySamples( ) const
{
	return m_iVDLBufferSize;
}

void CITASIMOVariableDelayLineBase::ReserveMaximumDelaySamples( int iMaxDelaySamples )
{
	// Festlegung: Die Methode darf nicht parallel betreten werden (non-reentrant)

	assert( iMaxDelaySamples >= 0 ); // Verz�gerung immer positiv

	// Anzahl Pufferbl�cke bestimmen
	int iNewBufferSize = iMaxDelaySamples;
	if( !m_psbVDLBuffer )
	{
		/*
		 *  Erste Initalisierung des Puffers
		 *  Hinweis: Hier ist keine Synchronisierung erforderlich, da dieser Ast
		 *  nur durch den Konstruktor aufgerufen wird
		 */
		m_psbVDLBuffer   = new ITASampleBuffer( iNewBufferSize, true ); // Important! set all samples to zero.
		m_iVDLBufferSize = iNewBufferSize;
		m_iWriteCursor   = 0;
	}
	else
	{
		// Puffer schon gross genug => Nichts tun...
		if( m_psbVDLBuffer->GetLength( ) >= iNewBufferSize )
			return;

		assert( m_psbVDLBuffer->GetLength( ) >= 0 ); // VDL-Pufferl�nge muss eine Gr��e haben

		m_csBuffer.enter( );


		// Alte Puffergr��e sichern
		int iOldBufferSize = m_psbVDLBuffer->length( );

		// Vorhandene Daten zyklisch Kopieren (aktuelle Schreibposition => Anfang abrollen)
		ITASampleBuffer* psbNewBuffer = new ITASampleBuffer( iNewBufferSize, true ); // Important! set all samples to zero.
		psbNewBuffer->cyclic_write( m_psbVDLBuffer, iNewBufferSize, m_iWriteCursor, 0 );

		// Alten Puffer freigeben, neuen zuweisen
		delete m_psbVDLBuffer;
		m_psbVDLBuffer   = psbNewBuffer;
		m_iVDLBufferSize = iNewBufferSize;
		m_iWriteCursor   = iOldBufferSize; // Hinten anh�ngend weiterschreiben

		m_csBuffer.leave( );
	}
}

int CITASIMOVariableDelayLineBase::AddCursor( )
{
	int iMaxNumber         = -1;
	std::vector<int> vrIDs = GetCursorIDs( );
	for( size_t i = 0; i < vrIDs.size( ); i++ )
		if( iMaxNumber < vrIDs[i] )
			iMaxNumber = vrIDs[i];

	const int iID       = iMaxNumber + 1;
	m_lUserCursors[iID] = CITAVDLReadCursor( );

	return iID;
}

void CITASIMOVariableDelayLineBase::SetOverlapSamples( const int iCursorID, const int iLeft, const int iRight )
{
	if( !CursorExists( iCursorID ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid read cursor id" );

	if( iLeft < 0 || iRight < 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid overlap to the left or right for this cursor" );

	std::map<int, CITAVDLReadCursor>::iterator it = m_lUserCursors.find( iCursorID );
	CITAVDLReadCursor& oReadCursor( it->second );

	oReadCursor.iOverlapSamplesLeft  = iLeft;
	oReadCursor.iOverlapSamplesRight = iRight;
}

int CITASIMOVariableDelayLineBase::GetEffectiveReadLength( const int iCursorID, const int iLength ) const
{
	if( !CursorExists( iCursorID ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Could not find requested read cursor" );

	auto cit = m_lUserCursors.find( iCursorID );
	auto& oReadCursor( cit->second );

	int iDelayDifference     = oReadCursor.iNewReadCursorSamples - oReadCursor.iOldReadCursorSamples;
	int iEffectiveReadLength = oReadCursor.iOverlapSamplesLeft + iLength + oReadCursor.iOverlapSamplesRight - iDelayDifference;

	return std::max( 0, iEffectiveReadLength );
}

int CITASIMOVariableDelayLineBase::GetHeadroomSamples( const int iCursorID ) const
{
	ITA_EXCEPT1( NOT_IMPLEMENTED, "Function is faulty" );
	auto cit = m_lUserCursors.find( iCursorID );
	auto& oReadCursor( cit->second );
	int iDelta = m_iWriteCursor - oReadCursor.iOldReadCursorSamples;
	if( iDelta < 0 )
		iDelta += m_iVDLBufferSize;
	return iDelta;
}

int CITASIMOVariableDelayLineBase::GetOverlappingSamplesLeft( const int iCursorID ) const
{
	if( !CursorExists( iCursorID ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Could not find requested read cursor" );

	auto cit = m_lUserCursors.find( iCursorID );
	auto& oReadCursor( cit->second );

	return oReadCursor.iOverlapSamplesLeft;
}

int CITASIMOVariableDelayLineBase::GetOverlappingSamplesRight( const int iCursorID ) const
{
	if( !CursorExists( iCursorID ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Could not find requested read cursor" );

	auto cit = m_lUserCursors.find( iCursorID );
	auto& oReadCursor( cit->second );

	return oReadCursor.iOverlapSamplesRight;
}

bool CITASIMOVariableDelayLineBase::CursorExists( const int iCursorID ) const
{
	std::map<int, CITAVDLReadCursor>::const_iterator cit = m_lUserCursors.find( iCursorID );
	return ( cit != m_lUserCursors.end( ) );
}

std::vector<int> CITASIMOVariableDelayLineBase::GetCursorIDs( ) const
{
	std::vector<int> viCursorIDs;
	std::map<int, CITAVDLReadCursor>::const_iterator cit = m_lUserCursors.begin( );
	while( cit != m_lUserCursors.end( ) )
		viCursorIDs.push_back( cit++->first );

	return viCursorIDs;
}

int CITASIMOVariableDelayLineBase::GetNumCursors( ) const
{
	return int( m_lUserCursors.size( ) );
}

int CITASIMOVariableDelayLineBase::GetCurrentDelaySamples( const int iCursorID ) const
{
	if( !CursorExists( iCursorID ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Could not find requested read cursor" );

	// Um Konsistenz zu wahren geben wir immer den
	// neuen Verz�gerungswert zur�ck, da es sein kann,
	// dass Getter und Setter f�r den Delay mehrmals benutzt werden,
	// ohne dass im Process() Schritt der neue Wert als Aktueller
	// �bernommen wurde.

	std::map<int, CITAVDLReadCursor>::const_iterator cit = m_lUserCursors.find( iCursorID );
	const CITAVDLReadCursor& oReadCursor( cit->second );

	return oReadCursor.iOldReadCursorSamples;
}

int CITASIMOVariableDelayLineBase::GetNewDelaySamples( const int iCursorID ) const
{
	if( !CursorExists( iCursorID ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Could not find requested read cursor" );

	// Um Konsistenz zu wahren geben wir immer den
	// neuen Verz�gerungswert zur�ck, da es sein kann,
	// dass Getter und Setter f�r den Delay mehrmals benutzt werden,
	// ohne dass im Process() Schritt der neue Wert als Aktueller
	// �bernommen wurde.

	std::map<int, CITAVDLReadCursor>::const_iterator cit = m_lUserCursors.find( iCursorID );
	const CITAVDLReadCursor& oReadCursor( cit->second );

	return oReadCursor.iNewReadCursorSamples;
}

void CITASIMOVariableDelayLineBase::SetDelaySamples( const int iID, const int iDelaySamples )
{
	assert( iDelaySamples >= 0 ); // Verz�gerung immer positiv

	// Internen Puffer vergr��ern
	/* Erfordert die neuen Verz�gerung auch eine Puffer-Vergr��erung,
	 * ist es wahrscheinlich, dass bald erneut eine Vergr��erung n�tig ist.
	 * Dies ist teuer, deshalb erzwingen wird hier direkt eine deutliche
	 * Vergr��erung.
	 */
	if( m_iVDLBufferSize < iDelaySamples )
		ReserveMaximumDelaySamples( iDelaySamples * 2 );

	if( !CursorExists( iID ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Could not find requested read cursor" );

	CITAVDLReadCursor& oReadCursor( m_lUserCursors[iID] );
	oReadCursor.iNewReadCursorSamples = iDelaySamples;

	// If not started, set both to avoid artifact on first block
	if( !m_bStarted || !oReadCursor.bInitialized )
		oReadCursor.iOldReadCursorSamples = iDelaySamples;
}

void CITASIMOVariableDelayLineBase::Write( const int iNumSamples, const float* pfInput )
{
	if( !m_bStarted )
		m_bStarted = true;

	m_psbVDLBuffer->cyclic_write( pfInput, iNumSamples, m_iWriteCursor );
}

void CITASIMOVariableDelayLineBase::Increment( const int iNumSamples )
{
	m_iWriteCursor = ( m_iWriteCursor + iNumSamples ) % m_iVDLBufferSize;
}

void CITASIMOVariableDelayLineBase::Read( const int iCursorID, const int iNumSamples, float* pfOutput )
{
	if( !CursorExists( iCursorID ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Could not find requested read cursor" );

	auto cit = m_lUserCursors.find( iCursorID );
	auto& oReadCursor( m_lUserCursors[iCursorID] );

	int iCurrentDelayLocalCopy = oReadCursor.iOldReadCursorSamples;
	int iNewDelayLocalCopy     = oReadCursor.iNewReadCursorSamples;

	// Use a fast-forward copy in case of no new delay (static situation)
	int iReadCursorWithOverlapLeft = ( m_iWriteCursor - iCurrentDelayLocalCopy - oReadCursor.iOverlapSamplesLeft + m_iVDLBufferSize ) % m_iVDLBufferSize;

	int iEffectiveNumSamples = GetEffectiveReadLength( iCursorID, iNumSamples );
	m_psbVDLBuffer->cyclic_read( pfOutput, iEffectiveNumSamples, iReadCursorWithOverlapLeft );

	if( iNewDelayLocalCopy != iCurrentDelayLocalCopy )
	{
		m_lUserCursors[iCursorID].iOldReadCursorSamples = iNewDelayLocalCopy;
	}

	if( !oReadCursor.bInitialized )
		oReadCursor.bInitialized = true;
}
