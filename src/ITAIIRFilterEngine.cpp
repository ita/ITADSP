﻿#include <ITAIIRFilterEngine.h>
#include <ITABaseDefinitions.h>
#include <ITAException.h>
#include <cassert>

CITAIIRFilterEngine::CITAIIRFilterEngine( unsigned int uiOrder /* = 0 */, bool bIsARMA /* = false */ ) : m_uiCursor( 0 ), m_bNewFilterCoefficients( false )
{
	Initialise( uiOrder, bIsARMA );
}

void CITAIIRFilterEngine::Initialise( unsigned int uiOrder, bool bIsARMA )
{
	m_oCoeffsNew.Initialise( uiOrder, bIsARMA );
	m_oCoeffs                = m_oCoeffsNew;
	m_bNewFilterCoefficients = true; // trigger swap in streaming context

	m_vfAccumulator.resize( uiOrder + 1 );

	Reset( );
}

void CITAIIRFilterEngine::Reset( )
{
	SetCoefficientsToZero( );
	ClearAccumulators( );
}

unsigned int CITAIIRFilterEngine::GetOrder( ) const
{
	return (unsigned int)( m_vfAccumulator.size( ) - 1 );
}

void CITAIIRFilterEngine::SetCoefficientsToZero( )
{
	m_oCoeffsNew.SetZero( );
	m_bNewFilterCoefficients = true; // trigger swap in streaming context
}

void CITAIIRFilterEngine::SetCoefficientsToIdentity( )
{
	m_oCoeffsNew.SetIdentity( );
	m_bNewFilterCoefficients = true; // trigger swap in streaming context
}

void CITAIIRFilterEngine::SetCoefficients( const ITADSP::CFilterCoefficients& oNewCoeffs )
{
	if( oNewCoeffs.uiOrder != m_oCoeffs.uiOrder )
		ITA_EXCEPT_INVALID_PARAMETER( "Order missmatch in filter coefficients" );

	m_oCoeffsNew             = oNewCoeffs;
	m_bNewFilterCoefficients = true; // trigger swap in streaming context
}

void CITAIIRFilterEngine::ClearAccumulators( )
{
	for( size_t i = 0; i < m_vfAccumulator.size( ); i++ )
		m_vfAccumulator[i] = 0.0f;
}

void CITAIIRFilterEngine::Process( const float* pfInputData, float* pfOutputData, const int iNumSamples )
{
	// This implements a "Direct form II" IIR filter structure of arbitrary order.
	// The corresponding equations are:
	// v(i) = x(i) - a(1)*v(i-1) - a(2)*v(i-2) - ... - a(n)*v(i-n)
	// y(i) = b(0)*v(i) + b(1)*v(i-1) + ... + bn*v(i-n)
	// i -> sample number, n -> filter order, a() -> filter denominator coefficients, b() -> filter numerator coefficients
	// x() -> input samples, v() -> accumulators, y() -> output samples

	// In this implementation, the accumulator is thought of as a cyclic vector of length filter_order + 1
	// The accumulator element corresponding to the latest "v" variable is shown by the read cursor "m_uiCursor
	// accumulator elements corresponding to previously calculated "v" values are situated to the right of the read cursor cyclically from most to least recent
	// The read cursor then moves left to right cyclically, over-writing the least recent element at each sample iteration (each i)
	// e.g. say we have a 2nd order filter, for the first sample (i=0), m_vfAccumulator(0) is assigned a value, and m_vfAccumulator(1 & 2) = 0.
	// for the second sample (i=1), the read cursor moves to the right from 0 to 2, then m_vfAccumulator(2) is calculated. m_vfAccumulator(0 & 1) are the same as the
	// previous iteration. for the third sample (i=3), the read cursor becomes 1, and m_vfAccumulator(1) is calculated. after this (i=4), the read cursor returns to 0, and
	// the value for m_vfAccumulator(0) calculated at i=0 is overwritten as it is the least recent entry. This cycle repeats for all samples in the input buffer

	if( m_bNewFilterCoefficients )
	{
		m_oCoeffs                = m_oCoeffsNew;
		m_bNewFilterCoefficients = false;
	}

	for( int i = 0; i < iNumSamples; i++ )
	{
		m_vfAccumulator[m_uiCursor] = pfInputData[i]; // v(n) = x(n), insert ith sample of input data at the start of the accumulator
		for( int j = 0; j < m_oCoeffs.vfDenominator.size( ) - 1; j++ )
		{ // assumes that element 0 of the numerator coefficients is a1, rather than a0 (a0 normalised to 1)
			m_vfAccumulator[m_uiCursor] -= m_oCoeffs.vfDenominator[j + 1] *
			                               m_vfAccumulator[( m_uiCursor + j + 1 ) % m_vfAccumulator.size( )]; // add to the current accumulator v(n) = v(n) - a(j)*v(j)
		}

		pfOutputData[i] = m_oCoeffs.vfNumerator[0] * m_vfAccumulator[m_uiCursor]; // overrides whatever might have been there already
		for( int j = 1; j < m_oCoeffs.vfNumerator.size( ); j++ )
		{
			pfOutputData[i] += m_oCoeffs.vfNumerator[j] * m_vfAccumulator[( m_uiCursor + j ) % m_vfAccumulator.size( )]; // add output sample y(n) += b(j)*v(j)
		}

		m_uiCursor = (unsigned int)( m_vfAccumulator.size( ) + m_uiCursor - 1 ) % m_vfAccumulator.size( ); // move the read cursor one space back
	}
}

void CITAIIRFilterEngine::Process( const float* pfInputData, float* pfOutputData, const int iNumSamples, const float fOutputGain, const int iOutputMode )
{
	if( m_bNewFilterCoefficients )
	{
		m_oCoeffs                = m_oCoeffsNew;
		m_bNewFilterCoefficients = false;
	}

	if( iOutputMode == ITABase::MixingMethod::ADD )
	{
		for( int i = 0; i < iNumSamples; i++ )
		{
			m_vfAccumulator[m_uiCursor] = pfInputData[i]; // v(n) = x(n), insert ith sample of input data at the start of the accumulator
			for( int j = 0; j < m_oCoeffs.vfDenominator.size( ) - 1; j++ )
			{ // assumes that element 0 of the numerator coefficients is a1, rather than a0 (a0 normalised to 1)
				m_vfAccumulator[m_uiCursor] -=
				    m_oCoeffs.vfDenominator[j + 1] *
				    m_vfAccumulator[( m_uiCursor + j + 1 ) % m_vfAccumulator.size( )]; // add to the current accumulator v(n) = v(n) - a(j)*v(j)
			}

			for( int j = 0; j < m_oCoeffs.vfNumerator.size( ); j++ )
			{
				pfOutputData[i] +=
				    m_oCoeffs.vfNumerator[j] * m_vfAccumulator[( m_uiCursor + j ) % m_vfAccumulator.size( )] * fOutputGain; // add output sample y(n) += b(j)*v(j)
			}

			m_uiCursor = (unsigned int)( m_vfAccumulator.size( ) + m_uiCursor - 1 ) % m_vfAccumulator.size( ); // move the read cursor one space back
		}
	}
	else if( iOutputMode == ITABase::MixingMethod::OVERWRITE )
	{
		for( int i = 0; i < iNumSamples; i++ )
		{
			m_vfAccumulator[m_uiCursor] = pfInputData[i]; // v(n) = x(n), insert ith sample of input data at the start of the accumulator
			for( int j = 0; j < m_oCoeffs.vfDenominator.size( ) - 1; j++ )
			{ // assumes that element 0 of the numerator coefficients is a1, rather than a0 (a0 normalised to 1)
				m_vfAccumulator[m_uiCursor] -=
				    m_oCoeffs.vfDenominator[j + 1] *
				    m_vfAccumulator[( m_uiCursor + j + 1 ) % m_vfAccumulator.size( )]; // add to the current accumulator v(n) = v(n) - a(j)*v(j)
			}

			pfOutputData[i] = m_oCoeffs.vfNumerator[0] * m_vfAccumulator[m_uiCursor] * fOutputGain; // overrides whatever might have been there already
			for( int j = 1; j < m_oCoeffs.vfNumerator.size( ); j++ )
			{
				pfOutputData[i] +=
				    m_oCoeffs.vfNumerator[j] * m_vfAccumulator[( m_uiCursor + j ) % m_vfAccumulator.size( )] * fOutputGain; // add output sample y(n) += b(j)*v(j)
			}

			m_uiCursor = (unsigned int)( m_vfAccumulator.size( ) + m_uiCursor - 1 ) % m_vfAccumulator.size( ); // move the read cursor one space back
		}
	}
	else
	{
		ITA_EXCEPT1( INVALID_PARAMETER, "Unrecognized output write mode in CITAFilterEngine" );
	}
}

void CITAIIRFilterEngine::Process( const float* pfInputData, float* pfOutputData, const int iNumSamples, const float fOutputGain1, const float fOutputGain2,
                                   const int iOutputWriteMode )
{
	if( iNumSamples == 0 )
		return;

	if( m_bNewFilterCoefficients )
	{
		m_oCoeffs                = m_oCoeffsNew;
		m_bNewFilterCoefficients = false;
	}

	// Factor for linear gain
	const float fLinearGainFactor = ( fOutputGain2 - fOutputGain1 ) / float( iNumSamples );

	if( iOutputWriteMode == ITABase::MixingMethod::ADD )
	{
		for( int i = 0; i < iNumSamples; i++ )
		{
			const float fSampleGain = fOutputGain1 + i * fLinearGainFactor;

			m_vfAccumulator[m_uiCursor] = pfInputData[i]; // v(n) = x(n), insert ith sample of input data at the start of the accumulator
			for( int j = 0; j < m_oCoeffs.vfDenominator.size( ) - 1; j++ )
			{ // assumes that element 0 of the numerator coefficients is a1, rather than a0 (a0 normalised to 1)
				m_vfAccumulator[m_uiCursor] -=
				    m_oCoeffs.vfDenominator[j + 1] *
				    m_vfAccumulator[( m_uiCursor + j + 1 ) % m_vfAccumulator.size( )]; // add to the current accumulator v(n) = v(n) - a(j)*v(j)
			}

			for( int j = 0; j < m_oCoeffs.vfNumerator.size( ); j++ )
			{
				pfOutputData[i] +=
				    m_oCoeffs.vfNumerator[j] * m_vfAccumulator[( m_uiCursor + j ) % m_vfAccumulator.size( )] * fSampleGain; // add output sample y(n) += b(j)*v(j)
			}

			m_uiCursor = (unsigned int)( m_vfAccumulator.size( ) + m_uiCursor - 1 ) % m_vfAccumulator.size( ); // move the read cursor one space back
		}
	}
	else if( iOutputWriteMode == ITABase::MixingMethod::OVERWRITE )
	{
		for( int i = 0; i < iNumSamples; i++ )
		{
			const float fSampleGain = fOutputGain1 + i * fLinearGainFactor;

			m_vfAccumulator[m_uiCursor] = pfInputData[i]; // v(n) = x(n), insert ith sample of input data at the start of the accumulator
			for( int j = 0; j < m_oCoeffs.vfDenominator.size( ) - 1; j++ )
			{ // assumes that element 0 of the numerator coefficients is a1, rather than a0 (a0 normalised to 1)
				m_vfAccumulator[m_uiCursor] -=
				    m_oCoeffs.vfDenominator[j + 1] *
				    m_vfAccumulator[( m_uiCursor + j + 1 ) % m_vfAccumulator.size( )]; // add to the current accumulator v(n) = v(n) - a(j)*v(j)
			}

			// Overrides whatever might have been there already
			pfOutputData[i] = m_oCoeffs.vfNumerator[0] * m_vfAccumulator[m_uiCursor];

			for( int j = 1; j < m_oCoeffs.vfNumerator.size( ); j++ ) // Add residual samples from network
			{
				pfOutputData[i] += m_oCoeffs.vfNumerator[j] * m_vfAccumulator[( m_uiCursor + j ) % m_vfAccumulator.size( )]; // add output sample y(n) += b(j)*v(j)
			}

			pfOutputData[i] *= fSampleGain; // Apply gain on output

			m_uiCursor = (unsigned int)( m_vfAccumulator.size( ) + m_uiCursor - 1 ) % m_vfAccumulator.size( ); // move the read cursor one space back
		}
	}
	else
	{
		ITA_EXCEPT1( INVALID_PARAMETER, "Unrecognized output write mode in CITAFilterEngine" );
	}
}
