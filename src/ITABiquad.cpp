﻿#include <ITABiquad.h>
#include <ITABaseDefinitions.h>
#include <ITAException.h>

CITABiquad::CITABiquad( )
{
	ClearAccumulators( );
}

void CITABiquad::ClearAccumulators( )
{
	z[0] = z[1] = 0;
}

void CITABiquad::Process( const float* pfInputData, float* pfOutputData, const int iNumSamples )
{
	// Local accumulators
	float z0, z1, z2;

	// Restore accumulators from last process call
	z1 = z[0];
	z2 = z[1];

	for( int i = 0; i < iNumSamples; i++ )
	{
		// w[n] = x[n] - a_1*w[n-1] - a_2*w[n-2]
		z0 = oParams.g * pfInputData[i] - oParams.a1 * z1 - oParams.a2 * z2;

		// y[n] = b_0*w[n] + b_1*w[n-1] + b_2*w[n-2]
		pfOutputData[i] = oParams.b0 * z0 + oParams.b1 * z1 + oParams.b2 * z2;

		// Shift accumulators
		z2 = z1;
		z1 = z0;
	}

	// Store accumulators for next process call
	z[0] = z1;
	z[1] = z2;

	return;
}

void CITABiquad::Process( const float* pfInputData, float* pfOutputData, const int iNumSamples, const float fOutputGain, const int iOutputMode )
{
	// Local accumulators
	float z0, z1, z2;

	z1 = z[0];
	z2 = z[1];

	if( iOutputMode == ITABase::MixingMethod::ADD )
	{
		for( int i = 0; i < iNumSamples; i++ )
		{
			z0 = oParams.g * pfInputData[i] - oParams.a1 * z1 - oParams.a2 * z2;
			pfOutputData[i] += ( oParams.b0 * z0 + oParams.b1 * z1 + oParams.b2 * z2 ) * fOutputGain;

			// Shift accumulators
			z2 = z1;
			z1 = z0;
		}
	}
	else if( iOutputMode == ITABase::MixingMethod::OVERWRITE )
	{
		for( int i = 0; i < iNumSamples; i++ )
		{
			z0              = oParams.g * pfInputData[i] - oParams.a1 * z1 - oParams.a2 * z2;
			pfOutputData[i] = ( oParams.b0 * z0 + oParams.b1 * z1 + oParams.b2 * z2 ) * fOutputGain;

			// Shift accumulators
			z2 = z1;
			z1 = z0;
		}
	}
	else
	{
		ITA_EXCEPT1( INVALID_PARAMETER, "Unrecognized output write mode in CITABiquad" );
	}

	// Store accumulators
	z[0] = z1;
	z[1] = z2;

	return;
}

void CITABiquad::Process( const float* pfInputData, float* out, const int iNumSamples, const float fOutputGain1, const float fOutputGain2, const int iOutputWriteMode )
{
	if( iNumSamples == 0 )
		return;

	// Local accumulators
	float z0, z1, z2;
	z1 = z[0];
	z2 = z[1];

	// Factor for linear gain
	const float fLinearGainFactor = ( fOutputGain2 - fOutputGain1 ) / float( iNumSamples );

	if( iOutputWriteMode == ITABase::MixingMethod::ADD )
	{
		for( int i = 0; i < iNumSamples; i++ )
		{
			const float fSampleGain = fOutputGain1 + i * fLinearGainFactor;

			z0 = oParams.g * pfInputData[i] - oParams.a1 * z1 - oParams.a2 * z2;
			out[i] += ( oParams.b0 * z0 + oParams.b1 * z1 + oParams.b2 * z2 ) * fSampleGain;

			// Shift accumulators
			z2 = z1;
			z1 = z0;
		}
	}
	else if( iOutputWriteMode == ITABase::MixingMethod::OVERWRITE )
	{
		for( int i = 0; i < iNumSamples; i++ )
		{
			const float fSampleGain = fOutputGain1 + i * fLinearGainFactor;

			z0     = oParams.g * pfInputData[i] - oParams.a1 * z1 - oParams.a2 * z2;
			out[i] = ( oParams.b0 * z0 + oParams.b1 * z1 + oParams.b2 * z2 ) * fSampleGain;

			// Shift accumulators
			z2 = z1;
			z1 = z0;
		}
	}
	else
	{
		ITA_EXCEPT1( INVALID_PARAMETER, "Unrecognized output write mode in CITABiquad" );
	}

	// Store accumulators
	z[0] = z1;
	z[1] = z2;
}

CITABiquad::CParams::CParams( ) : g( 1 ), a1( 0 ), a2( 0 ), b0( 1 ), b1( 0 ), b2( 0 ) {}

void CITABiquad::CParams::SetParameters( const float params[] )
{
	g  = params[0];
	b0 = params[1];
	b1 = params[2];
	b2 = params[3];
	a1 = params[4];
	a2 = params[5];
}
