#include <ITAConstants.h>
#include <ITADebug.h>
#include <ITAFastMath.h>
#include <ITAInterpolation.h>
#include <ITANumericUtils.h>
#include <ITASampleBuffer.h>
#include <ITAStringUtils.h>
#include <ITAVariableDelayLine.h>
#include <cassert>
#include <cmath>
#include <iostream>
#include <spline.h>


// Interpolation resampling factor range (otherwise crossfading is used)
#define MIN_RESAMPLING_FACTOR 0.01f // [0, inf) ... aber sinnvoll z.B. 0, 0.1, 0.5 (< 1)
#define MAX_RESAMPLING_FACTOR 25.0f // [0, inf) ... aber sinnvoll z.B. 1, 1.5, 2, 3 (> 1)


// --= VDL =--

CITAVariableDelayLine::CITAVariableDelayLine( const double dSamplerate, const int iBlocklength, const float fReservedMaxDelaySamples,
                                              const EVDLAlgorithm eAlgorithm /* = cubic spline */ )
    : m_dSampleRate( dSamplerate )
    , m_iBlockLength( iBlocklength )
    , m_eSwitchingAlgorithm( eAlgorithm )
    , m_psbVDLBuffer( nullptr )
    , m_psbTemp( nullptr )
    , m_bFracDelays( false )
    , m_pInterpolationRoutine( nullptr )
{
	assert( dSamplerate > 0 );
	assert( iBlocklength > 0 );

	// Define size of temp buffer depending on maximum oversampling factor
	int iTempBufferBlockLength = ( (int)ceil( MAX_RESAMPLING_FACTOR ) + 1 ) * m_iBlockLength;
	m_psbTemp                  = new ITASampleBuffer( iTempBufferBlockLength, true );

	ReserveMaximumDelaySamples( fReservedMaxDelaySamples );

	m_iFadeLength = std::min( m_iBlockLength, 32 );

	if( m_eSwitchingAlgorithm == EVDLAlgorithm::SWITCH || m_eSwitchingAlgorithm == EVDLAlgorithm::CROSSFADE )
		m_pInterpolationRoutine = nullptr;
	else if( m_eSwitchingAlgorithm == EVDLAlgorithm::LINEAR_INTERPOLATION )
		m_pInterpolationRoutine = std::make_shared<CITASampleLinearInterpolation>( );
	else if( m_eSwitchingAlgorithm == EVDLAlgorithm::CUBIC_SPLINE_INTERPOLATION )
		m_pInterpolationRoutine = std::make_shared < CITASampleCubicSplineInterpolation>( );
	else if( m_eSwitchingAlgorithm == EVDLAlgorithm::WINDOWED_SINC_INTERPOLATION )
		m_pInterpolationRoutine = std::make_shared < CITASampleWindowedSincInterpolation>( );
	else
		ITA_EXCEPT_INVALID_PARAMETER( "Unknown switching algorithm for VDL." );

#if( ITA_DSP_VDL_DATA_LOG == 1 )
	m_oDataLog.setOutputFile( "VDL.log" );
#endif

	Clear( );
}

CITAVariableDelayLine::CITAVariableDelayLine( const CITAVDLConfig& oConfig )
    : CITAVariableDelayLine( oConfig.dSampleRate, oConfig.iBlockSize, oConfig.fMaxDelaySamples, oConfig.eAlgorithm )
{
}

void CITAVariableDelayLine::Clear( )
{
	m_psbVDLBuffer->Zero( );
	m_psbTemp->Zero( );

	m_iWriteCursor  = 0;
	m_fCurrentDelay = 0;
	m_fNewDelay     = 0;
	m_bStarted      = false;

	m_swProcess.reset( );
	m_swBufferSizeInc.reset( );
	m_iNumberOfDropouts = 0;
}

CITAVariableDelayLine::~CITAVariableDelayLine( )
{
	delete m_psbVDLBuffer;
	delete m_psbTemp;

	std::string sAddition = "";
	if( m_eSwitchingAlgorithm == EVDLAlgorithm::SWITCH )
		sAddition = "(Switching)";
	else if( m_eSwitchingAlgorithm == EVDLAlgorithm::CROSSFADE )
		sAddition = "(Crossfading)";
	else if( m_pInterpolationRoutine )
		sAddition = "(" + m_pInterpolationRoutine->GetName( ) + ")";

	if( m_swProcess.cycles( ) > 0 )
	{
		DEBUG_PRINTF( " * [VDL] Process time monitor: avg=%.2f us max=%.2f us stddev=%.2f us, dropouts=%i %s\n", m_swProcess.mean( ) * 1e6, m_swProcess.maximum( ) * 1e6,
		              m_swProcess.std_deviation( ) * 1e6, m_iNumberOfDropouts, sAddition.c_str( ) );
	}
}

EVDLAlgorithm CITAVariableDelayLine::GetAlgorithm( ) const
{
	return m_eSwitchingAlgorithm;
}

void CITAVariableDelayLine::SetAlgorithm( EVDLAlgorithm eAlgorithm )
{
	m_eSwitchingAlgorithm = eAlgorithm;
}

float CITAVariableDelayLine::GetReservedMaximumDelaySamples( ) const
{
	return (float)( m_iVDLBufferSize - m_iBlockLength );
}

float CITAVariableDelayLine::GetReservedMaximumDelayTime( ) const
{
	return GetReservedMaximumDelaySamples( ) / (float)m_dSampleRate;
}

int CITAVariableDelayLine::GetMinimumDelaySamples( ) const
{
	if( !m_pInterpolationRoutine )
		return 0;

	int iLeft, iRight;
	m_pInterpolationRoutine->GetOverlapSamples( iLeft, iRight );

	return ( iLeft + iRight );
}

float CITAVariableDelayLine::GetMinimumDelayTime( ) const
{
	return GetMinimumDelaySamples( ) / (float)m_dSampleRate;
}

void CITAVariableDelayLine::ReserveMaximumDelaySamples( float fMaxDelaySamples )
{
	// Festlegung: Die Methode darf nicht parallel betreten werden (non-reentrant)

	assert( fMaxDelaySamples >= 0 ); // Verz�gerung immer positiv

	// Anzahl Pufferbl�cke bestimmen
	int iNewBufferSize = uprmul( (int)ceil( fMaxDelaySamples ), m_iBlockLength );

	assert( iNewBufferSize > 0 ); // Puffer muss eine Gr��e haben

	if( !m_psbVDLBuffer )
	{
		/*
		 *  Erste Initalisierung des Puffers
		 *  Hinweis: Hier ist keine Synchronisierung erforderlich, da dieser Ast
		 *  nur durch den Konstruktor aufgerufen wird
		 */

		m_psbVDLBuffer   = new ITASampleBuffer( iNewBufferSize, true );
		m_iVDLBufferSize = iNewBufferSize;
		m_iWriteCursor   = 0;
	}
	else
	{
		// Puffer schon gross genug => Nichts tun...
		if( m_psbVDLBuffer->length( ) >= iNewBufferSize )
			return;

		m_swBufferSizeInc.start( );

		assert( m_psbVDLBuffer->length( ) >= 0 ); // VDL-Pufferl�nge muss eine Gr��e haben

		m_csBuffer.enter( );


		// Alte Puffergr��e sichern
		int iOldBufferSize = m_psbVDLBuffer->length( );

		// Vorhandene Daten zyklisch Kopieren (aktuelle Schreibposition => Anfang abrollen)
		ITASampleBuffer* psbNewBuffer = new ITASampleBuffer( iNewBufferSize, true );
		psbNewBuffer->cyclic_write( m_psbVDLBuffer, iNewBufferSize, m_iWriteCursor, 0 );

		// Alten Puffer freigeben, neuen zuweisen
		delete m_psbVDLBuffer;
		m_psbVDLBuffer   = psbNewBuffer;
		m_iVDLBufferSize = iNewBufferSize;
		m_iWriteCursor   = iOldBufferSize; // Hinten anh�ngend weiterschreiben

		m_csBuffer.leave( );

		double t = m_swBufferSizeInc.stop( ) * 1e6;
		std::cout << "VairableDelayLine: Buffer increment from " << iOldBufferSize << " samples to " << iNewBufferSize << " samples triggered, took "
		          << timeToString( t * 1e-6 );
	}
}

void CITAVariableDelayLine::ReserveMaximumDelayTime( float fMaxDelaySecs )
{
	ReserveMaximumDelaySamples( fMaxDelaySecs * (float)m_dSampleRate );
}

bool CITAVariableDelayLine::GetFractionalDelaysEnabled( ) const
{
	return m_bFracDelays;
}

void CITAVariableDelayLine::SetFractionalDelaysEnabled( bool bEnabled )
{
	m_bFracDelays = bEnabled;
}

float CITAVariableDelayLine::GetDelaySamples( ) const
{
	// Um Konsistenz zu wahren geben wir immer den
	// neuen Verz�gerungswert zur�ck, da es sein kann,
	// dass Getter und Setter f�r den Delay mehrmals benutzt werden,
	// ohne dass im Process() Schritt der neue Wert als Aktueller
	// �bernommen wurde.
	if( m_fCurrentDelay == m_fNewDelay )
		return m_fCurrentDelay;
	else
		return m_fNewDelay;
}

float CITAVariableDelayLine::GetDelaySamples( int& iIntegerDelay, float& fFractionalDelay ) const
{
	// Lokale Kopie der Verz�gerung
	float fDelay = GetDelaySamples( );

	iIntegerDelay    = (int)floor( fDelay );
	fFractionalDelay = fDelay - (float)iIntegerDelay;
	return fDelay;
}

void CITAVariableDelayLine::SetDelaySamples( float fDelaySamples )
{
	assert( fDelaySamples >= 0 ); // Verz�gerung immer positiv

	// Internen Puffer vergr��ern
	/* Erfordert die neuen Verz�gerung auch eine Puffer-Vergr��erung,
	 * ist es wahrscheinlich, dass bald erneut eine Vergr��erung n�tig ist.
	 * Dies ist teuer, deshalb erzwingen wird hier direkt eine deutliche
	 * Vergr��erung.
	 */
	if( m_iVDLBufferSize - m_iBlockLength < ceil( fDelaySamples ) )
		ReserveMaximumDelaySamples( ceil( fDelaySamples ) * 2 );

	// Neuen Wert nicht einfach �bernehmen, sondern der
	// Process()-Routine �berlassen
	m_fNewDelay = fDelaySamples;

	// DEBUG_PRINTF(" * [VDL] Delay set to %.3f samples\n", fDelaySamples);

	// Falls das Streaming noch nicht gestartet ist sofort die
	// neue Verz�gerung �bernehmen, sonst wird im ersten Block
	// gleich von 0 auf fDelaySamples umgesetzt und bei
	// Crossfading und Interpolieren gibts unerw�nschte Effekte
	if( !m_bStarted )
		m_fCurrentDelay.exchange( m_fNewDelay );
}

void CITAVariableDelayLine::SetDelayTime( float fDelaySecs )
{
	SetDelaySamples( fDelaySecs * (float)m_dSampleRate );
}

void CITAVariableDelayLine::Process( const ITASampleBuffer* psbInput, ITASampleBuffer* psbOutput )
{
	m_swProcess.start( );

	// Start-Flag setzen
	if( !m_bStarted )
		m_bStarted = true;

	assert( m_iWriteCursor % m_iBlockLength == 0 ); // Schreibcursor immer Vielfaches der Blockl�nge

	// Neue Daten in den VDL-Puffer schreiben
	m_psbVDLBuffer->write( psbInput, m_iBlockLength, 0, m_iWriteCursor );

	// Lokale Kopie des gew�nschten Algorithmus (Atomare Membervariable)
	EVDLAlgorithm eAlgorithm = m_eSwitchingAlgorithm;

	// Lokale Kopie der neuen Verz�gerung
	float fCurrentDelay = m_fCurrentDelay;
	float fNewDelay     = m_fNewDelay;

#if( ITA_DSP_VDL_DATA_LOG == 1 )
	VDLLogData oLogDataItem;
	oLogDataItem.fCurrentDelay     = fCurrentDelay;
	oLogDataItem.fNewDelay         = m_fNewDelay;
	oLogDataItem.fResamplingFactor = 1;
	oLogDataItem.iTargetBlockSize  = m_iBlockLength;
#endif

	// --= Keine �nderung der Verz�gerung (f�r rasant schnelle statische Szenen) =--

	if( fNewDelay == fCurrentDelay )
	{
		// Keine �nderung der Verz�gerung. Einfach Anfang der VDL in den Ausgang kopieren.
		int iReadCursor = ( m_iWriteCursor + m_iVDLBufferSize - (int)ceil( fCurrentDelay ) ) % m_iVDLBufferSize;
		psbOutput->cyclic_write( m_psbVDLBuffer, m_iBlockLength, iReadCursor, 0 );

		// Schreibzeiger um Blockl�nge vergr��ern (BlockPointerIncrement)
		m_iWriteCursor = ( m_iWriteCursor + m_iBlockLength ) % m_iVDLBufferSize;

#if( ITA_DSP_VDL_DATA_LOG == 1 )
		oLogDataItem.fProcessingTime = (float)( m_swProcess.stop( ) * 1.0e6 );
		m_oDataLog.log( oLogDataItem );
#endif

		return;
	}


	// --= �nderung der Verz�gerung =--

	// Zerlegen in Ganzzahl und Kommazahl
	int iCurrentIntDelay    = (int)ceil( fCurrentDelay );
	float fCurrentFracDelay = (float)iCurrentIntDelay - fCurrentDelay;
	assert( fCurrentFracDelay >= 0.0f ); // Subsample darf nicht negativ sein
	int iNewIntDelay    = (int)ceil( fNewDelay );
	float fNewFracDelay = (float)iNewIntDelay - fNewDelay;
	assert( fNewFracDelay >= 0.0f ); // Subsample darf nicht negativ sein
	int iDeltaDelay = iNewIntDelay - iCurrentIntDelay;

	// Falls Interpolation gew�nscht ist, Grenzen pr�fen
	if( ( eAlgorithm == EVDLAlgorithm::LINEAR_INTERPOLATION ) || ( eAlgorithm == EVDLAlgorithm::WINDOWED_SINC_INTERPOLATION ) ||
	    ( eAlgorithm == EVDLAlgorithm::CUBIC_SPLINE_INTERPOLATION ) )
	{
		/*
		 * Folgendes Problem kann bei der Abstandsverkleinerung auftreten:
		 * Ist die Abstandsverkleinerung zu schnell, werden Frequenzen so stark gestaucht,
		 * dass sie �ber die Nyquist-Grenze der Soundkarte hinaus wandern (22.05 kHz) und
		 * bei der Abtastung zu Alias-Fehlern f�hren. Es muss hierf�r das Eingangssignal
		 * entsprechend tiefpass gefiltert werden. Eine synchrone Abtastratenerh�hung
		 * kann auch sinnvoll sein. Diese muss nach der Interpolation erneut tp-gefiltert
		 * und entsprechend runtergesetzt werden.
		 *
		 * Folgendes Problem kann bei der Abstandsvergr��erung auftreten:
		 * Ist die Abstandsvergr��erung sehr schnell, werden im Grenzfall lediglich
		 * einige St�tzsamples zur Interpolation vieler Ausgangssamples genutzt,
		 * aber ein systembedingte Grenze existiert bis zur Schallgeschwindigkeit
		 * nicht.
		 *
		 * Kann die Interpolation die entsprechenden Frequenzen nicht nachbilden,
		 * treten Verzerrungen auf, bei der Linearinterpolation nicht-lineare Ver-
		 * zerrungen. Diese Fehler Verst�rken sich zunehmend.
		 *
		 */

		// Resamplingfaktor auf dem Eingangsstream bezogen auf eine Blockl�nge berechnen
		float fResamplingFactor = 1 - iDeltaDelay / (float)m_iBlockLength;

#if( ITA_DSP_VDL_DATA_LOG == 1 )
		oLogDataItem.fResamplingFactor = fResamplingFactor;
#endif

		// Wenn Voraussetzungen verletzt werden, f�r diesen Bearbeitungsschritt auf weiches Umschalten wechseln
		if( ( fResamplingFactor <= MIN_RESAMPLING_FACTOR ) || ( fResamplingFactor > MAX_RESAMPLING_FACTOR ) )
		{
			eAlgorithm = EVDLAlgorithm::CROSSFADE;
			std::cout << "VariableDelayLine: Forced crossfading, because resampling factor is out of bounds: r=" << fResamplingFactor
			          << "( min = " << MIN_RESAMPLING_FACTOR << ", max = " << MAX_RESAMPLING_FACTOR << " )" << std::endl;
		}
	}


	// --= Lesek�pfe =--

	// VDL-Puffergr��e wird nur addiert, damit
	int iReadCursorCurrent = ( m_iWriteCursor - iCurrentIntDelay + m_iVDLBufferSize ) % m_iVDLBufferSize;
	int iReadCursorNew     = ( m_iWriteCursor - iNewIntDelay + m_iVDLBufferSize ) % m_iVDLBufferSize;
	assert( iReadCursorCurrent >= 0 ); // Cursor darf nicht negativ sein
	assert( iReadCursorNew >= 0 );     // Cursor darf nicht negativ sein


	// --= Umschaltverfahren =--

	// TODO:  - vermutlich sehen die R�mpfe f�r jede andere Interpolation �hnlich aus, sodass man mit getOverlap() vereinheitlichen kann.
	//			(LinInterp schon fertig, aber erst Erfahrungen mit den anderen Verfahren sammeln...)

	int iSize;
	int iLeft, iRight; // �berlappung an den Grenzen
	switch( eAlgorithm )
	{
			// o Hartes Umschalten
		case EVDLAlgorithm::SWITCH:
			// Direkt neue Verz�gerung nehmen. Einfach kopieren. Keine R�cksicht nehmen.
			psbOutput->cyclic_write( m_psbVDLBuffer, m_iBlockLength, iReadCursorNew, 0 );

			break;

			// o Umschalten mittels Kreuzblende
		case EVDLAlgorithm::CROSSFADE:
			// Kreuzblende mittels tempor�rem Puffer
			assert( m_iFadeLength <= m_psbTemp->length( ) ); // Zu gro�e Blende f�r diesen Puffer
			m_psbTemp->cyclic_write( m_psbVDLBuffer, m_iFadeLength, iReadCursorCurrent, 0 );
			psbOutput->cyclic_write( m_psbVDLBuffer, m_iBlockLength, iReadCursorNew, 0 );

			psbOutput->Crossfade( m_psbTemp, 0, m_iFadeLength, ITABase::CrossfadeDirection::FROM_SOURCE, ITABase::FadingFunction::COSINE_SQUARE );

			break;


			// o Umschalten durch Stauchen oder Strecken: Lineare Interpolation, Kubische Spline-Interpolation oder gefensterte Sinc-Interpolation
		default:

			/* Zur Erkl�rung:

			    Verringerung der Verz�gerung => Samples stauchen
			    (Size > Blocklength), "Zeit" muss eingeholt werden
			    Resamplingfaktor > 1

			    Vergr��ern der Verz�gerung => Samples strecken
			    (Size < Blocklength), "Zeit" muss gedehnt werden
			    Resamplingfaktor < 1
			    */

			// �berlappung an den Grenzen holen
			m_pInterpolationRoutine->GetOverlapSamples( iLeft, iRight );

			// Neuer Abstand
			if( iNewIntDelay < iLeft )
				iSize = m_iBlockLength; // Kausalit�t erzwingen, wenn der Abstand kleiner als linker �berlappbereich ist
			else
				iSize = iCurrentIntDelay - iNewIntDelay + m_iBlockLength;

#if( ITA_DSP_VDL_DATA_LOG == 1 )
			oLogDataItem.iTargetBlockSize = iSize;
#endif

			// Sicherstellen, dass Daten des rechten �berlappungsbereichs vorhanden sind
			// (VDL Puffer sollte immer mindestens eine Blockl�nge gr��er sein als die
			//  aktuelle Verz�gerung)
			assert( iLeft < m_iBlockLength );                   // �berlapp links bereits gr��er als Blockl�nge
			assert( iRight < m_iBlockLength );                  // �berlapp rechts bereits gr��er als Blockl�nge
			assert( m_iVDLBufferSize > iNewIntDelay + iRight ); // Neue Verz�gerung und rechter �berlapp gr��er als VDL L�nge

			// "Size" Daten plus �berlappung aus dem Hauptpuffer vom momentanen Lesekopf an
			// den Anfang des Tempor�rpuffers kopieren
			assert( iLeft + iSize + iRight <= m_psbTemp->length( ) ); // iTempBufferBlockLength zu klein
			m_psbTemp->cyclic_write( m_psbVDLBuffer, iLeft + iSize + iRight, iReadCursorCurrent - iLeft, 0 );

			// Interpolieren: Tempor�rpuffer (Gr��e Size+Offset) --> Output (Gr��e Blockl�nge)
			// Input Offset = iLeft -> effektive Inputl�nge = iSize
			// iRight wird implizit durch die Interpolationsroutine verwendet, d.h.
			// bei iInputLength von iLeft+iSize wird auf weitere Daten zugegriffen, die
			// rechts davon liegen und im Tempor�rpuffer vorhanden sein m�ssen!
			int iInputLength      = iSize;
			int iInputStartOffset = iLeft;
			m_pInterpolationRoutine->Interpolate( m_psbTemp, iInputLength, iInputStartOffset, psbOutput, m_iBlockLength );

			for( int k = 0; k < psbOutput->GetLength( ); k++ )
			{
				if( std::isnan( psbOutput->GetData( )[k] ) )
				{
					std::stringstream ss;
					ss << "PANIC! VDL produced NAN value at output sample " << k << std::endl;
					ss << "\t"
					   << "Switching from " + IntToString( iCurrentIntDelay ) + " to " + IntToString( iNewIntDelay ) << std::endl;
					ss << "\t"
					   << "Write cursor: " << m_iWriteCursor << std::endl;
					ss << "\t"
					   << "Read cursor current: " << iReadCursorCurrent << std::endl;
					ss << "\t"
					   << "Read cursor new:     " << iReadCursorNew << std::endl;
					ss << "\t"
					   << "Interpolation routine: " << m_pInterpolationRoutine->GetName( ) << std::endl;

					int iNANsInInputBuffer = 0;
					for( int k1 = 0; k1 < psbOutput->GetLength( ); k1++ )
					{
						if( std::isnan( m_psbTemp->GetData( )[k] ) )
							iNANsInInputBuffer++;
					}
					ss << "\t"
					   << "NaN values in input buffer: " << iNANsInInputBuffer << std::endl;

					ITA_EXCEPT1( INVALID_PARAMETER, ss.str( ).c_str( ) );
				}
			}

			break;


	} // end case switch

	// Neue Verz�gerung speichern
	m_fCurrentDelay = fNewDelay;

	// Schreibzeiger inkrementieren
	// (Hinweis: Der Schreibzeiger ist immer Vielfaches der Blockl�nge)
	m_iWriteCursor = ( m_iWriteCursor + m_iBlockLength ) % m_iVDLBufferSize;

	// Zeitnahme
	double t = m_swProcess.stop( );
	if( t > m_iBlockLength / m_dSampleRate )
	{
		DEBUG_PRINTF( " * [VDL] Overload, processing took %.2f ms\n", t * 1e3 );
		m_iNumberOfDropouts++;
	}

#if( ITA_DSP_VDL_DATA_LOG == 1 )
	oLogDataItem.fProcessingTime = (float)( t * 1e6 );
	m_oDataLog.log( oLogDataItem );
#endif

	return;
}

#if( ITA_DSP_VDL_DATA_LOG == 1 )
std::ostream& CITAVariableDelayLine::VDLLogData::outputDesc( std::ostream& os )
{
	os << "Current delay"
	   << "\t"
	   << "New delay"
	   << "\t"
	   << "Resampling factor"
	   << "\t"
	   << "Target block size"
	   << "\t"
	   << "Processing time [us]" << std::endl;
	return os;
}

std::ostream& CITAVariableDelayLine::VDLLogData::outputData( std::ostream& os ) const
{
	os << fCurrentDelay << "\t" << fNewDelay << "\t" << fResamplingFactor << "\t" << iTargetBlockSize << "\t" << fProcessingTime << std::endl;
	return os;
}
#endif

float CITAVariableDelayLine::GetDelayTime( ) const
{
	return m_fCurrentDelay;
}

float CITAVariableDelayLine::GetNewDelayTime( ) const
{
	return float( m_fNewDelay / m_dSampleRate );
}
