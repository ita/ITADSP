#include <ITAStatefulSmoothedGain.h>
#include <cassert>
#include <cstring>

ITA::DSP::StatefulSmoothedGain::StatefulSmoothedGain( double initialGain, FadingMode fadingMode )
    : m_currentGain( initialGain )
    , m_newGain( initialGain )
    , m_fadingMode( fadingMode )
{
}

void ITA::DSP::StatefulSmoothedGain::SetGain( double gain, bool forceInstant )
{
	m_newGain = gain;
	if (forceInstant)
		m_currentGain = m_newGain;
}

void ITA::DSP::StatefulSmoothedGain::Process( const ITASampleBuffer& inputBlock, ITASampleBuffer& outputBlock )
{
	if( &inputBlock != &outputBlock )
	{
		assert( inputBlock.GetLength( ) == outputBlock.GetLength( ) );
		std::memcpy( outputBlock.GetData( ), inputBlock.GetData( ), sizeof(float) * inputBlock.GetLength( ) );
	}

	if( m_newGain == m_currentGain )
	{
		ApplyConstantGain( outputBlock );
		return;
	}

	ApplyInterpolatedGain( outputBlock );
	m_currentGain = m_newGain;
}

void ITA::DSP::StatefulSmoothedGain::Process( ITASampleBuffer& audioBlock )
{
	Process( audioBlock, audioBlock );
}

void ITA::DSP::StatefulSmoothedGain::ApplyConstantGain( ITASampleBuffer& audioBlock )
{
	for( int idx = 0; idx < audioBlock.GetLength( ); idx++ )
		audioBlock[idx] *= (float)m_newGain;
}

void ITA::DSP::StatefulSmoothedGain::ApplyInterpolatedGain( ITASampleBuffer& audioBlock )
{
	FadingFunction::FadingFunction fadingFunction = FadingFunction::FadingFunction::COSINE_SQUARE;

	if (m_fadingMode == FadingMode::LINEAR)
	{
		fadingFunction = FadingFunction::FadingFunction::LINEAR;
	}
	else if( m_fadingMode == FadingMode::SMART_COSINE && std::abs( m_newGain ) < 1e-6 ) // fading towards zero
	{
		fadingFunction = FadingFunction::FadingFunction::COSINE;
	}
	else if( m_fadingMode == FadingMode::SMART_COSINE && std::abs( m_currentGain ) < 1e-6 ) // fading from zero
	{
		fadingFunction = FadingFunction::FadingFunction::COSINE;
	}

	const int nSamples = audioBlock.GetLength( );

	for( int idx = 0; idx < nSamples; idx++ )
	{
		const auto fadingValue = static_cast<float>( idx ) / nSamples;
		const auto currentGain = static_cast<float>( m_currentGain );
		const auto newGain     = static_cast<float>( m_newGain );
		const auto gainFactor  = currentGain * FadingFunction::fading_func( fadingValue, FadingFunction::FadingDirection::FADE_OUT, fadingFunction ) +
		                        newGain * FadingFunction::fading_func( fadingValue, FadingFunction::FadingDirection::FADE_IN, fadingFunction );
		audioBlock[idx] *= gainFactor;
	}
}
