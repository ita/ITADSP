#include "ITAThirdOctaveFilterbankIIRCoefficients.h"

#include <ITAException.h>
#include <ITAFastMath.h>
#include <ITAStopWatch.h>
#include <ITAThirdOctaveFilterbankIIR.h>
#include <cassert>

CITAThirdOctaveFilterbankIIR::CITAThirdOctaveFilterbankIIR( const double dSampleRate, const int iBlockLength )
    : m_dSampleRate( dSampleRate )
    , m_iBlockLength( iBlockLength )
    , m_nBandsInternal( FB_NUM_BANDS )
    , m_nBiquadsPerBand( FB_NUM_BIQUADS_PER_BAND )
    , m_pfTempFilterBuf( nullptr )
    , m_pfTempOutputBuf( nullptr )
{
	if( dSampleRate != FB_SAMPLINGRATE )
		ITA_EXCEPT1( INVALID_PARAMETER, "Filterbank does not support this samplingrate" );

	// Initialize biquads
	int nBiquads = m_nBandsInternal * m_nBiquadsPerBand;
	m_vBiquads.resize( nBiquads );
	for( int i = 0; i < m_nBandsInternal; i++ )
	{
		for( int j = 0; j < m_nBiquadsPerBand; j++ )
		{
			m_vBiquads[i * m_nBiquadsPerBand + j].oParams.SetParameters( FB_BIQUAD_PARAMS[i][j] );
		}
	}

	m_pfTempFilterBuf = fm_falloc( m_iBlockLength, true );
	m_pfTempOutputBuf = fm_falloc( m_iBlockLength, true );
}

CITAThirdOctaveFilterbankIIR::~CITAThirdOctaveFilterbankIIR( )
{
	fm_free( m_pfTempFilterBuf );
	fm_free( m_pfTempOutputBuf );
}

int CITAThirdOctaveFilterbankIIR::GetLatency( ) const
{
	return 0; // [stienen] sicher?
}

void CITAThirdOctaveFilterbankIIR::SetGains( const ITABase::CThirdOctaveGainMagnitudeSpectrum& oGains, bool bSmoothChangeover )
{
	GainUpdate oUpdate;
	oUpdate.oGains        = oGains;
	oUpdate.iBlendSamples = ( bSmoothChangeover ? 1 : 0 );
	m_qGains.push( oUpdate );
}

void CITAThirdOctaveFilterbankIIR::SetMagnitudes( const ITABase::CThirdOctaveGainMagnitudeSpectrum& oGains, const bool bSmoothChangeover /*= true */ )
{
	SetGains( oGains, bSmoothChangeover );
}

void CITAThirdOctaveFilterbankIIR::Clear( )
{
	for( int i = 0; i < (int)m_vBiquads.size( ); i++ )
		m_vBiquads[i].ClearAccumulators( );
}

void CITAThirdOctaveFilterbankIIR::Process( const float* pfInputSamples, float* pfOutputSamples )
{
	assert( pfInputSamples != nullptr );
	assert( pfOutputSamples != nullptr );

	// F�r Schleifen unten
	assert( m_nBiquadsPerBand >= 2 );

	// TODO: Fehler l�sen. Warum kommt aus den hohen B�ndern nix? Scheinen nicht stabil zu sein?!
	const int iMaxBand = m_nBandsInternal - 1;
	// const int iMaxBand = 10;
	const int k = m_nBiquadsPerBand - 1; // Index des letzten Biquads pro Band

	if( m_nBandsInternal == 0 )
		return; // [stienen] lieber assert? Filter ohne B�nder?

	// Gains �bernehmen
	GainUpdate oUpdate;
	bool bUpdateGains = false;
	while( m_qGains.try_pop( oUpdate ) )
	{
		bUpdateGains = true;
	}

	if( bUpdateGains )
	{
		bool bSwitchGains = ( oUpdate.iBlendSamples == 0 );

		if( bSwitchGains )
		{
			for( int i = 0; i < iMaxBand; i++ )
			{
				// Erster Biquad in der Sequenz: input => tmp
				m_vBiquads[i * m_nBiquadsPerBand + 0].Process( pfInputSamples, m_pfTempFilterBuf, m_iBlockLength );

				// Biquads dazwischen: tmp => tmp
				for( int j = 1; j < k; j++ )
					m_vBiquads[i * m_nBiquadsPerBand + j].Process( m_pfTempFilterBuf, m_pfTempFilterBuf, m_iBlockLength );

				// Letztes Biquad mit Gain: tmp => output
				m_vBiquads[i * m_nBiquadsPerBand + k].Process( m_pfTempFilterBuf, m_pfTempOutputBuf, m_iBlockLength, oUpdate.oGains[i],
				                                               ( i == 0 ? ITABase::MixingMethod::OVERWRITE : ITABase::MixingMethod::ADD ) );
			}
		}
		else
		{
			// Smooth gain changeover
			for( int i = 0; i < iMaxBand; i++ )
			{
				// Erster Biquad in der Sequenz: input => tmp
				m_vBiquads[i * m_nBiquadsPerBand + 0].Process( pfInputSamples, m_pfTempFilterBuf, m_iBlockLength );

				// Biquads dazwischen: tmp => tmp
				for( int j = 1; j < k; j++ )
					m_vBiquads[i * m_nBiquadsPerBand + j].Process( m_pfTempFilterBuf, m_pfTempFilterBuf, m_iBlockLength );

				// Letztes Biquad mit Gain: tmp => output
				m_vBiquads[i * m_nBiquadsPerBand + k].Process( m_pfTempFilterBuf, m_pfTempOutputBuf, m_iBlockLength, m_oGainsInternal[i], oUpdate.oGains[i],
				                                               ( i == 0 ? ITABase::MixingMethod::OVERWRITE : ITABase::MixingMethod::ADD ) );
			}
		}

		m_oGainsInternal = oUpdate.oGains;
	}
	else
	{
		// No change in gains
		for( int i = 0; i < iMaxBand; i++ )
		{
			// Erster Biquad in der Sequenz: input => tmp
			m_vBiquads[i * m_nBiquadsPerBand + 0].Process( pfInputSamples, m_pfTempFilterBuf, m_iBlockLength );

			// Biquads dazwischen: tmp => tmp
			for( int j = 1; j < k; j++ )
				m_vBiquads[i * m_nBiquadsPerBand + j].Process( m_pfTempFilterBuf, m_pfTempFilterBuf, m_iBlockLength );

			// Letztes Biquad mit Gain: tmp => output
			m_vBiquads[i * m_nBiquadsPerBand + k].Process( m_pfTempFilterBuf, m_pfTempOutputBuf, m_iBlockLength, m_oGainsInternal[i],
			                                               ( i == 0 ? ITABase::MixingMethod::OVERWRITE : ITABase::MixingMethod::ADD ) );
		}
	}

	// Ausgabe bereitstellen
	memcpy( pfOutputSamples, m_pfTempOutputBuf, m_iBlockLength * sizeof( float ) );
}
