#include <ITADSP/ThirdOctaveFilterbank/IIRBurg.h>
#include <ITAThirdOctaveFilterbank.h>
#include <ITAThirdOctaveFilterbankFIR.h>
#include <ITAThirdOctaveFilterbankIIR.h>
#include <cassert>

CITAThirdOctaveFilterbank* CITAThirdOctaveFilterbank::Create( const double dSampleRate, const int iBlockLength, const int iMethod )
{
	switch( iMethod )
	{
		case FIR_SPLINE_LINEAR_PHASE:
			return new CITAThirdOctaveFilterbankFIR( dSampleRate, iBlockLength );
		case IIR_BIQUADS_ORDER10:
			return new CITAThirdOctaveFilterbankIIR( dSampleRate, iBlockLength );
		case IIR_BURG_ORDER4:
			return new ITADSP::ThirdOctaveFilterbank::CIIRBurg( dSampleRate, iBlockLength, 4 );
		case IIR_BURG_ORDER10:
			return new ITADSP::ThirdOctaveFilterbank::CIIRBurg( dSampleRate, iBlockLength, 10 );
		default:
			assert( false );
			return nullptr;
	}
}

std::unique_ptr<CITAThirdOctaveFilterbank> CITAThirdOctaveFilterbank::CreateUnique(const double dSampleRate, const int iBlockLength, const int iMethod)
{
	switch( iMethod )
	{
		case FIR_SPLINE_LINEAR_PHASE:
			return std::make_unique<CITAThirdOctaveFilterbankFIR>( dSampleRate, iBlockLength );
		case IIR_BIQUADS_ORDER10:
			return std::make_unique<CITAThirdOctaveFilterbankIIR>( dSampleRate, iBlockLength );
		case IIR_BURG_ORDER4:
			return std::make_unique<ITADSP::ThirdOctaveFilterbank::CIIRBurg>( dSampleRate, iBlockLength, 4 );
		case IIR_BURG_ORDER10:
			return std::make_unique<ITADSP::ThirdOctaveFilterbank::CIIRBurg>( dSampleRate, iBlockLength, 10 );
		default:
			assert( false );
			return nullptr;
	}
}
