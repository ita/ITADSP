﻿#include <ITAIIRFilterGenerator.h>
#include <ITAConstants.h>
#include <ITAException.h>
#include <algorithm>
#include <iostream>


void ITADSP::IIRFilterGenerator::Yulewalk( const ITABase::CFiniteImpulseResponse &oIR, CFilterCoefficients &oCoeffs )
{
	int na = oCoeffs.vfDenominator.size( );
	int n  = oIR.GetLength( );
	int n2 = floor( ( n + 1 ) / 2 );
	int nb = na;
	int nr = 4 * na;
	// nt = 0:nr-1

	// pick nr coefficients
	ITASampleBuffer R( nr, false );
	for( int i = 0; i < nr; i++ )
		R.GetData( )[i] = oIR.GetData( )[i] * ( 0.54 + 0.46 * cos( ITAConstants::PI_F * i / ( nr - 1 ) ) );


	// window used t extract right wing of two sided covaianc sequence
	ITASampleBuffer Rwindow( n, true );
	Rwindow[0] = 0.5f;
	for( int i = 1; i <= n2; i++ )
		Rwindow[i] = 1.0f;

	// find roots of

	float *A = new float( oCoeffs.vfDenominator.size( ) );

	denf( A, R, na );
}


void ITADSP::IIRFilterGenerator::denf( float *A, const ITASampleBuffer &R, int na )
{
	// output coefficients A of length na + 1, input vector R, and order na

	float **out;
	out = new float *[R.GetLength( ) - na - 1];
	for( int i = 0; i < R.GetLength( ) - na - 1; i++ )
	{
		out[i] = new float[na];
	}

	toeplitz( out, R, na );

	for( int i = 0; i < R.GetLength( ) - na - 1; i++ )
	{
		for( int j = 0; j < na; j++ )
		{
			std::cout << out[i][j] << '\t';
		}
		std::cout << '\n';
	}
}


void ITADSP::IIRFilterGenerator::toeplitz( float **out, const ITASampleBuffer &row, const ITASampleBuffer &column )
{
	// return a toeplitz matrix out constructed from input vectors row and column

	// initialise 2d array out to size [column][row]
	out = new float *[column.GetLength( )];
	for( int i = 0; i < column.GetLength( ); i++ )
	{
		out[i] = new float[row.GetLength( )];
	}

	int test = std::min( row.GetLength( ), column.GetLength( ) );

	// set the upper diagonal (excluding leading edge)
	for( int i = 1; i < row.GetLength( ); i++ )
	{
		for( int j = 0; j < test - i; j++ )
			out[j][j + i] = row[i];
	}

	// set lower diagonal (including leading edge)
	for( int i = 0; i < column.GetLength( ); i++ )
	{
		for( int j = 0; j < test - i; j++ )
			out[j + i][j] = column[i];
	}

	return;
}

void ITADSP::IIRFilterGenerator::toeplitz( float **out, const ITASampleBuffer &in, int na )
{
	// return a toeplitz matrix out constructed from input vectors row and column
	/*
	//initialise 2d array out to size [in.GetLength()-na-1][na]
	out = new float*[in.GetLength() - na - 1];
	for (int i = 0; i < in.GetLength() - na - 1; i++) {
	out[i] = new float[na];
	}
	*/

	// set the upper diagonal (excluding leading edge)
	for( int i = 1; i < na; i++ )
	{
		for( int j = 0; j < na - i; j++ )
			out[j][j + i] = in[na - i];
	}

	// set lower diagonal (including leading edge)
	for( int i = 0; i < in.GetLength( ) - na - 1; i++ )
	{
		for( int j = 0; j < na - i; j++ )
			out[j + i][j] = in[na + i - 1];
	}

	return;
}


void ITADSP::IIRFilterGenerator::Burg( const ITABase::CFiniteImpulseResponse &oIR, CFilterCoefficients &oCoeffs )
{
	// oIR -> targe impulse response -> x,     filter order = 	oCoeffs.uiOrder -> p
	float k, k_numerator, k_denominator;
	int buffer_length = oIR.GetLength( ) - 1;
	ITASampleBuffer efp( buffer_length );
	ITASampleBuffer ebp( buffer_length );
	ITASampleBuffer efp_temp( buffer_length );

	oIR.read( efp.GetData( ), buffer_length, 1 ); // read oIR 1:end
	oIR.read( ebp.GetData( ), buffer_length, 0 ); // read oIR 0:end - 1

	std::vector<float> a_temp;
	a_temp.resize( oCoeffs.uiOrder + 1 );

	for( auto it = oCoeffs.vfDenominator.begin( ); it != oCoeffs.vfDenominator.end( ); it++ )
		*it = 0.0f;

	oCoeffs.vfNumerator[0]   = InnerProduct( oIR.GetData( ), oIR.GetData( ), oIR.GetLength( ) ) / oIR.GetLength( );
	oCoeffs.vfDenominator[0] = 1.0f;
	oCoeffs.bIsARMA          = false;
	oCoeffs.iDesignAlgorithm = ITADSP::BURG; // record that the Burg algorithm was used to generate the coefficients

	for( int m = 0; m < oCoeffs.uiOrder; m++ )
	{
		k_numerator   = ( -2 * InnerProduct( ebp.GetData( ), efp.GetData( ) + m, buffer_length ) );
		k_denominator = ( InnerProduct( efp.GetData( ) + m, efp.GetData( ) + m, buffer_length ) + InnerProduct( ebp.GetData( ), ebp.GetData( ), buffer_length ) );
		if( k_denominator != 0.0f )
			k = k_numerator / k_denominator;
		else
			k = 0.0f;

		buffer_length--;

		efp_temp = efp;
		efp_temp.MulAdd( ebp, k, 0, m, buffer_length ); // makes sure efp and abp are added in correct alignment
		ebp.MulAdd( efp, k, m, 0, buffer_length );
		efp = efp_temp;

		for( int j = 0; j <= m; j++ )
			a_temp[j + 1] = oCoeffs.vfDenominator[j + 1] + ( k * oCoeffs.vfDenominator[m - j] );

		for( int i = 1; i <= m + 1; i++ )
			oCoeffs.vfDenominator[i] = a_temp[i];

		oCoeffs.vfNumerator[0] = ( 1.0f - pow( k, 2 ) ) * oCoeffs.vfNumerator[0];
	}
	if( oCoeffs.vfNumerator[0] < 0 )
		ITA_EXCEPT_INVALID_PARAMETER( "[ITADSP::IIRFilterGenerator::Burg]: Trying to take a square-root of a negative value." );
	oCoeffs.vfNumerator[0] = sqrt( oCoeffs.vfNumerator[0] * (float)oIR.GetLength( ) );
}


float ITADSP::IIRFilterGenerator::InnerProduct( const float *x, const float *y, const int length )
{
	// calculate the inner product of two sample buffer inputs. Lenghts of the inputs must be equal

	float out = 0.0f;
	for( int i = 0; i < length; i++ )
		out += x[i] * y[i];

	return out;
}