#include <ITADSP/PD/JetEngine.h>

ITADSP::PD::CJetEngine::CJetEngine( double dSampleRate, float fRPMInit, bool bColdStart ) : m_dSampleRate( dSampleRate )
{
	m_vfTurbineBaseModeFrequencies = { 3097.0f, 4495.0f, 5588.0f, 7414.0f, 11000.0f };
	m_vfTurbineBaseModeAmplitudes  = { 0.25f, 0.25f, 1.0f, 0.4f, 0.4f };

	m_vfRPMRange = { 1000.0f, 5000.0f };

	const float fCenterFrequency1 = 8000.0f;
	const float fQ1               = 0.5f;
	m_oForcedFlameBP1.setup( 1, float( dSampleRate ), fCenterFrequency1, fCenterFrequency1 / fQ1 );

	const float fCutoffFrequency1 = 120.0f;
	m_oForcedFlameHP1.setup( 1, float( dSampleRate ), fCutoffFrequency1 );

	const float fLPCutoffFrequency1 = 11000.0f;
	m_oJetEngineLP1.setup( 1, float( dSampleRate ), fLPCutoffFrequency1 );

	const float fLPCutoffFrequency2 = 1.0f;
	m_oJetEngineLP2.setup( 1, float( dSampleRate ), fLPCutoffFrequency2 );

	oCtxAudio.vfTurbineModePhaseShift  = { 0, 0, 0, 0, 0 };
	oCtxAudio.vfTurbineModeFrequencies = { 0, 0, 0, 0, 0 };

	SetRPM( fRPMInit );

	if( !bColdStart )
	{
		// Run input low-pass for 3 secs (until 0.2 Hz LP died away on unit step function) to akkumulate
		// of the default input, otherwise there is a run-up from zero to target RPM value
		const float fNormalizedRPMInit = GetRPMNormalized( oCtxAudio.vfRPM );
		float fNormalizedRPMUpsample;
		float* pfNormalizedRPMUpsampleAlias = &fNormalizedRPMUpsample;
		for( int n = 0; n < (int)dSampleRate * 3; n++ )
		{
			fNormalizedRPMUpsample = fNormalizedRPMInit;
			m_oJetEngineLP2.process( 1, &pfNormalizedRPMUpsampleAlias );
		}
	}
};

ITADSP::PD::CJetEngine::~CJetEngine( ) {}

void ITADSP::PD::CJetEngine::SetRPM( float fRPM )
{
	oCtxAudio.vfRPM = fRPM; // Direct write-through, no lock & swap implemented here
}

float ITADSP::PD::CJetEngine::GetRPMNormalized( float fRPM ) const
{
	// Validate input range
	assert( m_vfRPMRange.size( ) == 2 );
	const float fValidRPM      = std::min( std::max( m_vfRPMRange[0], fRPM ), m_vfRPMRange[1] );
	const float fNormalizedRPM = std::max( 0.1f, fValidRPM / m_vfRPMRange[1] );
	return fNormalizedRPM;
}

void ITADSP::PD::CJetEngine::Process( float* pfOutputBuffer, int iNumSamples )
{
	/* Validate, normalize and up-sampled RPM and apply low-pass filter */

	// Processed DSP coefficient with normalized input
	const float fNormalizedRPMControl = GetRPMNormalized( oCtxAudio.vfRPM );
	float fNormalizedRPMControlUpscaled;

	// We need this ref pointer for Dsp functions, but its ugly we dont use it apart from that
	float* pfTempSampleAlias           = &m_fTempSample;
	float* pfNormalizedRPMControlAlias = &fNormalizedRPMControlUpscaled;

	for( int n = 0; n < iNumSamples; n++ )
	{
		// Low-pass input
		fNormalizedRPMControlUpscaled = fNormalizedRPMControl;
		m_oJetEngineLP2.process( 1, &pfNormalizedRPMControlAlias );
		fNormalizedRPMControlUpscaled = *pfNormalizedRPMControlAlias;

		/* Update DSP network coefficients */

		float& fCurrentSample( pfOutputBuffer[n] );

		// Update turbine frequencies
		assert( m_vfTurbineBaseModeFrequencies.size( ) == oCtxAudio.vfTurbineModeFrequencies.size( ) );
		for( int i = 0; i < m_vfTurbineBaseModeFrequencies.size( ); i++ )
			oCtxAudio.vfTurbineModeFrequencies[i] = m_vfTurbineBaseModeFrequencies[i] * fNormalizedRPMControlUpscaled;

		// Update forced flame filters
		const float fCenterFrequency2 = fNormalizedRPMControlUpscaled * fNormalizedRPMControlUpscaled * 150.0f;
		m_oForcedFlameBP2.setup( 1, float( m_dSampleRate ), fCenterFrequency2, fCenterFrequency2 / 1.0f );

		const float fCenterFrequency3 = fNormalizedRPMControlUpscaled * 12000.0f;
		const float fQ3               = 0.6f;
		m_oForcedFlameBP3.setup( 1, float( m_dSampleRate ), fCenterFrequency3, fCenterFrequency3 / fQ3 );


		/* Process DSP */

		// Forced flame

		m_fTempSample = oRNG.GenerateFloat( -1.0f, 1.0f ); // noise~

		m_oForcedFlameBP1.process( 1, &pfTempSampleAlias ); // bd~ 8000 0.5

		m_oForcedFlameBP2.process( 1, &pfTempSampleAlias ); // vcf~ 0 1 (real part only = band pass)

		m_oForcedFlameHP1.process( 1, &pfTempSampleAlias ); // hip~ 120

		m_fTempSample *= 120.0f; // *~ 120

		m_fTempSample = ( m_fTempSample < -1.0f ) ? -1 : ( ( m_fTempSample > 1 ) ? 1 : m_fTempSample ); // clip~ -1 1

		m_oForcedFlameBP3.process( 1, &pfTempSampleAlias ); // vcf~ 0 0.6 (real part only = band pass)

		fCurrentSample = m_fTempSample; // override buffer


		/* Turbine (adds to output buffer) */

		m_fTempSample = 0.0f;

		assert( oCtxAudio.vfTurbineModeFrequencies.size( ) == oCtxAudio.vfTurbineModePhaseShift.size( ) );
		assert( oCtxAudio.vfTurbineModeFrequencies.size( ) == m_vfTurbineBaseModeAmplitudes.size( ) );
		for( int i = 0; i < oCtxAudio.vfTurbineModeFrequencies.size( ); i++ )
		{
			const float& fFrequency( oCtxAudio.vfTurbineModeFrequencies[i] );
			const float& fPhaseShift( oCtxAudio.vfTurbineModePhaseShift[i] );
			float fAmplitude( m_vfTurbineBaseModeAmplitudes[i] );

			const float t = fmodf( ITAConstants::TWO_PI_F_L * fFrequency * float( n ) / float( m_dSampleRate ), ITAConstants::TWO_PI_F );

			// [MANUAL MODIFICATION] sine-sqared roll-in of amplitude for the frequencies, otherwise very synthetic
			if( fNormalizedRPMControlUpscaled < 0.1 )
				fAmplitude *=
				    sin( ITAConstants::HALF_PI_F * fNormalizedRPMControlUpscaled / 0.1f ) * sin( ITAConstants::HALF_PI_F * fNormalizedRPMControlUpscaled / 0.1f );

			m_fTempSample += fAmplitude * sin( t + fPhaseShift ); // all osc~ and *~
		}

		m_fTempSample = ( m_fTempSample < -0.9f ) ? -0.9f : ( ( m_fTempSample > 0.9f ) ? 0.9f : m_fTempSample ); // clip~ -0.9 0.9


		/* Jet engine */

		m_fTempSample *= 0.5f; //  *~ 0.5

		const float fManualBalance = 0.1f / 0.5f;                        // [MANUAL MODIFICATION] not included in patch
		m_fTempSample = fCurrentSample + fManualBalance * m_fTempSample; // combine turbine and flame from jet engine patch (with a manual balance that sound better)

		m_oJetEngineLP1.process( 1, &pfTempSampleAlias ); // ~lop 11000

		fCurrentSample = 0.2f * m_fTempSample; // *~0.2
	}

	/* Update phases for turbine */

	for( int i = 0; i < oCtxAudio.vfTurbineModeFrequencies.size( ); i++ )
	{
		const float& fFrequency( oCtxAudio.vfTurbineModeFrequencies[i] );
		float& fPhaseShift( oCtxAudio.vfTurbineModePhaseShift[i] );

		fPhaseShift = fmodf( ITAConstants::TWO_PI_F_L * fFrequency / float( m_dSampleRate ) * float( iNumSamples ) + fPhaseShift, ITAConstants::TWO_PI_F );
	}
}
