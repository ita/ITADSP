#include <ITADSP/ThirdOctaveFilterbank/IIRBurg.h>
#include <ITAIIRFilterEngine.h>
#include <ITAIIRFilterGenerator.h>
#include <ITAThirdOctaveFIRFilterGenerator.h>
#include <algorithm>
//#include "ITAIIRUtils.h" // rm me

using namespace ITADSP::ThirdOctaveFilterbank;

CIIRBurg::CIIRBurg( const double dSampleRate, const int iBlockLength, const int iFilterOrder /* = 4 */ ) : m_iBlockLength( iBlockLength )
{
	m_pFilterEngine    = new CITAIIRFilterEngine( iFilterOrder );
	int iFIRGenLength  = int( 4 * ceil( dSampleRate / ITABase::CThirdOctaveMagnitudeSpectrum::GetCenterFrequencies( )[0] ) );
	m_pFilterGenerator = new CITAThirdOctaveFIRFilterGenerator( dSampleRate, iFIRGenLength );
}

CIIRBurg::~CIIRBurg( )
{
	delete m_pFilterGenerator;
	delete m_pFilterEngine;
}

void CIIRBurg::SetMagnitudes( const ITABase::CThirdOctaveGainMagnitudeSpectrum& oGains, const bool )
{
	ITABase::CFiniteImpulseResponse oIR( m_pFilterGenerator->GetFilterLength( ), (float)m_pFilterGenerator->GetSampleRate( ), true );
	m_pFilterGenerator->GenerateFilter( oGains, oIR.GetData( ), true );

	// oIR.Store( "iirburg_fb_ir_input.wav" );

	CFilterCoefficients oCoeffs( m_pFilterEngine->GetOrder( ) );
	ITADSP::IIRFilterGenerator::Burg( oIR, oCoeffs );

	// ITADSP::ExportIIRCoefficientsToJSON( "iirburg_fb_coeffs.json", oCoeffs );

	m_pFilterEngine->SetCoefficients( oCoeffs );
}

void CIIRBurg::Clear( )
{
	m_pFilterEngine->ClearAccumulators( );
}

void ITADSP::ThirdOctaveFilterbank::CIIRBurg::Process( const float* pfInputSamples, float* pfOutputSamples )
{
	m_pFilterEngine->Process( pfInputSamples, pfOutputSamples, m_iBlockLength );
}
