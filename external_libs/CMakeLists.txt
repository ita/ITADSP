CPMAddPackage (
	NAME DSPFilters
	GITHUB_REPOSITORY vinniefalco/DSPFilters
	GIT_TAG master
	DOWNLOAD_ONLY ON
)

if (DSPFilters_ADDED)
	find_package (Git 2.29 REQUIRED)
	execute_process (
		COMMAND ${GIT_EXECUTABLE} apply --check "${CMAKE_CURRENT_LIST_DIR}/patches/namspace_alias.patch"
		RESULT_VARIABLE patch_ok
		WORKING_DIRECTORY ${DSPFilters_SOURCE_DIR}
		ERROR_QUIET
	)

	if (${patch_ok} STREQUAL "0")
		execute_process (
			COMMAND ${GIT_EXECUTABLE} apply --whitespace=fix
					"${CMAKE_CURRENT_LIST_DIR}/patches/namspace_alias.patch"
			RESULT_VARIABLE patch_ok
			WORKING_DIRECTORY ${DSPFilters_SOURCE_DIR}
		)
	endif ()

	file (GLOB_RECURSE DSPFilters_sources ${DSPFilters_SOURCE_DIR}/shared/DSPFilters/source/**.cpp
		  ${DSPFilters_SOURCE_DIR}/shared/DSPFilters/include/DspFilters/**.h
	)

	add_library (DSPFilters STATIC ${DSPFilters_sources})
	add_library (DSPFilters::DSPFilters ALIAS DSPFilters)

	target_include_directories (
		DSPFilters PUBLIC $<BUILD_INTERFACE:${DSPFilters_SOURCE_DIR}/shared/DSPFilters/include>
						  $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/DspFilters>
	)

	set_property (TARGET DSPFilters PROPERTY FOLDER "external_libs")

	packageProject (
		NAME DSPFilters
		VERSION 1.0.0
		BINARY_DIR ${PROJECT_BINARY_DIR}
		INCLUDE_DIR ${DSPFilters_SOURCE_DIR}/shared/DSPFilters/include
		INCLUDE_DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/DspFilters
		NAMESPACE DSPFilters
		DISABLE_VERSION_SUFFIX YES
	)
endif ()
