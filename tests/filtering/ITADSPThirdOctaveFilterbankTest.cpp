﻿#include <ITAThirdOctaveFilterbank.h>

#include <ITAAudiofileWriter.h>
#include <ITANumericUtils.h>
#include <ITASampleBuffer.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>
#include <ITAThirdOctaveMagnitudeSpectrum.h>

#ifdef WITH_JSON_SUPPORT
#	include <ITABase/UtilsJSON.h>
#endif

#include <iostream>

using namespace std;
using namespace ITABase;

const int g_iSampleLength  = ( 1 << 17 );
const int g_iBlockLength   = 128;
const double g_dSampleRate = 44100;
const int g_iFIRGenLength  = int( 4 * ceil( g_dSampleRate / CThirdOctaveMagnitudeSpectrum::GetCenterFrequencies( )[0] ) );
ITAStopWatch sw;
ITASampleBuffer sbImpulseResponse( g_iSampleLength );
ITASampleBuffer sbDirac( g_iSampleLength, true );

ITABase::CThirdOctaveGainMagnitudeSpectrum oZero;
ITABase::CThirdOctaveGainMagnitudeSpectrum oIdentity;
ITABase::CThirdOctaveGainMagnitudeSpectrum oSomeBands;
ITABase::CThirdOctaveGainMagnitudeSpectrum oHighPass;
ITABase::CThirdOctaveGainMagnitudeSpectrum oDirectivity;
ITABase::CThirdOctaveGainMagnitudeSpectrum oReflection;
ITABase::CThirdOctaveGainMagnitudeSpectrum oDiffraction;

void Run( CITAThirdOctaveFilterbank* pFilterBank, std::string sExportName );

int main( int, char** )
{
	// Initialize

	sbDirac[0] = 1.0f;

	oZero.SetZero( );
#ifdef WITH_JSON_SUPPORT
	ITABase::Utils::JSON::Export( &oZero, "ZeroSpectrum.json" );
#endif

	oIdentity.SetIdentity( );
	oIdentity.Multiply( 1.000001f );
#ifdef WITH_JSON_SUPPORT
	ITABase::Utils::JSON::Export( &oIdentity, "IdentitySpectrum.json" );
#endif

	oHighPass.SetIdentity( );
	for( int i = 15; i >= 0; i-- )
		oHighPass[i] = powf( 0.5f, 15.0f - float( i ) );
#ifdef WITH_JSON_SUPPORT
	ITABase::Utils::JSON::Export( &oHighPass, "HighPassSpectrum.json" );
#endif

	oSomeBands.SetZero( );
	oSomeBands[3]  = (float)db10_to_ratio( 3.0f );
	oSomeBands[10] = (float)db10_to_ratio( -3.0f );
	oSomeBands[11] = (float)db10_to_ratio( -6.0f );
	oSomeBands[12] = (float)db10_to_ratio( -9.0f );
	oSomeBands[13] = (float)db10_to_ratio( -9.0f );
	oSomeBands[14] = (float)db10_to_ratio( -7.0f );
#ifdef WITH_JSON_SUPPORT
	ITABase::Utils::JSON::Export( &oSomeBands, "SomeBandsSpectrum.json" );
#endif

	vector<float> vfAbsorptionSpectrumTO = { 0.02f,      0.02f, 0.02f, 0.0233333f, 0.0266667f, 0.03f,      0.03f,      0.03f,      0.03f,      0.03f, 0.03f,
		                                     0.03f,      0.03f, 0.03f, 0.03f,      0.0333333f, 0.0366667f, 0.04f,      0.0433333f, 0.0466667f, 0.05f, 0.0566667f,
		                                     0.0633333f, 0.07f, 0.07f, 0.07f,      0.07f,      0.0733333f, 0.0766667f, 0.08f,      0.08f };
	for( int i = 0; i < oReflection.GetNumBands( ); i++ )
		oReflection.SetMagnitude( i, 1 - vfAbsorptionSpectrumTO[i] * vfAbsorptionSpectrumTO[i] );
#ifdef WITH_JSON_SUPPORT
	ITABase::Utils::JSON::Export( &oReflection, "ReflectedSpectrum.json" );
#endif

	vector<float> vfTrumpetDirectivitySpectrumTO = { 0.860280990600586f,  0.860280632972717f,  0.860280632972717f,  0.860280632972717f,  0.860280632972717f,
		                                             0.860280632972717f,  0.860280632972717f,  0.860280692577362f,  0.860280692577362f,  0.863133668899536f,
		                                             0.752475023269653f,  0.674751520156860f,  0.499466955661774f,  0.738280415534973f,  0.653443872928619f,
		                                             0.507191777229309f,  0.533296763896942f,  0.503476321697235f,  0.376767426729202f,  0.353374809026718f,
		                                             0.269741356372833f,  0.207140043377876f,  0.153062343597412f,  0.112099960446358f,  0.127615734934807f,
		                                             0.0946486070752144f, 0.0785422623157501f, 0.0600289255380631f, 0.0488252453505993f, 0.0387985333800316f,
		                                             0.0315645076334477f };
	oDirectivity.SetMagnitudes( vfTrumpetDirectivitySpectrumTO );
#ifdef WITH_JSON_SUPPORT
	ITABase::Utils::JSON::Export( &oDirectivity, "DirectivitySpectrum.json" );
#endif

	vector<float> vfDiffractionSpectrumTO = { 0.111934553579764f,  0.106460935614618f,  0.100699704548526f,   0.0947001721104573f,  0.0890866491204974f,
		                                      0.0835569704783756f, 0.0775507540050590f, 0.0722382340567499f,  0.0670519961369095f,  0.0621838133597414f,
		                                      0.0565935658858452f, 0.0519303873398500f, 0.0473436952392266f,  0.0428843783633814f,  0.0389753239378729f,
		                                      0.0351914143864374f, 0.0315689677042525f, 0.0284535398945892f,  0.0255957249048243f,  0.0227266136854582f,
		                                      0.0203857703689526f, 0.0182702796747406f, 0.0162996829305633f,  0.0144787461518097f,  0.0129580533457382f,
		                                      0.0115033291355433f, 0.0102513560003608f, 0.00917063160436257f, 0.00820336236165088f, 0.00725137721071123f,
		                                      0.00648611579946720f };
	oDiffraction.SetMagnitudes( vfDiffractionSpectrumTO );
#ifdef WITH_JSON_SUPPORT
	ITABase::Utils::JSON::Export( &oDiffraction, "DiffractionSpectrum.json" );
#endif

	auto pIIRFilterBurgOrder10 = CITAThirdOctaveFilterbank::Create( g_dSampleRate, g_iBlockLength, CITAThirdOctaveFilterbank::IIR_BURG_ORDER10 );
	Run( pIIRFilterBurgOrder10, "IIRBurgOrder10" );
	delete pIIRFilterBurgOrder10;

	auto pIIRBurgOrder4 = CITAThirdOctaveFilterbank::Create( g_dSampleRate, g_iBlockLength, CITAThirdOctaveFilterbank::IIR_BURG_ORDER4 );
	Run( pIIRBurgOrder4, "IIRBurgOrder4" );
	delete pIIRBurgOrder4;

	auto pIIRFilterbankBiquadOrder10 = CITAThirdOctaveFilterbank::Create( g_dSampleRate, g_iBlockLength, CITAThirdOctaveFilterbank::IIR_BIQUADS_ORDER10 );
	Run( pIIRFilterbankBiquadOrder10, "IIRBiquadOrder10" );
	delete pIIRFilterbankBiquadOrder10;

	auto pFIRConv = CITAThirdOctaveFilterbank::Create( g_dSampleRate, g_iBlockLength, CITAThirdOctaveFilterbank::FIR_SPLINE_LINEAR_PHASE );
	Run( pFIRConv, "FIRSplineLinearPhase" );
	delete pFIRConv;


	return 255;
}

void Run( CITAThirdOctaveFilterbank* pFilterBank, std::string sExportName )
{
	double dT;
	cout << " ### " << sExportName << " ### " << endl;

	pFilterBank->Clear( );
	pFilterBank->SetMagnitudes( oDirectivity );
	sw.start( );
	for( int i = 0; i < g_iSampleLength / g_iBlockLength; i++ )
		pFilterBank->Process( sbDirac.GetData( ) + i * g_iBlockLength, sbImpulseResponse.GetData( ) + i * g_iBlockLength );
	dT = sw.stop( );
	cout << "Runtime directivity magnitudes: \t" << timeToString( dT ) << endl;
	writeAudiofile( sExportName + "_Dir.wav", &sbImpulseResponse, g_dSampleRate );
	sbImpulseResponse.Zero( );

	pFilterBank->Clear( );
	pFilterBank->SetMagnitudes( oIdentity, false );
	sw.start( );
	for( int i = 0; i < g_iSampleLength / g_iBlockLength; i++ )
		pFilterBank->Process( sbDirac.GetData( ) + i * g_iBlockLength, sbImpulseResponse.GetData( ) + i * g_iBlockLength );
	dT = sw.stop( );
	cout << "Runtime identity magnitudes:  \t" << timeToString( dT ) << " (" << dT / sbImpulseResponse.GetLength( ) * g_dSampleRate * 100.0f << "% of budget)" << endl;
	writeAudiofile( sExportName + "_Identity.wav", &sbImpulseResponse, g_dSampleRate );
	sbImpulseResponse.Zero( );

	pFilterBank->Clear( );
	pFilterBank->SetMagnitudes( oZero, false );
	sw.start( );
	for( int i = 0; i < g_iSampleLength / g_iBlockLength; i++ )
		pFilterBank->Process( sbDirac.GetData( ) + i * g_iBlockLength, sbImpulseResponse.GetData( ) + i * g_iBlockLength );
	dT = sw.stop( );
	cout << "Runtime zero magnitude:\t\t" << timeToString( dT ) << endl;
	writeAudiofile( sExportName + "_Zeros.wav", &sbImpulseResponse, g_dSampleRate );
	sbImpulseResponse.Zero( );

	pFilterBank->Clear( );
	pFilterBank->SetMagnitudes( oSomeBands );
	sw.start( );
	for( int i = 0; i < g_iSampleLength / g_iBlockLength; i++ )
		pFilterBank->Process( sbDirac.GetData( ) + i * g_iBlockLength, sbImpulseResponse.GetData( ) + i * g_iBlockLength );
	dT = sw.stop( );
	cout << "Runtime real magnitudes:\t" << timeToString( dT ) << endl;
	writeAudiofile( sExportName + "_SomeBands.wav", &sbImpulseResponse, g_dSampleRate );
	sbImpulseResponse.Zero( );

	pFilterBank->Clear( );
	pFilterBank->SetMagnitudes( oHighPass );
	sw.start( );
	for( int i = 0; i < g_iSampleLength / g_iBlockLength; i++ )
		pFilterBank->Process( sbDirac.GetData( ) + i * g_iBlockLength, sbImpulseResponse.GetData( ) + i * g_iBlockLength );
	dT = sw.stop( );
	cout << "Runtime high-pass magnitudes: \t" << timeToString( dT ) << endl;
	writeAudiofile( sExportName + "_HighPass.wav", &sbImpulseResponse, g_dSampleRate );
	sbImpulseResponse.Zero( );

	pFilterBank->Clear( );
	pFilterBank->SetMagnitudes( oReflection );
	sw.start( );
	for( int i = 0; i < g_iSampleLength / g_iBlockLength; i++ )
		pFilterBank->Process( sbDirac.GetData( ) + i * g_iBlockLength, sbImpulseResponse.GetData( ) + i * g_iBlockLength );
	dT = sw.stop( );
	cout << "Runtime reflection magnitudes: \t" << timeToString( dT ) << endl;
	writeAudiofile( sExportName + "_Reflect.wav", &sbImpulseResponse, g_dSampleRate );
	sbImpulseResponse.Zero( );

	pFilterBank->Clear( );
	pFilterBank->SetMagnitudes( oDiffraction );
	sw.start( );
	for( int i = 0; i < g_iSampleLength / g_iBlockLength; i++ )
		pFilterBank->Process( sbDirac.GetData( ) + i * g_iBlockLength, sbImpulseResponse.GetData( ) + i * g_iBlockLength );
	dT = sw.stop( );
	cout << "Runtime diffraction magnitudes: \t" << timeToString( dT ) << endl;
	writeAudiofile( sExportName + "_Diffr.wav", &sbImpulseResponse, g_dSampleRate );
	sbImpulseResponse.Zero( );

	cout << endl;
}
