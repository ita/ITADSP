%% ITADSPThirdOctaveFilterbankTest evaluation Matlab script

% Loads the results of different filterbank implementations from the test ITADSPThirdOctaveFilterbankTest
% Requires ITA Toolbox, see http://ita-toolbox.org

% We use energy/power signal types as the impulse responses will give
% appropriate results of flat spectrum after filtering (a la insertion loss)

addpath( '../generators' )

%% FIR spline interpolation linear phase
fir_zeros = ita_read( 'FIRSplineLinearPhase_Zeros.wav' );
fir_unity = ita_read( 'FIRSplineLinearPhase_Identity.wav' );
fir_sb = ita_read( 'FIRSplineLinearPhase_SomeBands.wav' );
fir_hp = ita_read( 'FIRSplineLinearPhase_HighPass.wav' );
fir_refl = ita_read( 'FIRSplineLinearPhase_Reflect.wav' );
fir_dir = ita_read( 'FIRSplineLinearPhase_Dir.wav' );
fir_dif = ita_read( 'FIRSplineLinearPhase_Diffr.wav' );

%% IIR Burg order 4
iir_burg4_zeros = ita_read( 'IIRBurgOrder4_Zeros.wav' );
iir_burg4_unity = ita_read( 'IIRBurgOrder4_Identity.wav' );
iir_burg4_sb = ita_read( 'IIRBurgOrder4_SomeBands.wav' );
iir_burg4_hp = ita_read( 'IIRBurgOrder4_HighPass.wav' );
iir_burg4_refl = ita_read( 'IIRBurgOrder4_Reflect.wav' );
iir_burg4_dir = ita_read( 'IIRBurgOrder4_Dir.wav' );
iir_burg4_dif = ita_read( 'IIRBurgOrder4_Diffr.wav' );

%% IIR Burg order 10
iir_burg10_zeros = ita_read( 'IIRBurgOrder10_Zeros.wav' );
iir_burg10_unity = ita_read( 'IIRBurgOrder10_Identity.wav' );
iir_burg10_sb = ita_read( 'IIRBurgOrder10_SomeBands.wav' );
iir_burg10_hp = ita_read( 'IIRBurgOrder10_HighPass.wav' );
iir_burg10_refl = ita_read( 'IIRBurgOrder10_Reflect.wav' );
iir_burg10_dir = ita_read( 'IIRBurgOrder10_Dir.wav' );
iir_burg10_dif = ita_read( 'IIRBurgOrder10_Diffr.wav' );

%% IIR BiQuads
iir_bq_zeros = ita_read( 'IIRBiquadOrder10_Zeros.wav' );
iir_bq_unity = ita_read( 'IIRBiquadOrder10_Identity.wav' );
iir_bq_sb = ita_read( 'IIRBiquadOrder10_SomeBands.wav' );
iir_bq_hp = ita_read( 'IIRBiquadOrder10_HighPass.wav' );
iir_bq10_refl = ita_read( 'IIRBiquadOrder10_Reflect.wav' );
iir_bq10_dir = ita_read( 'IIRBiquadOrder10_Dir.wav' );
iir_bq_dif = ita_read( 'IIRBiquadOrder10_Diffr.wav' );


%% JSON import
json_zeros = ita_result_from_json( 'ZeroSpectrum.json' );
json_ident = ita_result_from_json( 'IdentitySpectrum.json' );
json_hp = ita_result_from_json( 'HighPassSpectrum.json' );
json_sb = ita_result_from_json( 'SomeBandsSpectrum.json' );
json_refl = ita_result_from_json( 'ReflectedSpectrum.json' );
json_dir = ita_result_from_json( 'DirectivitySpectrum.json' );
json_diffr = ita_result_from_json( 'DiffractionSpectrum.json' );

%% Plots
cn = { 'Target energetic spectrum', 'FIR spline-interpolation linear-phase (128 taps)', 'IIR Burg Design Algorithm (order 4)', 'IIR Burg Design Algorithm (order 10)', 'IIR Biquad filterbank (order 10)' };

if 0
    cp_zeros = ita_merge( fir_zeros, iir_burg4_zeros, iir_burg10_zeros, iir_bq_zeros );
    cp_zeros.signalType = 'energy';
    cp_zeros_r = ita_spk2frequencybands( cp_zeros );
    json_zero_r = cp_zeros_r.ch(1);
    json_zero_r.freqData = json_zeros.freqData;
    cp_zeros_r = ita_merge( json_zero_r, cp_zeros_r );
    cp_zeros_r.channelNames = cn;
    cp_zeros_r.pf
    title( 'Zero spectrum' )
end

if 0
    cp_ident = ita_merge( fir_unity, iir_burg4_unity, iir_burg10_unity, iir_bq_unity );
    cp_ident.signalType = 'energy';
    cp_ident_r = ita_spk2frequencybands( cp_ident );
    json_zero_r = cp_ident_r.ch(1);
    json_zero_r.freqData = json_ident.freqData;
    cp_ident_r = ita_merge( json_zero_r, cp_ident_r );
    cp_ident_r.channelNames = cn;
    cp_ident_r.pf
    title( 'Identity / unity spectrum' )
end

if 0
    cp_sb = ita_merge( fir_sb, iir_burg4_sb, iir_burg10_sb, iir_bq_sb );
    cp_sb.signalType = 'energy';
    cp_sb_r = ita_spk2frequencybands( cp_sb );
    json_sb_r = cp_sb_r.ch(1);
    json_sb_r.freqData = json_sb.freqData;
    cp_sb_r = ita_merge( json_sb_r, cp_sb_r );
    cp_sb_r.channelNames = cn;
    cp_sb_r.pf
    title( 'Some bands spectrum' )
end

if 0
    cp_hp = ita_merge( fir_hp, iir_burg4_hp, iir_burg10_hp, iir_bq_hp );
    cp_hp.signalType = 'energy';
    cp_hp_r = ita_spk2frequencybands( cp_hp );
    json_hp_r = cp_hp_r.ch(1);
    json_hp_r.freqData = json_hp.freqData;
    cp_hp_r = ita_merge( json_hp_r, cp_hp_r );
    cp_hp_r.channelNames = cn;
    cp_hp_r.pf
    title( 'High-pass spectrum' )
end


if 1
    cp_dir = ita_merge( fir_dir, iir_burg4_dir, iir_burg10_dir, iir_bq10_dir );
    cp_dir.signalType = 'energy';
    cp_dir_r = ita_spk2frequencybands( cp_dir );
    json_dir_r = cp_dir_r.ch(1);
    json_dir_r.freqData = json_dir.freqData;
    cp_dir_r = ita_merge( json_dir_r, cp_dir_r );
    cp_dir_r.channelNames = cn;
    cp_dir_r.pf
    title( 'Directivity spectrum' )
end



if 0
    cp_refl = ita_merge( fir_refl, iir_burg4_refl, iir_burg10_refl, iir_bq10_refl );
    cp_refl.signalType = 'energy';
    cp_refl_r = ita_spk2frequencybands( cp_refl );
    json_refl_r = cp_refl_r.ch(1);
    json_refl_r.freqData = json_refl.freqData;
    cp_refl_r = ita_merge( json_refl_r, cp_refl_r );
    cp_refl_r.channelNames = cn;
    cp_refl_r.pf
    title( 'Reflection spectrum' )
end


