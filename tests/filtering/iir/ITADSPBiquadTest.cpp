﻿/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <ITAAudiofileWriter.h>
#include <ITABiquad.h>
#include <ITASampleBuffer.h>
#include <ITASampleFrame.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>
#include <ITAThirdOctaveFIRFilterGenerator.h>
#include <ITAThirdOctaveFilterbank.h>
#include <iostream>

using namespace std;

const double g_dSampleRate = 44100;
// const int g_iFilterLength = int( ceil( g_dSampleRate / CThirdOctaveMagnitudeSpectrum::GetCenterFrequencies()[ 0 ] ) );
const int g_iFilterLength = int( 10 * ceil( g_dSampleRate / ITABase::CThirdOctaveGainMagnitudeSpectrum::GetCenterFrequencies( )[0] ) );

void TestThirdOctaveFilterGeneratorFIRIdentity( );
void TestThirdOctaveFilterGeneratorFIRZero( );
void TestThirdOctaveFilterGeneratorFIRSingleBands( );

int main( int, char** )
{
	TestThirdOctaveFilterGeneratorFIRIdentity( );
	TestThirdOctaveFilterGeneratorFIRZero( );
	TestThirdOctaveFilterGeneratorFIRSingleBands( );
	return 255;
}

void TestThirdOctaveFilterGeneratorFIRIdentity( )
{
	ITABase::CThirdOctaveGainMagnitudeSpectrum oMags;
	oMags.SetIdentity( );

	ITASampleBuffer oFilter( g_iFilterLength );

	CITAThirdOctaveFIRFilterGenerator oFilterGenerator( g_dSampleRate, g_iFilterLength );
	oFilterGenerator.GenerateFilter( oMags, oFilter.GetData( ) );

	string sFilePath = "ITADSPThirdOctaveFilterGeneratorTest_Identity_FIR.wav";
	writeAudiofile( sFilePath, &oFilter, g_dSampleRate );
	cout << "Exported result to " << sFilePath << endl;
}

void TestThirdOctaveFilterGeneratorFIRZero( )
{
	ITABase::CThirdOctaveGainMagnitudeSpectrum oMags;
	oMags.SetZero( );

	ITASampleBuffer oFilter( g_iFilterLength );

	CITAThirdOctaveFIRFilterGenerator oFilterGenerator( g_dSampleRate, g_iFilterLength );
	oFilterGenerator.GenerateFilter( oMags, oFilter.GetData( ) );

	string sFilePath = "ITADSPThirdOctaveFilterGeneratorTest_Zero_FIR.wav";
	writeAudiofile( sFilePath, &oFilter, g_dSampleRate );
	cout << "Exported result to " << sFilePath << endl;
}

void TestThirdOctaveFilterGeneratorFIRSingleBands( )
{
	ITASampleFrame oFilter( ITABase::CThirdOctaveGainMagnitudeSpectrum::GetNumBands( ), g_iFilterLength, true );

	CITAThirdOctaveFIRFilterGenerator oFilterGenerator( g_dSampleRate, g_iFilterLength );

	ITABase::CThirdOctaveGainMagnitudeSpectrum oMags;
	for( int i = 0; i < ITABase::CThirdOctaveGainMagnitudeSpectrum::GetNumBands( ); i++ )
	{
		oMags.SetZero( );
		oMags[i] = 1.0f;
		oFilterGenerator.GenerateFilter( oMags, oFilter[i].GetData( ) );
	}

	string sFilePath = "ITADSPThirdOctaveFilterGeneratorTest_SingleBands_FIR.wav";
	writeAudiofile( sFilePath, &oFilter, g_dSampleRate );
	cout << "Exported result to " << sFilePath << endl;
}
