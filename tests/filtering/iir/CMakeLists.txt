cmake_minimum_required (VERSION 3.20 FATAL_ERROR)

project (ITADSPIIRFilteringTest)

# ######################################################################################################################

add_executable (ITADSPBiquadTest ITADSPBiquadTest.cpp)
target_link_libraries (ITADSPBiquadTest PUBLIC ITADSP::ITADSP)

set_property (TARGET ITADSPBiquadTest PROPERTY FOLDER "Tests/ITADSP/filtering/iir")

install (TARGETS ITADSPBiquadTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################

add_executable (ITADSPIIRFilterTest ITADSPIIRFilterTest.cpp)
target_link_libraries (ITADSPIIRFilterTest PUBLIC ITADSP::ITADSP)

set_property (TARGET ITADSPIIRFilterTest PROPERTY FOLDER "Tests/ITADSP/filtering/iir")

install (TARGETS ITADSPIIRFilterTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################
