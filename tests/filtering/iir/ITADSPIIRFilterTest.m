%% ITADSPIIRFilterTest

% Matlab vs. C++

impls = itaAudio();
impls.timeData = [ 1; zeros( 127, 1 ) ];

ir = itaAudio();
ir.timeData = zeros( 128, 1 );

iir1 = dsp.IIRFilter;
iir1.Numerator = [ 1 2 1 ];
iir1.Denominator = [ 1 0.56 0.78 ];
ir1 = ir;
ir1.timeData = iir1( impls.timeData );
ir1.channelNames = { 'Matlab dsp.IIRFilter hard-coded' };

ITADSPIIRFilterTest_coeffs1 = jsondecode( fileread( 'ITADSPIIRFilterTest_coeffs1.json' ) );
iir2 = dsp.IIRFilter;
iir2.Numerator = [ ITADSPIIRFilterTest_coeffs1.numerator.b0 ...
                   ITADSPIIRFilterTest_coeffs1.numerator.b1 ...
                   ITADSPIIRFilterTest_coeffs1.numerator.b2 ];
iir2.Denominator = [ ITADSPIIRFilterTest_coeffs1.denominator.a1 ...
                     ITADSPIIRFilterTest_coeffs1.denominator.a2 ...
                     ITADSPIIRFilterTest_coeffs1.denominator.a3 ];

ir2 = ir;
ir2.timeData = iir2( impls.timeData );
ir2.channelNames = { 'Matlab dsp.IIRFilter JSON imported coeffs' };

ir3 = ita_read( 'ITADSPIIRFilterTest_out.wav' );
ir3 = ita_time_crop( ir3, [ 1 ir1.nSamples ], 'samples' );
ir3.channelNames = { 'C++ implementation' };

irs = ita_merge( ir1, ir2, ir3 );
irs.pt


%% ITADSPIIRFilterTest 

% Different init values

ir_uninitialized = ita_read( 'ITADSPIIRFilterTest_out_uninitialized.wav' );
ir_identity = ita_read( 'ITADSPIIRFilterTest_out_identity.wav' );
ir_zero = ita_read( 'ITADSPIIRFilterTest_out_zero.wav' );

irs = ita_merge( ir_uninitialized, ir_identity, ir_zero );
irs.channelNames = { 'Uninitialized filter engine out', 'Identity filter engine out', 'Zero''d filter entine out' };
irs.signalType = 'energy';

ita_plot_freq( irs, 'axis', [ 100 20e3 -7e3 0 ] )
