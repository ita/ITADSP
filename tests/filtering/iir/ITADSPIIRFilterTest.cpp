﻿/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */


#include <ITAAudiofileWriter.h>
#include <ITAIIRCoefficients.h>
#include <ITAIIRFilterEngine.h>
#include <ITAIIRUtils.h>
#include <ITASampleBuffer.h>
#include <ITASampleFrame.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>
#include <iostream>

using namespace std;

const double g_dSampleRate = 44100;

int main( int, char** )
{
	ITASampleBuffer sbInput( 1024 );
	sbInput[0]        = 1.0f;
	const float* pfIn = sbInput.GetData( );
	int iSamples      = sbInput.GetLength( );

	ITASampleBuffer sbOutput( 1024 );
	float* pfOut = sbOutput.GetData( );


	ITADSP::CFilterCoefficients oCoeffs1;
	oCoeffs1.uiOrder       = 2;
	oCoeffs1.vfNumerator   = { 1, 2, 1 };
	oCoeffs1.vfDenominator = { 1, 0.56f, 0.78f };
	oCoeffs1.bIsARMA       = true;
#ifdef WITH_JSON_SUPPORT
	ITADSP::ExportIIRCoefficientsToJSON( "ITADSPIIRFilterTest_coeffs1.json", oCoeffs1 );
#endif


	CITAIIRFilterEngine oIIRFilterEngine( oCoeffs1.uiOrder );
	oIIRFilterEngine.SetCoefficients( oCoeffs1 );
	oIIRFilterEngine.Process( pfIn, pfOut, iSamples );
	writeAudiofile( "ITADSPIIRFilterTest_coeffs1_out.wav", &sbOutput, g_dSampleRate );


	CITAIIRFilterEngine oIIRFilterEngine_Uninitialized( oCoeffs1.uiOrder );
	oIIRFilterEngine_Uninitialized.Process( pfIn, pfOut, iSamples );
	writeAudiofile( "ITADSPIIRFilterTest_out_uninitialized.wav", &sbOutput, g_dSampleRate );


	CITAIIRFilterEngine oIIRFilterEngine_Identity( oCoeffs1.uiOrder );
	oIIRFilterEngine_Identity.SetCoefficientsToIdentity( );
	oIIRFilterEngine_Identity.Process( pfIn, pfOut, iSamples );
	writeAudiofile( "ITADSPIIRFilterTest_out_identity.wav", &sbOutput, g_dSampleRate );


	CITAIIRFilterEngine oIIRFilterEngine_Zero( oCoeffs1.uiOrder );
	oIIRFilterEngine_Zero.SetCoefficientsToZero( );
	oIIRFilterEngine_Zero.Process( pfIn, pfOut, iSamples );
	writeAudiofile( "ITADSPIIRFilterTest_out_zero.wav", &sbOutput, g_dSampleRate );


	return 255;
}
