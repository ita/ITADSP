﻿#include <ITAAudiofileWriter.h>
#include <ITAConstants.h>
#include <ITADSP/PD/JetEngine.h>
#include <ITAFiniteImpulseResponse.h>
#include <ITAMultichannelFiniteImpulseResponse.h>
#include <ITASampleBuffer.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>
#include <iostream>

using namespace std;

const double g_dSampleRate       = 44100;
const int g_iOutputLengthSamples = 20 * int( g_dSampleRate );
int iTotalSamples                = 0;

int main( int, char** )
{
	ITAStopWatch sw;
	ITASampleBuffer oOutputBuffer( g_iOutputLengthSamples );

	vector<float> vfRPMs = { 1000.f, 4000.0f, 2000.0f };
	bool bColdStart      = true;
	ITADSP::PD::CJetEngine oPatch( g_dSampleRate, vfRPMs[0], bColdStart );

	int iTimeSeriesLeg = g_iOutputLengthSamples / (int)vfRPMs.size( );

	for( auto m = 0; m < vfRPMs.size( ); m++ )
	{
		oPatch.SetRPM( vfRPMs[m] );

		int iProcessSamples = std::min( iTimeSeriesLeg, g_iOutputLengthSamples - m * iTimeSeriesLeg );
		sw.start( );
		oPatch.Process( oOutputBuffer.GetData( ) + iTotalSamples, iProcessSamples );
		sw.stop( );
		iTotalSamples += iProcessSamples;
	}
	assert( iTotalSamples == oOutputBuffer.GetLength( ) );

	string sFilePath = "ITADSP_pd_jet_engine_out.wav";
	writeAudiofile( sFilePath, &oOutputBuffer, g_dSampleRate, ITAQuantization::ITA_FLOAT );
	cout << "Exported result to " << sFilePath << endl;

	cout << "Real-time ratio: " << iTimeSeriesLeg / g_dSampleRate / sw.mean( ) << endl;

	return 255;
}
