%% Manual filters at 1kHz

IR_LP_1kHz = ita_read( 'NativeDspFiltersTest_IR_LP_1kHz.wav' );
IR_HP_1kHz = ita_read( 'NativeDspFiltersTest_IR_HP_1kHz.wav' );
IR_BP_1kHz = ita_read( 'NativeDspFiltersTest_IR_BP_1kHz.wav' );

IR_manual_filters_1kHz = ita_merge( IR_LP_1kHz, IR_HP_1kHz, IR_BP_1kHz );
IR_manual_filters_1kHz.channelNames = { 'Low pass 1kHz', 'High pass 1kHz', 'Band pass 1kHz and bandwidth 707Hz' };

%IR_manual_filters_1kHz.pf


%% Tribands

IR_Triband_Low = ita_read( 'NativeDspFiltersTest_IR_Triband_Low.wav' );
IR_Triband_Mid = ita_read( 'NativeDspFiltersTest_IR_Triband_Mid.wav' );
IR_Triband_High = ita_read( 'NativeDspFiltersTest_IR_Triband_High.wav' );

IR_Triband = ita_merge( IR_Triband_Low, IR_Triband_Mid, IR_Triband_High );
IR_Triband = ita_merge( IR_Triband, IR_Triband.sum );
IR_Triband.channelNames = { 'Low pass 250kHz cutoff', ...
    'Band pass 1kHz bandwidth 3.75kHz', ...
    'High pass 4kHz cutoff', ...
    'Triband identity (sum)' };

%IR_Triband.pf


%% Octaves

IR_Octaves_LP = ita_read( 'NativeDspFiltersTest_IR_OctaveBands_LP.wav' );
IR_Octaves_BP1 = ita_read( 'NativeDspFiltersTest_IR_OctaveBands_BP1.wav' );
IR_Octaves_BP2 = ita_read( 'NativeDspFiltersTest_IR_OctaveBands_BP2.wav' );
IR_Octaves_BP3 = ita_read( 'NativeDspFiltersTest_IR_OctaveBands_BP3.wav' );
IR_Octaves_BP4 = ita_read( 'NativeDspFiltersTest_IR_OctaveBands_BP4.wav' );
IR_Octaves_BP5 = ita_read( 'NativeDspFiltersTest_IR_OctaveBands_BP5.wav' );
IR_Octaves_BP6 = ita_read( 'NativeDspFiltersTest_IR_OctaveBands_BP6.wav' );
IR_Octaves_BP7 = ita_read( 'NativeDspFiltersTest_IR_OctaveBands_BP7.wav' );
IR_Octaves_BP8 = ita_read( 'NativeDspFiltersTest_IR_OctaveBands_BP8.wav' );
IR_Octaves_HP = ita_read( 'NativeDspFiltersTest_IR_OctaveBands_HP.wav' );

IR_Octaves = ita_merge( IR_Octaves_LP, IR_Octaves_BP1, IR_Octaves_BP2,IR_Octaves_BP3, IR_Octaves_BP4, IR_Octaves_BP5, IR_Octaves_BP6, IR_Octaves_BP7, IR_Octaves_BP8, IR_Octaves_HP );
IR_Octaves = ita_merge( IR_Octaves, IR_Octaves.sum );
IR_Octaves.pf


%% Time-variant filtering

LiveCoeffChangeTest = ita_read( 'NativeDspFiltersTest_LiveCoeffChangeTest.wav' );
LiveCoeffChangeTest.pt
LiveCoeffChangeTest.ch(1).play


%% Very low low pass

vllp_ir = ita_read( 'NativeDspFiltersTest_IR_VeryLowLowPass.wav' );
vllp_sf_in = ita_read( 'NativeDspFiltersTest_SF_VeryLowLowPass_in.wav' );
vllp_sf_out = ita_read( 'NativeDspFiltersTest_SF_VeryLowLowPass_out.wav' );

vllp = ita_merge( vllp_ir, vllp_sf_in, vllp_sf_out );
%vllp.channelUnits = 'energy';
vllp.channelNames = { 'Impulse response', 'Step function [in]', 'Step function [out]' };
title 'Very low pass filter (2 Hz) first order zero pole'
vllp.pt
