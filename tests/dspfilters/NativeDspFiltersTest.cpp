/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * Testing DspFilters library.
 *
 */

#include "VistaTools/VistaRandomNumberGenerator.h"

#include <DspFilters/Butterworth.h>
#include <ITAAudioSample.h>
#include <ITAConstants.h>
#include <ITAFiniteImpulseResponse.h>
#include <ITAStopWatch.h>
#include <cassert>
#include <cmath>
#include <iostream>
#include <vector>

using namespace std;
using namespace Dsp;
using namespace ITABase;
using namespace ITAConstants;

float fSampleRate   = 44100.0f;
const int iOrder    = 10;
const int iChannels = 1;
int iFilterLength   = (int)pow( 2, 12 );

void manual_low_pass_1kHz( );
void manual_high_pass_1kHz( );
void manual_band_pass_1kHz( );
void tri_band( );
void downcast_test( );
void octave_bands( );
void live_coeff_change_test( );
void very_low_low_pass( );

int main( int, char** )
{
	manual_low_pass_1kHz( );
	manual_high_pass_1kHz( );
	manual_band_pass_1kHz( );

	tri_band( );

	downcast_test( );

	octave_bands( );

	live_coeff_change_test( );

	very_low_low_pass( );

	return 255;
}

void manual_low_pass_1kHz( )
{
	SimpleFilter<Butterworth::LowPass<iOrder>, iChannels> oBandPassFilter;


	float fCuttoffFrequency = 1000.0f;
	oBandPassFilter.setup( iOrder, fSampleRate, fCuttoffFrequency );

	CFiniteImpulseResponse oDirac( iFilterLength, fSampleRate );
	oDirac.SetDirac( );

	float* pf = oDirac.GetData( );
	oBandPassFilter.process( iFilterLength, &pf );

	oDirac.Store( "NativeDspFiltersTest_IR_LP_1kHz.wav" );
}

void manual_high_pass_1kHz( )
{
	SimpleFilter<Butterworth::HighPass<iOrder>, iChannels> oBandPassFilter;

	float fCuttoffFrequency = 1000.0f;
	oBandPassFilter.setup( iOrder, fSampleRate, fCuttoffFrequency );

	CFiniteImpulseResponse oDirac( iFilterLength, fSampleRate );
	oDirac.SetDirac( );

	float* pf = oDirac.GetData( );
	oBandPassFilter.process( iFilterLength, &pf );

	oDirac.Store( "NativeDspFiltersTest_IR_HP_1kHz.wav" );
}

void manual_band_pass_1kHz( )
{
	SimpleFilter<Butterworth::BandPass<iOrder>, iChannels> oBandPassFilter;

	float fCenterFrequency = 1000.0f;
	float fBandWidth       = fCenterFrequency * (float)( sqrt( 2 ) - 1 / sqrt( 2 ) );
	oBandPassFilter.setup( iOrder, fSampleRate, fCenterFrequency, fBandWidth );

	CFiniteImpulseResponse oDirac( iFilterLength, fSampleRate );
	oDirac.SetDirac( );

	float* pf = oDirac.GetData( );
	oBandPassFilter.process( iFilterLength, &pf );

	oDirac.Store( "NativeDspFiltersTest_IR_BP_1kHz.wav" );
}

void tri_band( )
{
	float fLowerFrequencyCuttoff  = 330.0f;
	float fHigherFrequencyCuttoff = 3900.0f;
	float fMidCenterFrequency     = 2300.0f;
	float fMidBandWidth           = 4000;

	SimpleFilter<Butterworth::LowPass<iOrder>, iChannels> oLPF;
	oLPF.setup( iOrder, fSampleRate, fLowerFrequencyCuttoff );

	SimpleFilter<Butterworth::HighPass<iOrder>, iChannels> oHPF;
	oHPF.setup( iOrder, fSampleRate, fHigherFrequencyCuttoff );

	SimpleFilter<Butterworth::BandPass<iOrder>, iChannels> oBPF;
	assert( fMidBandWidth > 0 );
	oBPF.setup( iOrder, fSampleRate, fMidCenterFrequency, fMidBandWidth );

	CFiniteImpulseResponse oIR( iFilterLength, fSampleRate );

	oIR.SetDirac( );
	float* pf = oIR.GetData( );
	oLPF.process( iFilterLength, &pf );
	oIR.Store( "NativeDspFiltersTest_IR_Triband_Low.wav" );

	oIR.SetDirac( );
	pf = oIR.GetData( );
	oBPF.process( iFilterLength, &pf );
	oIR.Store( "NativeDspFiltersTest_IR_Triband_Mid.wav" );

	oIR.SetDirac( );
	pf = oIR.GetData( );
	oHPF.process( iFilterLength, &pf );
	oIR.Store( "NativeDspFiltersTest_IR_Triband_High.wav" );
}

void downcast_test( )
{
	SimpleFilter<Butterworth::BandPass<iOrder>, iChannels> oBandPassFilter;
	SimpleFilter<Butterworth::BandPass<iOrder>, iChannels>* pBPF = &oBandPassFilter;
	float fV                                                     = 1.0f;
	float* pf                                                    = &fV;
	pBPF->process( 1, &pf );
}

void octave_bands( )
{
	CFiniteImpulseResponse oIR( iFilterLength, fSampleRate );

	float fLowPassFrequencyCuttoff = OCTAVE_CENTER_FREQUENCIES_ISO_F[0] * sqrt( 2.0f );
	SimpleFilter<Butterworth::LowPass<iOrder>, iChannels> oLPF;
	oLPF.setup( iOrder, fSampleRate, fLowPassFrequencyCuttoff );

	oIR.SetDirac( );
	float* pf = oIR.GetData( );
	oLPF.process( iFilterLength, &pf );
	oIR.Store( "NativeDspFiltersTest_IR_OctaveBands_LP.wav" );

	for( size_t i = 1; i < OCTAVE_CENTER_FREQUENCIES_ISO_F.size( ) - 1; i++ )
	{
		float fMidCenterFrequency = OCTAVE_CENTER_FREQUENCIES_ISO_F[i];
		float fMidBandWidth       = ( OCTAVE_CENTER_FREQUENCIES_ISO_F[i + 1] - OCTAVE_CENTER_FREQUENCIES_ISO_F[i - 1] ) / 2.15f;

		SimpleFilter<Butterworth::BandPass<iOrder>, iChannels> oBPF;
		assert( fMidBandWidth > 0 );
		oBPF.setup( iOrder, fSampleRate, fMidCenterFrequency, fMidBandWidth );

		oIR.SetDirac( );
		pf = oIR.GetData( );
		oBPF.process( iFilterLength, &pf );
		oIR.Store( "NativeDspFiltersTest_IR_OctaveBands_BP" + std::to_string( (long)( i ) ) + ".wav" );
	}

	float fHighPassFrequencyCuttoff = OCTAVE_CENTER_FREQUENCIES_ISO_F[OCTAVE_CENTER_FREQUENCIES_ISO_F.size( ) - 1] / sqrt( 2.0f );
	SimpleFilter<Butterworth::HighPass<iOrder>, iChannels> oHPF;
	oHPF.setup( iOrder, fSampleRate, fHighPassFrequencyCuttoff );

	oIR.SetDirac( );
	pf = oIR.GetData( );
	oHPF.process( iFilterLength, &pf );
	oIR.Store( "NativeDspFiltersTest_IR_OctaveBands_HP.wav" );
}

void live_coeff_change_test( )
{
	float fMidCenterFrequency = 1200.0f;
	float fQ                  = 2;

	SimpleFilter<Butterworth::BandPass<iOrder>, iChannels> oBPFDynamic, oBPFStatic;
	assert( fMidCenterFrequency > 0 );
	oBPFDynamic.setup( iOrder, fSampleRate, fMidCenterFrequency, fMidCenterFrequency / fQ );
	oBPFStatic.setup( iOrder, fSampleRate, fMidCenterFrequency, fMidCenterFrequency / fQ );

	CITAAudioSample sbCoeffChangeTest( 3, int( fSampleRate * 10 ), fSampleRate );

	ITAStopWatch sw;
	for( int n = 0; n < sbCoeffChangeTest.GetLength( ); n++ )
	{
		float fRandomSample     = VistaRandomNumberGenerator::GetStandardRNG( )->GenerateFloat( -1.0f, 1.0f );
		sbCoeffChangeTest[0][n] = fRandomSample;
		sbCoeffChangeTest[1][n] = fRandomSample;

		float fModulatedCenterFrequency = fMidCenterFrequency + 600.0f * sin( ITAConstants::TWO_PI_F * n / float( sbCoeffChangeTest.GetLength( ) ) * 10 );
		sbCoeffChangeTest[2][n]         = ( fModulatedCenterFrequency - fMidCenterFrequency ) / 2.0f / fMidCenterFrequency;

		sw.start( );
		oBPFDynamic.setup( iOrder, fSampleRate, fModulatedCenterFrequency, fModulatedCenterFrequency / fQ );
		sw.stop( ); // takes about 30us on Intel i7 3GHZ from 2015 in debug mode .. not entirely irrelevant :/

		float* pfSampleAliasDynamic = &sbCoeffChangeTest[0][n];
		oBPFDynamic.process( 1, &pfSampleAliasDynamic );

		float* pfSampleAliasStatic = &sbCoeffChangeTest[1][n];
		oBPFStatic.process( 1, &pfSampleAliasStatic );
	}

	cout << "SimpleFilter setup routine stats: " << sw << endl;

	sbCoeffChangeTest.Store( "NativeDspFiltersTest_LiveCoeffChangeTest.wav" );
}

void very_low_low_pass( )
{
	float fVeryLowLowPassFrequencyCuttoff = 0.2f; // 0.2 Hz
	SimpleFilter<Butterworth::LowPass<1>, 1> oVLLPF;
	oVLLPF.setup( 1, fSampleRate, fVeryLowLowPassFrequencyCuttoff );

	CFiniteImpulseResponse oSF( iFilterLength * 30, fSampleRate );
	oSF.SetUnitStepFunction( 0.1f );
	for( int n = 0; n < 4 * iFilterLength; n++ )
		oSF[2 * 2 * iFilterLength + n] = 0.2f;
	for( int n = 0; n < 4 * iFilterLength; n++ )
		oSF[2 * 4 * iFilterLength + n] = 0.3f;
	for( int n = 0; n < 4 * iFilterLength; n++ )
		oSF[2 * 6 * iFilterLength + n] = 0.4f;
	for( int n = 0; n < 4 * iFilterLength; n++ )
		oSF[2 * 8 * iFilterLength + n] = 0.5f;
	for( int n = 0; n < 4 * iFilterLength; n++ )
		oSF[2 * 10 * iFilterLength + n] = 1.0f;
	for( int n = 0; n < 4 * iFilterLength; n++ )
		oSF[2 * 12 * iFilterLength + n] = 0.0f;

	oSF.Store( "NativeDspFiltersTest_SF_VeryLowLowPass_in.wav" );

	for( size_t n = 0; n < oSF.GetLength( ); n++ )
	{
		auto pf = oSF.GetData( ) + n;
		oVLLPF.process( 1, &pf );
	}
	oSF.Store( "NativeDspFiltersTest_SF_VeryLowLowPass_out.wav" );

	oVLLPF.reset( );


	CFiniteImpulseResponse oIR( iFilterLength * 30, fSampleRate );
	oIR.SetDirac( );
	for( size_t n = 0; n < oIR.GetLength( ); n++ )
	{
		auto pf = oIR.GetData( ) + n;
		oVLLPF.process( 1, &pf );
	}
	oIR.Store( "NativeDspFiltersTest_IR_VeryLowLowPass.wav" );
}
