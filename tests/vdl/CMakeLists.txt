cmake_minimum_required (VERSION 3.20 FATAL_ERROR)

project (ITADSPVDLTest)

# ######################################################################################################################

add_executable (ITADSPVariableDelayLineTest ITADSPVariableDelayLineTest.cpp)
target_link_libraries (
	ITADSPVariableDelayLineTest PUBLIC ITADSP::ITADSP ITABase::ITABase ITADataSources::ITADataSources
)

set_property (TARGET ITADSPVariableDelayLineTest PROPERTY FOLDER "Tests/ITADSP/vdl")
set_property (
	TARGET ITADSPVariableDelayLineTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
)

install (TARGETS ITADSPVariableDelayLineTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################

add_executable (ITADSPSIMOVDLTest ITADSPSIMOVDLTest.cpp)
target_link_libraries (ITADSPSIMOVDLTest PUBLIC ITADSP::ITADSP ITABase::ITABase ITADataSources::ITADataSources)

set_property (TARGET ITADSPSIMOVDLTest PROPERTY FOLDER "Tests/ITADSP/vdl")
set_property (TARGET ITADSPSIMOVDLTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

install (TARGETS ITADSPSIMOVDLTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################

add_executable (ITADSPSIMOVDLSourceInShoebox ITADSPSIMOVDLSourceInShoebox.cpp)
target_link_libraries (
	ITADSPSIMOVDLSourceInShoebox PUBLIC ITADSP::ITADSP ITABase::ITABase ITADataSources::ITADataSources
)

set_property (TARGET ITADSPSIMOVDLSourceInShoebox PROPERTY FOLDER "Tests/ITADSP/vdl")
set_property (
	TARGET ITADSPSIMOVDLSourceInShoebox PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
)

install (TARGETS ITADSPSIMOVDLSourceInShoebox RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################

add_executable (ITASIMOVariableDelayLineBaseTest ITASIMOVariableDelayLineBaseTest.cpp)
target_link_libraries (
	ITASIMOVariableDelayLineBaseTest PUBLIC ITADSP::ITADSP ITABase::ITABase ITADataSources::ITADataSources
)

set_property (TARGET ITASIMOVariableDelayLineBaseTest PROPERTY FOLDER "Tests/ITADSP/vdl")
set_property (
	TARGET ITASIMOVariableDelayLineBaseTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
)

install (TARGETS ITASIMOVariableDelayLineBaseTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################
