#include <ITAAudiofileWriter.h>
#include <ITASampleBuffer.h>
#include <ITASampleFrame.h>
#include <ITAStreamFunctionGenerator.h>
#include <ITAStreamInfo.h>
#include <ITAStringUtils.h>
#include <ITAVariableDelayLine.h>
#include <VistaTools/VistaRandomNumberGenerator.h>
#include <iostream>
#include <math.h>
#include <vector>

using namespace std;

const unsigned int iBlockLength      = 128;
const double dSampleRate             = 44.1e3;
const float fMaxReservedDelaySamples = 5 * iBlockLength;
const float fSimulateSeconds         = 10;
const float fInitialDelaySamples     = 10 * 2 * iBlockLength - 1;

void WriteFromDatasourceToFile( vector<CITAVariableDelayLine*>, ITADatasource*, float );
void TestVDLProcessing( );
void preProcessVDL( std::vector<CITAVariableDelayLine*> vpVDLs, unsigned int iCount );
void test7_lininterp( );
void TestSpecialResampling( int iFromSample, int iToSample );
void StressTest( );

int main( int, char** )
{
	try
	{
		// StressTest();

		TestVDLProcessing( );

		// TestSpecialResampling( 1778, 1630 );
	}
	catch( ITAException& e )
	{
		cout << e << endl;
		return 0;
	}


	// test1_crossfade();
	// test2_lininterp();
	// test3_lininterp();
	// test4_lininterp();
	// test5_lininterp();
	// test6_lininterp();
	// test7_lininterp();

	return 255;
}
void StressTest( )
{
	ITAStreamFunctionGenerator sinesignal( 1, dSampleRate, iBlockLength, ITAStreamFunctionGenerator::SINE, 500.0f, 0.9f, true );
	EVDLAlgorithm eAlgorithm    = EVDLAlgorithm::CUBIC_SPLINE_INTERPOLATION;
	CITAVariableDelayLine* pVDL = new CITAVariableDelayLine( dSampleRate, iBlockLength, fMaxReservedDelaySamples + 13, eAlgorithm );

	ITASampleBuffer sbIn( iBlockLength );
	ITASampleBuffer sbOut( iBlockLength );

	for( int i = 0; i < 10e6; i++ )
	{
		float fRandomResamplingFactor = VistaRandomNumberGenerator::GetStandardRNG( )->GenerateFloat( -0.5, 0.5f );
		float fDelaySamples           = float( iBlockLength ) + float( iBlockLength * fRandomResamplingFactor );
		pVDL->SetDelaySamples( fDelaySamples );

		sbIn.write( sinesignal.GetBlockPointer( 0, nullptr ), iBlockLength );
		pVDL->Process( &sbIn, &sbOut );
		sinesignal.IncrementBlockPointer( );

		if( ( i % 10000 ) == 0 )
			cout << ".";
	}

	delete pVDL;
}

void TestSpecialResampling( int iFromSample, int iToSample )
{
	ITAStreamFunctionGenerator sinesignal( 1, dSampleRate, iBlockLength, ITAStreamFunctionGenerator::SINE, 500.0f, 0.9f, true );

	CITAVariableDelayLine* pVDL = new CITAVariableDelayLine( dSampleRate, iBlockLength, fMaxReservedDelaySamples );

	ITASampleBuffer sbIn( iBlockLength );
	ITASampleBuffer sbOut( iBlockLength );

	pVDL->SetDelaySamples( float( iFromSample ) );

	sbIn.write( sinesignal.GetBlockPointer( 0, nullptr ), iBlockLength );
	pVDL->Process( &sbIn, &sbOut );
	sinesignal.IncrementBlockPointer( );

	pVDL->SetDelaySamples( float( iToSample ) );

	sbIn.write( sinesignal.GetBlockPointer( 0, nullptr ), iBlockLength );
	pVDL->Process( &sbIn, &sbOut );
	sinesignal.IncrementBlockPointer( );

	delete pVDL;
}

void TestVDLProcessing( )
{
	cout << " * VDL test" << endl;

	ITAStreamFunctionGenerator sinesignal( 1, dSampleRate, iBlockLength, ITAStreamFunctionGenerator::SINE, 500.0f, 0.9f, true );

	CITAVariableDelayLine* pVDLSwitch = new CITAVariableDelayLine( dSampleRate, iBlockLength, fMaxReservedDelaySamples, EVDLAlgorithm::SWITCH );
	CITAVariableDelayLine* pVDLCross  = new CITAVariableDelayLine( dSampleRate, iBlockLength, fMaxReservedDelaySamples, EVDLAlgorithm::CROSSFADE );
	CITAVariableDelayLine* pVDLLin    = new CITAVariableDelayLine( dSampleRate, iBlockLength, fMaxReservedDelaySamples, EVDLAlgorithm::LINEAR_INTERPOLATION );
	CITAVariableDelayLine* pVDLSpline = new CITAVariableDelayLine( dSampleRate, iBlockLength, fMaxReservedDelaySamples, EVDLAlgorithm::CUBIC_SPLINE_INTERPOLATION );
	CITAVariableDelayLine* pVDLSinc = new CITAVariableDelayLine( dSampleRate, iBlockLength, fMaxReservedDelaySamples, EVDLAlgorithm::WINDOWED_SINC_INTERPOLATION );

	pVDLSwitch->SetDelaySamples( fInitialDelaySamples );
	pVDLCross->SetDelaySamples( fInitialDelaySamples );
	pVDLLin->SetDelaySamples( fInitialDelaySamples );
	pVDLSpline->SetDelaySamples( fInitialDelaySamples );
	pVDLSinc->SetDelaySamples( fInitialDelaySamples );

	vector<CITAVariableDelayLine*> vpVDLs;
	vpVDLs.push_back( pVDLSwitch );
	vpVDLs.push_back( pVDLCross );
	vpVDLs.push_back( pVDLLin );
	vpVDLs.push_back( pVDLSpline );
	vpVDLs.push_back( pVDLSinc );

	WriteFromDatasourceToFile( vpVDLs, &sinesignal, fSimulateSeconds );

	for( int i = 0; i < (int)vpVDLs.size( ); i++ )
		delete vpVDLs[i];

	vpVDLs.clear( );
}

void preProcessVDL( std::vector<CITAVariableDelayLine*> vpVDLs, unsigned int iCount )
{
	float fNewDelaySamples           = 0.0f;
	const float fCurrentDelaySamples = vpVDLs[0]->GetDelaySamples( );

	// Noinc
	// fNewDelaySamples = fCurrentDelaySamples + 0;

	// Linear inc
	// fNewDelaySamples = fCurrentDelaySamples + 1;

	// Linear dec
	// fNewDelaySamples = fCurrentDelaySamples - 1;

	// Equivalent probability density for all r's between 0.5 and 2
	int iSig = ( ( iCount % 2 ) ? 1 : -1 );
	int i    = ( iCount % iBlockLength );
	if( iSig == 1 )
		fNewDelaySamples = fCurrentDelaySamples + (int)( i / 2 );
	if( iSig == -1 )
		fNewDelaySamples = fCurrentDelaySamples - i;

	// Sine wave modulation 0
	// fNewDelaySamples =  100 + 50 * (float) sin(2*3.1415*iCount/400.0f);

	// Sine wave modulation 0
	// fNewDelaySamples =  100 + 50 * (float) sin(2*3.1415*iCount/100.0f);

	// Sine wave modulation 1
	// fNewDelaySamples =  200 + 100 * (float) sin(2*3.1415*iCount/100.0f);

	// Sine wave modulation 2
	// fNewDelaySamples = 400 + 200 * (float) sin(2*3.1415*iCount/60.0f);

	// Sine wave modulation 3
	// fNewDelaySamples = 600 + 400 * (float) sin(2*3.1415*iCount/150.0f);

	// Ab und zu inc
	// fNewDelaySamples = ((iCount%100) == 0) ? fCurrentDelaySamples + 1 : fCurrentDelaySamples;


	// float r = 1 + ( (int) (fCurrentDelaySamples-fNewDelaySamples) ) / (float) iBlockLength;
	// VA_DEBUG_PRINTF(" * [VDL] Resampling factor = %f\n", r);

	if( fNewDelaySamples > 0 )
		for( int i = 0; i < (int)vpVDLs.size( ); i++ )
			vpVDLs[i]->SetDelaySamples( fNewDelaySamples );

	// if (fCurrentDelaySamples != fNewDelaySamples)
	// VA_DEBUG_PRINTF(" * [VDL] New delay = %f\n", fNewDelaySamples);
}

void WriteFromDatasourceToFile( std::vector<CITAVariableDelayLine*> vpVDL, ITADatasource* pSource, float fSeconds )
{
	double dSamplerate            = dSampleRate;
	unsigned int uiBlocklength    = iBlockLength;
	unsigned int uiNumberOfFrames = (unsigned int)std::ceil( dSamplerate * fSeconds / (float)uiBlocklength );

	unsigned int uiChannels = (unsigned int)vpVDL.size( );

	ITAAudiofileProperties props_in;
	props_in.iChannels            = 1;
	props_in.dSampleRate          = dSamplerate;
	props_in.eQuantization        = ITAQuantization::ITA_FLOAT;
	props_in.eDomain              = ITADomain::ITA_TIME_DOMAIN;
	props_in.iLength              = uiNumberOfFrames * uiBlocklength;
	ITAAudiofileWriter* writer_in = ITAAudiofileWriter::create( "VDL_in.wav", props_in );
	ITAAudiofileProperties props_out( props_in );
	props_out.iChannels            = uiChannels;
	ITAAudiofileWriter* writer_out = ITAAudiofileWriter::create( "VDL_out.wav", props_out );

	ITAStreamInfo oState;

	ITASampleBuffer* psbInput = new ITASampleBuffer( uiBlocklength, true );
	ITASampleFrame* psfOutput = new ITASampleFrame( uiChannels, uiBlocklength, true );

	unsigned int n = 0;
	while( n < uiNumberOfFrames )
	{
		psbInput->write( pSource->GetBlockPointer( 0, &oState ), uiBlocklength );
		pSource->IncrementBlockPointer( );
		preProcessVDL( vpVDL, n );
		for( int i = 0; i < (int)vpVDL.size( ); i++ )
			vpVDL[i]->Process( psbInput, &( *psfOutput )[i] );

		std::vector<float*> pIn;
		pIn.push_back( psbInput->data( ) );
		writer_in->write( uiBlocklength, pIn );
		writer_out->write( psfOutput, uiBlocklength );
		n++;
	}

	delete writer_in;
	delete writer_out;

	delete psbInput;
	delete psfOutput;
}


void test7_lininterp( )
{
	const double sr            = 44.1e3;
	const int bs               = 128;
	CITAVariableDelayLine* vdl = new CITAVariableDelayLine( sr, bs, 1, EVDLAlgorithm::LINEAR_INTERPOLATION );
	vdl->SetDelaySamples( 12 );

	ITASampleBuffer sbIn( bs, false );
	ITASampleBuffer sbOut( bs, false );

	// Initialize delay line sample values for 12 blocks
	for( int j = 0; j < 12; j++ )
	{
		// Overwrite Input
		for( int i = 0; i < bs; i++ )
			sbIn[i] = (float)( j * bs + i + 1 );

		vdl->SetDelaySamples( float( ( j + 1 ) * bs / 100 ) );

		vdl->Process( &sbIn, &sbOut );
		cout << "in  = " << sbIn.ValuesToString( ) << endl;
		cout << "out = " << sbOut.ValuesToString( ) << endl;
		cout << "for this block delay was set to " << vdl->GetDelaySamples( ) << endl << endl;
	}

	delete vdl;
}

void test6_lininterp( )
{
	const double sr            = 44.1e3;
	const int bs               = 128;
	CITAVariableDelayLine* vdl = new CITAVariableDelayLine( sr, bs, 1, EVDLAlgorithm::LINEAR_INTERPOLATION );
	vdl->SetDelaySamples( 12 );

	ITASampleBuffer sbIn( bs, false );
	ITASampleBuffer sbOut( bs, false );

	// Initialize delay line sample values for 12 blocks
	for( int j = 0; j < 12; j++ )
	{
		// Overwrite Input
		for( int i = 0; i < bs; i++ )
			sbIn[i] = (float)( j * bs + i + 1 );

		vdl->SetDelaySamples( float( bs / ( j + 1 ) ) );

		vdl->Process( &sbIn, &sbOut );
		cout << "in  = " << sbIn.ValuesToString( ) << endl;
		cout << "out = " << sbOut.ValuesToString( ) << endl;
		cout << "for this block delay was set to " << vdl->GetDelaySamples( ) << endl << endl;
	}

	delete vdl;
}

void test5_lininterp( )
{
	const double sr            = 44.1e3;
	const int bs               = 4;
	CITAVariableDelayLine* vdl = new CITAVariableDelayLine( sr, bs, 1, EVDLAlgorithm::LINEAR_INTERPOLATION );
	vdl->SetDelaySamples( 12 );

	ITASampleBuffer sbIn( bs, false );
	ITASampleBuffer sbOut( bs, false );

	// Initialize delay line sample values for 12 blocks
	for( int j = 0; j < 12; j++ )
	{
		// Overwrite Input
		for( int i = 0; i < bs; i++ )
			sbIn[i] = (float)( j * bs + i + 1 );

		vdl->SetDelaySamples( float( 12 - j - 1 ) );

		vdl->Process( &sbIn, &sbOut );
		cout << "in  = " << sbIn.ValuesToString( ) << endl;
		cout << "out = " << sbOut.ValuesToString( ) << endl;
		cout << "for this block delay was set to " << vdl->GetDelaySamples( ) << endl << endl;
	}

	delete vdl;
}

void test4_lininterp( )
{
	const double sr            = 44.1e3;
	const int bs               = 4;
	CITAVariableDelayLine* vdl = new CITAVariableDelayLine( sr, bs, 1, EVDLAlgorithm::LINEAR_INTERPOLATION );
	vdl->SetDelaySamples( 0 );

	ITASampleBuffer sbIn( bs, false );
	ITASampleBuffer sbOut( bs, false );

	// Initialize delay line sample values for 12 blocks
	for( int j = 0; j < 12; j++ )
	{
		// Overwrite Input
		for( int i = 0; i < bs; i++ )
			sbIn[i] = (float)( j * bs + i + 1 );

		vdl->SetDelaySamples( float( j + 1 ) );

		vdl->Process( &sbIn, &sbOut );
		cout << "in  = " << sbIn.ValuesToString( ) << endl;
		cout << "out = " << sbOut.ValuesToString( ) << endl;
		cout << "for this block delay was set to " << vdl->GetDelaySamples( ) << endl << endl;
	}

	delete vdl;
}

void test3_lininterp( )
{
	const double sr            = 44.1e3;
	const int bs               = 4;
	CITAVariableDelayLine* vdl = new CITAVariableDelayLine( sr, bs, 1, EVDLAlgorithm::LINEAR_INTERPOLATION );
	vdl->SetDelaySamples( 0 );

	ITASampleBuffer sbIn( bs, false );
	ITASampleBuffer sbOut( bs, false );

	// Initialize delay line sample values for 3 blocks
	for( int j = 0; j < 6; j++ )
	{
		// Overwrite Input
		for( int i = 0; i < bs; i++ )
			sbIn[i] = (float)( j * bs + i + 1 );

		if( j == 2 )
			vdl->SetDelaySamples( 1 );
		if( j == 4 )
			vdl->SetDelaySamples( 0 );

		vdl->Process( &sbIn, &sbOut );
		cout << "in  = " << sbIn.ValuesToString( ) << endl;
		cout << "out = " << sbOut.ValuesToString( ) << endl;
		cout << "for this block delay was set to " << vdl->GetDelaySamples( ) << endl << endl;
	}

	delete vdl;
}

void test2_lininterp( )
{
	const double sr            = 44.1e3;
	const int bs               = 4;
	CITAVariableDelayLine* vdl = new CITAVariableDelayLine( sr, bs, 1, EVDLAlgorithm::LINEAR_INTERPOLATION );

	ITASampleBuffer sbInit( bs * 2, true );
	ITASampleBuffer sbIn( bs, true );
	ITASampleBuffer sbOut( bs, true );

	// Initialize 2 times buffersize VDL and fill with zeros
	vdl->SetDelaySamples( bs * 2 );
	vdl->Process( &sbInit, &sbOut );
	cout << "out = " << sbOut.ValuesToString( ) << endl;
	cout << "delay was " << vdl->GetDelaySamples( ) << endl << endl;

	// Initialize delay line sample values for 3 blocks
	for( int j = 0; j < 5; j++ )
	{
		for( int i = 0; i < bs; i++ )
			sbIn[i] = (float)( j * bs + i + 1 );

		if( j == 3 )
			vdl->SetDelaySamples( bs * 2 + 1 );

		vdl->Process( &sbIn, &sbOut );
		cout << "in  = " << sbIn.ValuesToString( ) << endl;
		cout << "out = " << sbOut.ValuesToString( ) << endl;
		cout << "delay was " << vdl->GetDelaySamples( ) << endl << endl;
	}

	vdl->Process( &sbIn, &sbOut );
	cout << "in  = " << sbIn.ValuesToString( ) << endl;
	cout << "out = " << sbOut.ValuesToString( ) << endl;
	cout << "delay was " << vdl->GetDelaySamples( ) << endl << endl;

	delete vdl;
}

void test1_crossfade( )
{
	const double sr            = 44100;
	const int bs               = 8;
	CITAVariableDelayLine* vdl = new CITAVariableDelayLine( sr, bs, 1.0, EVDLAlgorithm::CROSSFADE );

	ITASampleBuffer sbIn( bs, true );
	ITASampleBuffer sbOut( bs, true );
	vdl->SetDelaySamples( 3 );

	for( int i = 0; i < 5; i++ )
	{
		for( int j = 0; j < bs; j++ )
			sbIn[j] = (float)( ( i * bs ) + j + 1 );
		cout << "in = " << sbIn.ValuesToString( ) << endl;

		if( i == 2 )
		{
			cout << "switch to 0" << endl;
			vdl->SetDelaySamples( 0 );
		}

		vdl->Process( &sbIn, &sbOut );

		cout << "out = " << sbOut.ValuesToString( ) << endl;
	}

	delete vdl;
}
