%% load
simo_io = ita_merge( ita_read( 'ITADSPSIMOVDLTest_in.wav' ), ita_read( 'ITADSPSIMOVDLTest_out.wav' ) );

%% prepare
simo_io_snipped = ita_time_crop( simo_io, [ 1 512 ], 'samples' );
simo_io_snipped.comment = 'Single-Input Multiple-Output Variable Delay Line';
simo_io_snipped.channelNames = { 'Sine signal in', ...
    'Read cursor 1 out (no delay)', ...
    'Read cursor 2 out (11 samples delay)', ...
    'Read cursor 3 out (2 blocks delay)', ...
    'Read cursor 4 out (3 blocks delay)' };

%% plot
simo_io_snipped.pt