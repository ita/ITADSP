/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * Processes a sine signal through single-input multiple-output
 * variable delay line with 4 cursors at different delays and
 * exports the i/o streams to hard drive.
 *
 */

#include <ITAAudiofileWriter.h>
#include <ITAFileDataSource.h>
#include <ITASIMOVariableDelayLine.h>
#include <ITASampleBuffer.h>
#include <ITASampleFrame.h>
#include <ITAStreamFunctionGenerator.h>
#include <ITAStreamInfo.h>
#include <ITAStringUtils.h>
#include <iostream>
#include <math.h>
#include <vector>

using namespace std;

const unsigned int iBlockLength      = 128;
const double dSampleRate             = 44.1e3;
const float fMaxReservedDelaySamples = 5 * iBlockLength;
const float fSimulateSeconds         = 10;
const float fInitialDelaySamples     = 10 * 2 * iBlockLength - 1;

const string sInFilePath  = "ITADSPSIMOVDLTest_in.wav";
const string sOutFilePath = "ITADSPSIMOVDLTest_out.wav";

int main( int, char** )
{
	ITAStreamFunctionGenerator sinesignal( 1, dSampleRate, iBlockLength, ITAStreamFunctionGenerator::SINE, 500.0f, 0.9f, true );
	// ITAFileDatasource filesignal( "cl-mod-bb-piece-32.wav", iBlockLength, true );

	ITADatasource* pIntputStream = &sinesignal;

	CITASIMOVariableDelayLine* pSIMOVDL = new CITASIMOVariableDelayLine( dSampleRate, iBlockLength, fMaxReservedDelaySamples, EVDLAlgorithm::CUBIC_SPLINE_INTERPOLATION );


	double dSamplerate            = dSampleRate;
	unsigned int uiBlocklength    = iBlockLength;
	unsigned int uiNumberOfFrames = (unsigned int)std::ceil( dSamplerate * fSimulateSeconds / (float)uiBlocklength );

	int iCursor0 = pSIMOVDL->AddCursor( );
	pSIMOVDL->SetDelaySamples( iCursor0, .0f );

	int iCursor1 = pSIMOVDL->AddCursor( );
	pSIMOVDL->SetDelaySamples( iCursor1, 11.0f );

	int iCursor2 = pSIMOVDL->AddCursor( );
	pSIMOVDL->SetDelaySamples( iCursor2, float( uiBlocklength * 2 ) );

	int iCursor3 = pSIMOVDL->AddCursor( );
	pSIMOVDL->SetDelaySamples( iCursor3, float( uiBlocklength * 3 ) );

	ITAAudiofileProperties props_in;
	props_in.iChannels            = 1;
	props_in.dSampleRate          = dSamplerate;
	props_in.eQuantization        = ITAQuantization::ITA_FLOAT;
	props_in.eDomain              = ITADomain::ITA_TIME_DOMAIN;
	props_in.iLength              = uiNumberOfFrames * uiBlocklength;
	ITAAudiofileWriter* writer_in = ITAAudiofileWriter::create( sInFilePath, props_in );
	ITAAudiofileProperties props_out( props_in );
	props_out.iChannels            = pSIMOVDL->GetNumCursors( );
	ITAAudiofileWriter* writer_out = ITAAudiofileWriter::create( sOutFilePath, props_out );

	ITAStreamInfo oState;

	ITASampleBuffer* psbInput = new ITASampleBuffer( uiBlocklength, true );
	ITASampleFrame* psfOutput = new ITASampleFrame( pSIMOVDL->GetNumCursors( ), uiBlocklength, true );

	cout << "Input file: " << sInFilePath << endl;
	cout << "Processing ";

	unsigned int n = 0;
	while( n < uiNumberOfFrames )
	{
		// Add new samples
		psbInput->write( pIntputStream->GetBlockPointer( 0, &oState ), uiBlocklength );
		pSIMOVDL->WriteBlock( psbInput );

		pSIMOVDL->ReadBlockAndIncrement( psfOutput );

		std::vector<float*> pIn;
		pIn.push_back( psbInput->data( ) );
		writer_in->write( uiBlocklength, pIn );
		writer_out->write( psfOutput, uiBlocklength );
		n++;

		pIntputStream->IncrementBlockPointer( );

		if( n % ( uiNumberOfFrames / 40 ) == 0 )
			cout << ".";
	}

	cout << " done." << endl;
	cout << "Output file: " << sOutFilePath << endl;

	delete writer_in;
	delete writer_out;

	delete psbInput;
	delete psfOutput;

	return 255;
}
