/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * Circulates a sound source in a shoebox room including specular
 * reflections off walls (perfectly hard).
 *
 */

#include <ITAAudiofileWriter.h>
#include <ITAFileDataSource.h>
#include <ITASIMOVariableDelayLine.h>
#include <ITASampleBuffer.h>
#include <ITASampleFrame.h>
#include <ITAStreamFunctionGenerator.h>
#include <ITAStreamInfo.h>
#include <ITAStringUtils.h>
#include <cassert>
#include <cmath>
#include <iostream>
#include <vector>

using namespace std;

const float fSimulateSeconds = 20;    // s
const float fShoeboxLength   = 10.0f; // m
const float fShoeboxWidth    = 7.0f;  // m
const float fShoeboxHeight   = 3.0f;  // m

const float fCircleRadiusHorizontal = 3.0f; // m
const float fCircleDuration         = 3.0f; // s

const float fSpeedOfSound = 343.0f; // m/s

const string sInFilePath  = "CirculatingSource_Signal.wav";
const string sOutFilePath = "CirculatingSource_ShoeboxRoom.wav";

const unsigned int iBlockLength = 128;
const double dSampleRate        = 44.1e3;

float DistanceToPropagationTime( const float fDistanceMeter )
{
	return fDistanceMeter / fSpeedOfSound;
}

int main( int, char** )
{
	assert( fCircleRadiusHorizontal * 2 < fShoeboxLength );
	assert( fCircleRadiusHorizontal * 2 < fShoeboxWidth );
	assert( 1.7f < fShoeboxHeight );

	ITAStreamFunctionGenerator sinesignal( 1, dSampleRate, iBlockLength, ITAStreamFunctionGenerator::SINE, 500.0f, 0.5f, true );
	ITAFileDatasource filesignal( "sine.wav", iBlockLength, true );
	ITADatasource* pIntputStream = &filesignal;

	assert( fShoeboxLength > fShoeboxWidth );
	const float fMaxReservedDelaySamples = float( fShoeboxLength * pow( 2, 1 ) / double( fSpeedOfSound ) * dSampleRate );
	CITASIMOVariableDelayLine* pSIMOVDL = new CITASIMOVariableDelayLine( dSampleRate, iBlockLength, fMaxReservedDelaySamples, EVDLAlgorithm::CUBIC_SPLINE_INTERPOLATION );

	unsigned int uiNumberOfFrames = (unsigned int)ceil( dSampleRate * fSimulateSeconds / (float)iBlockLength );

	// OpenGL coordinates
	const int iCursorDirect    = pSIMOVDL->AddCursor( );
	const int iCursorPositiveX = pSIMOVDL->AddCursor( );
	const int iCursorNegativeX = pSIMOVDL->AddCursor( );
	const int iCursorPositiveY = pSIMOVDL->AddCursor( );
	const int iCursorNegativeY = pSIMOVDL->AddCursor( );
	const int iCursorPositiveZ = pSIMOVDL->AddCursor( );
	const int iCursorNegativeZ = pSIMOVDL->AddCursor( );

	ITAAudiofileProperties props_in;
	props_in.iChannels            = 1;
	props_in.dSampleRate          = dSampleRate;
	props_in.eQuantization        = ITAQuantization::ITA_FLOAT;
	props_in.eDomain              = ITADomain::ITA_TIME_DOMAIN;
	props_in.iLength              = (unsigned int)uiNumberOfFrames * iBlockLength;
	ITAAudiofileWriter* writer_in = ITAAudiofileWriter::create( sInFilePath, props_in );
	ITAAudiofileProperties props_out( props_in );
	props_out.iChannels            = pSIMOVDL->GetNumCursors( );
	ITAAudiofileWriter* writer_out = ITAAudiofileWriter::create( sOutFilePath, props_out );

	ITAStreamInfo oState;

	ITASampleBuffer* psbInput = new ITASampleBuffer( iBlockLength, true );
	ITASampleFrame* psfOutput = new ITASampleFrame( pSIMOVDL->GetNumCursors( ), iBlockLength, true );

	cout << "Input file: " << sInFilePath << endl;
	cout << "Processing ";

	unsigned int n = 0;
	while( n < uiNumberOfFrames )
	{
		// Set new delays
		const double dT = double( n ) * double( iBlockLength ) / dSampleRate;

		const float fX = fCircleRadiusHorizontal * float( sin( dT / fCircleDuration ) );
		const float fY = 1.7f;
		const float fZ = -fCircleRadiusHorizontal * float( cos( dT / fCircleDuration ) );

		// Direct
		const float fDelay = DistanceToPropagationTime( sqrt( fX * fX + fZ * fZ ) );
		pSIMOVDL->SetDelayTime( iCursorDirect, fDelay );

		// Reflection positive X (right wall)
		const float fX_ReflectionPositiveX = fShoeboxWidth - fX;
		pSIMOVDL->SetDelayTime( iCursorPositiveX, DistanceToPropagationTime( sqrt( fX_ReflectionPositiveX * fX_ReflectionPositiveX + fZ * fZ ) ) );

		// Reflection negative X (left wall)
		const float fX_ReflectionNegativeX = -fShoeboxWidth + fX;
		pSIMOVDL->SetDelayTime( iCursorNegativeX, DistanceToPropagationTime( sqrt( fX_ReflectionNegativeX * fX_ReflectionNegativeX + fZ * fZ ) ) );

		// Reflection positive Y (ceiling)
		const float fY_ReflectionPositiveY = fShoeboxHeight - fY;
		pSIMOVDL->SetDelayTime( iCursorPositiveY, DistanceToPropagationTime( sqrt( fX * fX + fY_ReflectionPositiveY * fY_ReflectionPositiveY + fZ * fZ ) ) );

		// Reflection negative Y (floor)
		const float fY_ReflectionNegativeY = -fShoeboxHeight + fY;
		pSIMOVDL->SetDelayTime( iCursorNegativeY, DistanceToPropagationTime( sqrt( fX * fX + fY_ReflectionNegativeY * fY_ReflectionNegativeY + fZ * fZ ) ) );

		// Reflection positive Z (rear wall)
		const float fZ_ReflectionPositiveZ = fShoeboxLength - fZ;
		pSIMOVDL->SetDelayTime( iCursorPositiveZ, DistanceToPropagationTime( sqrt( fX * fX + fZ_ReflectionPositiveZ * fZ_ReflectionPositiveZ ) ) );

		// Reflection negative Z (front wall)
		const float fZ_ReflectionNegativeZ = -fShoeboxLength + fZ;
		pSIMOVDL->SetDelayTime( iCursorNegativeZ, DistanceToPropagationTime( sqrt( fX * fX + fZ_ReflectionNegativeZ * fZ_ReflectionNegativeZ ) ) );


		// Process
		psbInput->write( pIntputStream->GetBlockPointer( 0, &oState ), iBlockLength );
		pSIMOVDL->WriteBlock( psbInput );
		pSIMOVDL->ReadBlockAndIncrement( psfOutput );

		std::vector<float*> pIn;
		pIn.push_back( psbInput->data( ) );
		writer_in->write( iBlockLength, pIn );
		writer_out->write( psfOutput, iBlockLength );
		n++;

		pIntputStream->IncrementBlockPointer( );

		if( n % ( uiNumberOfFrames / 40 ) == 0 )
			cout << ".";
	}

	cout << " done." << endl;
	cout << "Output file: " << sOutFilePath << endl;

	delete writer_in;
	delete writer_out;

	delete psbInput;
	delete psfOutput;

	return 255;
}
