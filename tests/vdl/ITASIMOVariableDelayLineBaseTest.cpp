/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 * Processes a sine signal through single-input multiple-output
 * variable delay line base with 1 cursors and variying delay
 * but without interpolating read-out (cp VDL without "Base")
 *
 */

#include "VistaTools\VistaRandomNumberGenerator.h"

#include <ITAAudiofileWriter.h>
#include <ITAFileDataSource.h>
#include <ITASIMOVariableDelayLineBase.h>
#include <ITASampleBuffer.h>
#include <ITASampleFrame.h>
#include <ITAStreamFunctionGenerator.h>
#include <ITAStreamInfo.h>
#include <ITAStringUtils.h>
#include <algorithm>
#include <cassert>
#include <iostream>
#include <math.h>
#include <vector>

using namespace std;

const unsigned int iBlockLength    = 128;
const double dSampleRate           = 44.1e3;
const int iMaxReservedDelaySamples = 5 * iBlockLength;
const float fSimulateSeconds       = 10;
const float fInitialDelaySamples   = 10 * 2 * iBlockLength - 1;

const string sInFilePath   = "ITASIMOVariableDelayLineBaseTest_in.wav";
const string sOutFilePath1 = "ITASIMOVariableDelayLineBaseTest_out1.wav";
const string sOutFilePath2 = "ITASIMOVariableDelayLineBaseTest_out2.wav";

int main( int, char** )
{
	ITAStreamFunctionGenerator sinesignal( 1, dSampleRate, iBlockLength, ITAStreamFunctionGenerator::SINE, 500.0f, 0.9f, true );

	ITADatasource* pIntputStream = &sinesignal;

	auto pSIMOVDL = new CITASIMOVariableDelayLineBase( iMaxReservedDelaySamples );

	double dSamplerate            = dSampleRate;
	unsigned int uiBlocklength    = iBlockLength;
	unsigned int uiNumberOfFrames = (unsigned int)std::ceil( dSamplerate * fSimulateSeconds / (float)uiBlocklength );

	int iCursor1 = pSIMOVDL->AddCursor( );
	pSIMOVDL->SetDelaySamples( iCursor1, 0 );
	pSIMOVDL->SetOverlapSamples( iCursor1, 0, 0 );
	int iCursor2 = pSIMOVDL->AddCursor( );
	pSIMOVDL->SetDelaySamples( iCursor2, 120 );
	pSIMOVDL->SetOverlapSamples( iCursor2, 1, 1 ); // E.g. for linear interpolation one additional sample at left & right bounds

	ITAAudiofileProperties props;
	props.iChannels                        = 1;
	props.dSampleRate                      = dSamplerate;
	props.eQuantization                    = ITAQuantization::ITA_FLOAT;
	props.eDomain                          = ITADomain::ITA_TIME_DOMAIN;
	props.iLength                          = uiNumberOfFrames * uiBlocklength;
	ITAAudiofileWriter* writer_out_cursor2 = ITAAudiofileWriter::create( sOutFilePath2, props );
	ITAAudiofileWriter* writer_in          = ITAAudiofileWriter::create( sInFilePath, props );
	ITAAudiofileWriter* writer_out_cursor1 = ITAAudiofileWriter::create( sOutFilePath1, props );

	ITASampleFrame psbOutputCursor2_all( 1, props.iLength, true );

	ITAStreamInfo oState;

	ITASampleBuffer* psbInput         = new ITASampleBuffer( uiBlocklength, true );
	ITASampleBuffer* psbOutputCursor1 = new ITASampleBuffer( uiBlocklength, true );
	ITASampleBuffer* psbOutputCursor2 = new ITASampleBuffer( iMaxReservedDelaySamples, true );

	cout << "Input file: " << sInFilePath << endl;
	cout << "Processing ";

	unsigned int n                       = 0;
	int iVariableDelaySamples            = pSIMOVDL->GetNewDelaySamples( iCursor2 );
	unsigned int nVariableSamplesWritten = 0;
	while( n < uiNumberOfFrames )
	{
		// Add new samples
		psbInput->write( pIntputStream->GetBlockPointer( 0, &oState ), uiBlocklength );
		pSIMOVDL->Write( (int)uiBlocklength, psbInput->GetData( ) );

		pSIMOVDL->Read( iCursor1, (int)uiBlocklength, psbOutputCursor1->GetData( ) );

		// iVariableLength = VistaRandomNumberGenerator::GetStandardRNG()->GenerateInt32( 1, min( iMaxReservedDelaySamples, int( 2 * uiBlocklength ) ) );
		if( iMaxReservedDelaySamples > iVariableDelaySamples )
			iVariableDelaySamples++;
		pSIMOVDL->SetDelaySamples( iCursor2, iVariableDelaySamples );
		int iEffectiveReadLength = pSIMOVDL->GetEffectiveReadLength( iCursor2, (int)uiBlocklength );
		if( iEffectiveReadLength < psbOutputCursor2->GetLength( ) )
			psbOutputCursor2->Init( iEffectiveReadLength );
		pSIMOVDL->Read( iCursor2, (int)uiBlocklength, psbOutputCursor2->GetData( ) );


		std::vector<float*> pIn;
		pIn.push_back( psbInput->data( ) );
		writer_in->write( uiBlocklength, pIn );

		std::vector<float*> pOut1;
		pOut1.push_back( psbOutputCursor1->data( ) );
		writer_out_cursor1->write( uiBlocklength, pOut1 );

		std::vector<float*> pOut2;
		pOut2.push_back( psbOutputCursor2->GetData( ) + pSIMOVDL->GetOverlappingSamplesLeft( iCursor2 ) );
		if( nVariableSamplesWritten + iVariableDelaySamples < props.iLength )
		{
			writer_out_cursor2->write( iEffectiveReadLength - pSIMOVDL->GetOverlappingSamples( iCursor2 ), pOut2 );
			nVariableSamplesWritten += iVariableDelaySamples;
		}
		else
		{
			int N = props.iLength - nVariableSamplesWritten;
			writer_out_cursor2->write( N, pOut2 );
			nVariableSamplesWritten += N;
		}

		pIntputStream->IncrementBlockPointer( );
		pSIMOVDL->Increment( uiBlocklength );

		n++;

		if( n % ( uiNumberOfFrames / 40 ) == 0 )
			cout << ".";
	}

	if( nVariableSamplesWritten < props.iLength )
	{
		int N = props.iLength - nVariableSamplesWritten;
		psbOutputCursor2->Zero( 0, N );
		std::vector<float*> pOut;
		pOut.push_back( psbOutputCursor2->data( ) );
		writer_out_cursor2->write( N, pOut );
		// writer_out_cursor2->write( &psbOutputCursor2_all );
	}

	assert( nVariableSamplesWritten == props.iLength );

	cout << " done." << endl;
	cout << "Output files: " << sOutFilePath1 << ", " << sOutFilePath2 << endl;

	delete writer_in;
	delete writer_out_cursor1;
	delete writer_out_cursor2;

	delete psbInput;
	delete psbOutputCursor1, psbOutputCursor2;

	return 255;
}
