﻿/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <ITAAudiofileWriter.h>
#include <ITABase/UtilsJSON.h>
#include <ITAFileDataSource.h>
#include <ITAIIRCoefficients.h>
#include <ITAIIRFilterEngine.h>
#include <ITAIIRFilterGenerator.h>
#include <ITAIIRUtils.h>
#include <ITASIMOVariableDelayLine.h>
#include <ITASampleBuffer.h>
#include <ITASampleFrame.h>
#include <ITAStopWatch.h>
#include <ITAStreamFunctionGenerator.h>
#include <ITAStreamInfo.h>
#include <ITAStringUtils.h>
#include <ITAThirdOctaveFIRFilterGenerator.h>
#include <ITAThirdOctaveMagnitudeSpectrum.h>
#include <ccomplex>
#include <iostream>
#include <math.h>
#include <memory>
#include <vector>

using namespace std;
using namespace ITABase;

const float g_fSampleRate = 44100;
const int g_iFIRGenLength = int( 4 * ceil( g_fSampleRate / CThirdOctaveMagnitudeSpectrum::GetCenterFrequencies( )[0] ) );

// Absorption filter
vector<float> vfAbsorptionSpectrumTO = { 0.02f,      0.02f, 0.02f, 0.0233333f, 0.0266667f, 0.03f,      0.03f,      0.03f,      0.03f,      0.03f, 0.03f,
	                                     0.03f,      0.03f, 0.03f, 0.03f,      0.0333333f, 0.0366667f, 0.04f,      0.0433333f, 0.0466667f, 0.05f, 0.0566667f,
	                                     0.0633333f, 0.07f, 0.07f, 0.07f,      0.07f,      0.0733333f, 0.0766667f, 0.08f,      0.08f };

// Trumpet directivity
vector<float> vfTrumpetDirectivitySpectrumTO = {
	0.860280990600586f, 0.860280632972717f,  0.860280632972717f,  0.860280632972717f,  0.860280632972717f,  0.860280632972717f,  0.860280632972717f, 0.860280692577362f,
	0.860280692577362f, 0.863133668899536f,  0.752475023269653f,  0.674751520156860f,  0.499466955661774f,  0.738280415534973f,  0.653443872928619f, 0.507191777229309f,
	0.533296763896942f, 0.503476321697235f,  0.376767426729202f,  0.353374809026718f,  0.269741356372833f,  0.207140043377876f,  0.153062343597412f, 0.112099960446358f,
	0.127615734934807f, 0.0946486070752144f, 0.0785422623157501f, 0.0600289255380631f, 0.0488252453505993f, 0.0387985333800316f, 0.0315645076334477f
};

// Diffracion filter
vector<float> vfDiffractionSpectrumTO = { 0.111934553579764f,  0.106460935614618f,  0.100699704548526f,  0.0947001721104573f,  0.0890866491204974f,  0.0835569704783756f,
	                                      0.0775507540050590f, 0.0722382340567499f, 0.0670519961369095f, 0.0621838133597414f,  0.0565935658858452f,  0.0519303873398500f,
	                                      0.0473436952392266f, 0.0428843783633814f, 0.0389753239378729f, 0.0351914143864374f,  0.0315689677042525f,  0.0284535398945892f,
	                                      0.0255957249048243f, 0.0227266136854582f, 0.0203857703689526f, 0.0182702796747406f,  0.0162996829305633f,  0.0144787461518097f,
	                                      0.0129580533457382f, 0.0115033291355433f, 0.0102513560003608f, 0.00917063160436257f, 0.00820336236165088f, 0.00725137721071123f,
	                                      0.00648611579946720f };


void DesignFromSpectrum( );
void VDLIIRTest( );
void Burg_Time_Test( );


int main( int, char** )
{
	DesignFromSpectrum( );
	// VDLIIRTest();
	// Burg_Time_Test();

	return 255;
}

void DesignFromSpectrum( )
{
	ITADSP::CFilterCoefficients oBurgCoeffsOrder4( 4, false );
	ITADSP::CFilterCoefficients oBurgCoeffsOrder10( 10, false );

	// Generate an impulse response from the given spectra
	ITABase::CThirdOctaveGainMagnitudeSpectrum oMags;
	ITABase::CFiniteImpulseResponse oIRTarget( g_iFIRGenLength, g_fSampleRate );
	CITAThirdOctaveFIRFilterGenerator oIRGenerator( oIRTarget.GetSampleRate( ), oIRTarget.GetLength( ) );

	// Absorption spectrum
	oMags.SetMagnitudes( vfAbsorptionSpectrumTO );
	for( int i = 0; i < oMags.GetNumBands( ); i++ )
		oMags.SetValue( i, sqrt( 1.0f - oMags.GetValues( )[i] ) );
#ifdef WITH_JSON_SUPPORT
	ITABase::Utils::JSON::Export( &oMags, "ReflectionTOSpectrum.json" );
#endif
	oIRGenerator.GenerateFilter( oMags, oIRTarget.GetData( ), true );
	oIRTarget.Store( "ReflectionIR_min_phase.wav" );
	ITADSP::IIRFilterGenerator::Burg( oIRTarget, oBurgCoeffsOrder4 );
	ITADSP::IIRFilterGenerator::Burg( oIRTarget, oBurgCoeffsOrder10 );
#ifdef WITH_JSON_SUPPORT
	ITADSP::ExportIIRCoefficientsToJSON( "ReflectionIIRCoeffsOrder4.json", oBurgCoeffsOrder4 );
	ITADSP::ExportIIRCoefficientsToJSON( "ReflectionIIRCoeffsOrder4.json", oBurgCoeffsOrder10 );
#endif

	// Directivity spectrum
	oMags.SetMagnitudes( vfTrumpetDirectivitySpectrumTO );
#ifdef WITH_JSON_SUPPORT
	ITABase::Utils::JSON::Export( &oMags, "DirectivityTOSpectrum.json" );
#endif
	oIRGenerator.GenerateFilter( oMags, oIRTarget.GetData( ), true );
	oIRTarget.Store( "DirectivityIR_min_phase.wav" );
	oBurgCoeffsOrder4.InitialiseOrder( 4 );
	oBurgCoeffsOrder10.InitialiseOrder( 10 );
	ITADSP::IIRFilterGenerator::Burg( oIRTarget, oBurgCoeffsOrder4 );
	ITADSP::IIRFilterGenerator::Burg( oIRTarget, oBurgCoeffsOrder10 );
#ifdef WITH_JSON_SUPPORT
	ITADSP::ExportIIRCoefficientsToJSON( "DirectivityIIRCoeffsOrder4.json", oBurgCoeffsOrder4 );
	ITADSP::ExportIIRCoefficientsToJSON( "DirectivityIIRCoeffsOrder10.json", oBurgCoeffsOrder10 );
#endif

	// Diffraction spectrum
	oMags.SetMagnitudes( vfDiffractionSpectrumTO );
#ifdef WITH_JSON_SUPPORT
	ITABase::Utils::JSON::Export( &oMags, "DiffractionTOSpectrum.json" );
#endif
	oIRGenerator.GenerateFilter( oMags, oIRTarget.GetData( ), true );
	oIRTarget.Store( "DiffractionIR_min_phase.wav" );
	oBurgCoeffsOrder4.InitialiseOrder( 4 );
	oBurgCoeffsOrder10.InitialiseOrder( 10 );
	ITADSP::IIRFilterGenerator::Burg( oIRTarget, oBurgCoeffsOrder4 );
	ITADSP::IIRFilterGenerator::Burg( oIRTarget, oBurgCoeffsOrder10 );
#ifdef WITH_JSON_SUPPORT
	ITADSP::ExportIIRCoefficientsToJSON( "DiffractionIIRCoeffsOrder4.json", oBurgCoeffsOrder4 );
	ITADSP::ExportIIRCoefficientsToJSON( "DiffractionIIRCoeffsOrder10.json", oBurgCoeffsOrder10 );
#endif
}

void Burg_Time_Test( )
{
	int filter_order = 4;

	// Generate an impulse response (oIR2) from frequency third octave magnitude data
	ITABase::CFiniteImpulseResponse oIR( 1024, g_fSampleRate );
	CITAThirdOctaveFIRFilterGenerator oIRGenerator( oIR.GetSampleRate( ), oIR.GetLength( ) );

	ITABase::CThirdOctaveGainMagnitudeSpectrum oMags;
	oMags.SetMagnitudes( vfAbsorptionSpectrumTO );

	oIRGenerator.GenerateFilter( oMags, oIR.GetData( ), true );

	CITAIIRFilterEngine oIIRFilterEngine( filter_order ); // create an IIR filter

	int max_runs = 100000, max_filter_order = 20;
	int interval     = 10;
	double tolerence = 1e-8;
	int i, j;

	for( j = 1; j < max_filter_order; j++ )
	{ // loop over filer orders
		double running_avg = 0, old_avg = tolerence;

		for( i = 0; i < max_runs; i++ )
		{ // loop over repeated trials to average over

			ITADSP::CFilterCoefficients oBurgCoeffs( j, false );

			ITAStopWatch sw;

			sw.start( ); // start timing
			ITADSP::IIRFilterGenerator::Burg( oIR, oBurgCoeffs );
			double run_time = sw.stop( );

			running_avg += run_time;

			if( i % interval == 0 )
			{
				if( fabs( running_avg - old_avg ) / (double)( i + 1 ) < tolerence )
				{
					break;
				}
				old_avg = running_avg;
			}
		}

		cout << "After " << i - 1 << " runs, the average time to compute the " << j << " th order Burg coefficients was: " << running_avg / ( i + 1 ) << endl;
	}
}

void VDLIIRTest( )
{
	// input parameters
	const unsigned int iBlockLength      = 128;
	const double dSampleRate             = 44.1e3;
	const float fMaxReservedDelaySamples = 5 * iBlockLength;
	const float fSimulateSeconds         = 10;

	const string sInFilePath  = "IIR_SIMOVDL_in.wav";  // file where the input stream to the VDL is written
	const string sOutFilePath = "IIR_SIMOVDL_out.wav"; // file where the data read by the output cursors of the VDL is written
	//-----
	ITAStreamFunctionGenerator sinesignal( 1, dSampleRate, iBlockLength, ITAStreamFunctionGenerator::SINE, 500.0f, 0.9f, true );
	// ITAFileDatasource filesignal( "cl-mod-bb-piece-32.wav", iBlockLength, true );

	ITADatasource* pIntputStream = &sinesignal;

	CITASIMOVariableDelayLine* pSIMOVDL = new CITASIMOVariableDelayLine( dSampleRate, iBlockLength, fMaxReservedDelaySamples, EVDLAlgorithm::CUBIC_SPLINE_INTERPOLATION );


	double dSamplerate            = dSampleRate;
	unsigned int uiBlocklength    = iBlockLength;
	unsigned int uiNumberOfFrames = (unsigned int)std::ceil( dSamplerate * fSimulateSeconds / (float)uiBlocklength );

	int iCursor0 = pSIMOVDL->AddCursor( );    // create read cursors for the vdl
	pSIMOVDL->SetDelaySamples( iCursor0, 0 ); // set how many samples the cursor is delayed by from the write cursor

	int iCursor1 = pSIMOVDL->AddCursor( );
	pSIMOVDL->SetDelaySamples( iCursor1, 11 );

	int iCursor2 = pSIMOVDL->AddCursor( );
	pSIMOVDL->SetDelaySamples( iCursor2, int( uiBlocklength * 2 ) );

	int iCursor3 = pSIMOVDL->AddCursor( );
	pSIMOVDL->SetDelaySamples( iCursor3, int( uiBlocklength * 3 ) );

	ITAAudiofileProperties props_in;
	props_in.iChannels            = 1;
	props_in.dSampleRate          = dSamplerate;
	props_in.eQuantization        = ITAQuantization::ITA_FLOAT;
	props_in.eDomain              = ITADomain::ITA_TIME_DOMAIN;
	props_in.iLength              = uiNumberOfFrames * uiBlocklength;
	ITAAudiofileWriter* writer_in = ITAAudiofileWriter::create( sInFilePath, props_in );
	ITAAudiofileProperties props_out( props_in );
	props_out.iChannels            = pSIMOVDL->GetNumCursors( ); // set number of channels for output data to 4 (number of read cursors on VDL)
	ITAAudiofileWriter* writer_out = ITAAudiofileWriter::create( sOutFilePath, props_out );

	ITAStreamInfo oState;

	ITASampleBuffer* psbInput  = new ITASampleBuffer( uiBlocklength, true );
	ITASampleFrame* psfOutput  = new ITASampleFrame( pSIMOVDL->GetNumCursors( ), uiBlocklength, true );
	ITASampleFrame* psfOut_IIR = new ITASampleFrame( pSIMOVDL->GetNumCursors( ), uiBlocklength, true );

	//-----------------set up filters---------------------------

	int filter_order  = 4;
	int filter_length = 1024;

	ITABase::CThirdOctaveGainMagnitudeSpectrum oMags1; // set magnitude responses
	oMags1.SetMagnitudes( vfAbsorptionSpectrumTO );

	ITABase::CThirdOctaveGainMagnitudeSpectrum oMags2;
	oMags2.SetMagnitudes( vfTrumpetDirectivitySpectrumTO );

	ITABase::CThirdOctaveGainMagnitudeSpectrum oMags3;
	oMags3.SetMagnitudes( vfDiffractionSpectrumTO );

	ITABase::CThirdOctaveGainMagnitudeSpectrum oMags4;
	oMags4.SetIdentity( );


	CITAThirdOctaveFIRFilterGenerator oIRGenerator( g_fSampleRate, filter_length ); // create FIR filter generator, used to transform freq response to IR

	ITABase::CFiniteImpulseResponse oIR1( filter_length, g_fSampleRate ); // create empty IR
	oIRGenerator.GenerateFilter( oMags1, oIR1.GetData( ), false );        // generate IR from magnitude response

	ITABase::CFiniteImpulseResponse oIR2( filter_length, g_fSampleRate );
	oIRGenerator.GenerateFilter( oMags2, oIR2.GetData( ), false );

	ITABase::CFiniteImpulseResponse oIR3( filter_length, g_fSampleRate );
	oIRGenerator.GenerateFilter( oMags3, oIR3.GetData( ), false );

	ITABase::CFiniteImpulseResponse oIR4( filter_length, g_fSampleRate );
	oIRGenerator.GenerateFilter( oMags4, oIR4.GetData( ), false );


	CITAIIRFilterEngine oIIRFilterEngine1( filter_order ); // create an IIR filter
	ITADSP::CFilterCoefficients oFilterCoeffs1( filter_order, false );
	ITADSP::IIRFilterGenerator::Burg( oIR1, oFilterCoeffs1 );
	oIIRFilterEngine1.SetCoefficients( oFilterCoeffs1 ); // set filter coefficients to those calculated with Burg

	CITAIIRFilterEngine oIIRFilterEngine2( filter_order ); // create an IIR filter
	ITADSP::CFilterCoefficients oFilterCoeffs2( filter_order, false );
	ITADSP::IIRFilterGenerator::Burg( oIR2, oFilterCoeffs2 );
	oIIRFilterEngine2.SetCoefficients( oFilterCoeffs2 ); // set filter coefficients to those calculated with Burg

	CITAIIRFilterEngine oIIRFilterEngine3( filter_order ); // create an IIR filter
	ITADSP::CFilterCoefficients oFilterCoeffs3( filter_order, false );
	ITADSP::IIRFilterGenerator::Burg( oIR3, oFilterCoeffs3 );
	oIIRFilterEngine3.SetCoefficients( oFilterCoeffs3 ); // set filter coefficients to those calculated with Burg

	CITAIIRFilterEngine oIIRFilterEngine4( filter_order ); // create an IIR filter
	ITADSP::CFilterCoefficients oFilterCoeffs4( filter_order, false );
	ITADSP::IIRFilterGenerator::Burg( oIR4, oFilterCoeffs4 );
	oIIRFilterEngine4.SetCoefficients( oFilterCoeffs4 ); // set filter coefficients to those calculated with Burg


	//-----------begin
	cout << "Input file: " << sInFilePath << endl;
	cout << "Processing ";

	unsigned int n = 0;
	while( n < uiNumberOfFrames )
	{
		// Add new samples
		psbInput->write( pIntputStream->GetBlockPointer( 0, &oState ), uiBlocklength ); // set the data for a samplebuffer = the next bit of a sine wave
		pSIMOVDL->WriteBlock( psbInput );                                               // write the samplebuffer length blocksize to the VDL

		pSIMOVDL->ReadBlockAndIncrement( psfOutput ); // read VDL and increment cursor positions

		//--------------------------apply filters
		oIIRFilterEngine1.Process( ( *psfOutput )[0].GetData( ), ( *psfOut_IIR )[0].GetData( ), uiBlocklength ); // apply the filter to an input signal
		oIIRFilterEngine2.Process( ( *psfOutput )[1].GetData( ), ( *psfOut_IIR )[1].GetData( ), uiBlocklength ); // apply the filter to an input signal
		oIIRFilterEngine3.Process( ( *psfOutput )[2].GetData( ), ( *psfOut_IIR )[2].GetData( ), uiBlocklength ); // apply the filter to an input signal
		oIIRFilterEngine4.Process( ( *psfOutput )[3].GetData( ), ( *psfOut_IIR )[3].GetData( ), uiBlocklength ); // apply the filter to an input signal

		// ITASampleBuffer &test = (*psfOutput)[0];
		//--------------------------------------

		std::vector<float*> pIn;
		pIn.push_back( psbInput->data( ) );     // adds the data written on the VDL to the end of pIn
		writer_in->write( uiBlocklength, pIn ); // write pIn
		// writer_out->write(psfOutput, uiBlocklength); //write the outputs of the VDL
		writer_out->write( psfOut_IIR, uiBlocklength ); // write the outputs of the VDL

		n++;

		pIntputStream->IncrementBlockPointer( ); // increment the input stream to the VDL

		if( n % ( uiNumberOfFrames / 40 ) == 0 ) // show progress
			cout << ".";
	}

	cout << " done." << endl;
	cout << "Output file: " << sOutFilePath << endl;

	delete writer_in;
	delete writer_out;

	delete psbInput;
	delete psfOutput;

	return;
}
