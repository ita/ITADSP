function r = ita_result_from_json( file_path )
%ITA_RESULT_FROM_JSON Loads a spectrum from json format file

json_t = jsondecode( fileread( file_path ) );
r = itaResult();
r.domain = 'freq';

fn = fields( json_t.spectrum );
r.freqVector = zeros( numel( fn ), 1 );
r.freqData = zeros( numel( fn ), 1 );

for n = 1:numel( fn )
    r.freqVector( n ) = json_t.spectrum.( fn{ n } ).center_frequency;
    r.freqData( n ) = json_t.spectrum.( fn{ n } ).value;
end

end

