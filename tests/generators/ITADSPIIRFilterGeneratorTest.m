%% Reflection (from absorption data)

reflection_ir = ita_read( 'ReflectionIR_min_phase.wav' );
reflection_ir.channelNames = { 'C++ IR from spectrum generator' };

excitation_ir = ita_generate_impulse( 'fftDegree', reflection_ir.fftDegree );

reflection_coeffs4 = jsondecode( fileread( 'ReflectionIIRCoeffsOrder4.json' ) );
reflection_iir_o4 = dsp.IIRFilter;
reflection_iir_o4.Numerator = [ reflection_coeffs4.numerator.b0 0 0 0 0 ];
reflection_iir_o4.Denominator = [ reflection_coeffs4.denominator.a1 ...
                               reflection_coeffs4.denominator.a2 ...
                               reflection_coeffs4.denominator.a3 ...
                               reflection_coeffs4.denominator.a4 ...
                               reflection_coeffs4.denominator.a5 ];

reflection_ir_filtering = reflection_ir;
reflection_ir_filtering.timeData = reflection_iir_o4( excitation_ir.timeData );
reflection_ir_filtering.channelNames = { 'Matlab IIR response order 4' };

refl_irs = ita_merge( reflection_ir, reflection_ir_filtering );
refl_irs.signalType = 'energy';

refl_rs = ita_spk2frequencybands( refl_irs );

R_r = ita_result_from_json( 'ReflectionTOSpectrum.json' );
R_r.freqVector = refl_rs.freqVector;
R_r.channelNames = { 'Original third-octave band spectrum' };

refl_rs = ita_merge( R_r, refl_rs );
refl_rs.pf
title 'Reflection filtering (third octave energetic band spectrum)'


%% Diffraction

diffraction_ir = ita_read( 'DiffractionIR_min_phase.wav' );
diffraction_ir.channelNames = { 'Diffraction IR from C++ generator' };
diffraction_coeffs = jsondecode( fileread( 'DiffractionIIRCoeffs.json' ) );
diffraction_iir = dsp.IIRFilter;
diffraction_iir.Numerator = [ diffraction_coeffs.numerator.b0 ];
diffraction_iir.Denominator = zeros( 1, 5 );
for i = 1:5
    diffraction_iir.Denominator( i ) = diffraction_coeffs.denominator.( [ 'a' num2str( i ) ] );
end
diffraction_ir_filtering = diffraction_ir;
diffraction_ir_filtering.timeData = diffraction_iir( excitation_ir.timeData );
diffraction_ir_filtering.channelNames = { 'Matlab IIR response (order 4)' };

diffraction_irs = ita_merge( diffraction_ir, diffraction_ir_filtering );
diffraction_irs.signalType = 'energy';
diffr_rs = ita_spk2frequencybands( diffraction_irs );

diffr_target_r = ita_result_from_json( 'DiffractionTOSpectrum.json' );
diffr_target_r.freqVector = diffr_rs.freqVector;
diffr_target_r.freqData = diffr_target_r.freqData;
diffr_target_r.channelNames = { 'Original third-octave band spectrum' };

dir_rs = ita_merge( diffr_target_r, diffr_rs );
dir_rs.pf
title 'Diffraction filtering (third octave energetic band spectrum)'

%% Directivity

dir_ir = ita_read( 'DirectivityIR_min_phase.wav' );
dir_ir.channelNames = { 'Directivity IR from C++ generator' };

directivity_coeffs4 = jsondecode( fileread( 'DirectivityIIRCoeffsOrder4.json' ) );
directivity_iir4 = dsp.IIRFilter;
directivity_iir4.Numerator = [ directivity_coeffs4.numerator.b0 ];
directivity_iir4.Denominator = zeros( 1, 5 );
for i = 1:5
    directivity_iir4.Denominator( i ) = directivity_coeffs4.denominator.( [ 'a' num2str( i ) ] );
end
directivity_ir_filtering_4 = dir_ir;
directivity_ir_filtering_4.timeData = directivity_iir4( excitation_ir.timeData );
directivity_ir_filtering_4.channelNames = { 'Matlab IIR response (order 4)' };


directivity_coeffs10 = jsondecode( fileread( 'DirectivityIIRCoeffsOrder10.json' ) );
directivity_iir10 = dsp.IIRFilter;
directivity_iir10.Numerator = [ directivity_coeffs10.numerator.b0 ];
directivity_iir10.Denominator = zeros( 1, 11 );
for i = 1:11
    directivity_iir10.Denominator( i ) = directivity_coeffs10.denominator.( [ 'a' num2str( i ) ] );
end
directivity_ir_filtering_10 = dir_ir;
directivity_ir_filtering_10.timeData = directivity_iir10( excitation_ir.timeData );
directivity_ir_filtering_10.channelNames = { 'Matlab IIR response (order 10)' };

directivity_irs = ita_merge( dir_ir, directivity_ir_filtering_4, directivity_ir_filtering_10 );
directivity_irs.signalType = 'energy';
dir_rs = ita_spk2frequencybands( directivity_irs );

dir_target_r = ita_result_from_json( 'DirectivityTOSpectrum.json' );
dir_target_r.freqVector = dir_rs.freqVector;
dir_target_r.freqData = dir_target_r.freqData;
dir_target_r.channelNames = { 'Original third-octave band spectrum' };

dir_rs = ita_merge( dir_target_r, dir_rs );
dir_rs.pf
title 'Directivity filtering (third octave energetic band spectrum)'

