%% ITADSPThirdOctaveFilterGeneratorFIRTest evaluation Matlab script

% Loads the FIR filters resulting from the test ITADSPThirdOctaveFilterGeneratorFIRTest_Identity_FIR
% Requires ITA Toolbox, see http://ita-toolbox.org

dir_lin_phase_ir1 = ita_read( 'DirectivityExample_FIR_linear_phase.wav' );
dir_min_phase_ir2 = ita_read( 'DirectivityExample_FIR_minimum_phase.wav' );

dir_generated_irs = ita_merge( dir_lin_phase_ir1, dir_min_phase_ir2 );
dir_generated_irs.channelNames = { 'FIR linear phase', 'FIR minimum phase' };
dir_generated_irs.signalType = 'energy';
dirs_generated_t = ita_spk2frequencybands( dir_generated_irs );

dir_original_t = ita_result_from_json( 'DirectivityExampleTOSpectrum.json' );
dir_original_t.freqVector = dirs_generated_t.freqVector;
dir_original_t.channelNames = { 'Original third-octave band spectrum' };
dir_target_gain = rms( dir_original_t.freqData );

dir_rs = ita_merge( dir_original_t, dirs_generated_t );
dir_rs.pf
title 'FIR filtering from a directivity (third octave energetic band spectrum)'
