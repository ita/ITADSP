﻿#include <ITAThirdOctaveFIRFilterGenerator.h>
#include <ITAAudiofileWriter.h>
#include <ITABase/UtilsJSON.h>
#include <ITASampleBuffer.h>
#include <ITASampleFrame.h>
#include <ITAStopWatch.h>
#include <ITAStringUtils.h>
#include <ITAThirdOctaveFilterbank.h>
#include <iostream>

using namespace ITABase;
using namespace std;

const double g_dSampleRate = 44100;
const int g_iFilterLength  = int( 4 * ceil( g_dSampleRate / CThirdOctaveMagnitudeSpectrum::GetCenterFrequencies( )[0] ) );

void TestThirdOctaveFilterGeneratorFIRIdentity( );
void TestThirdOctaveFilterGeneratorFIRZero( );
void TestThirdOctaveFilterGeneratorFIRSingleBands( );
void TestThirdOctaveFilterGeneratorFIRDirectivityExample( );

int main( int, char** )
{
	TestThirdOctaveFilterGeneratorFIRIdentity( );
	TestThirdOctaveFilterGeneratorFIRZero( );
	TestThirdOctaveFilterGeneratorFIRSingleBands( );
	TestThirdOctaveFilterGeneratorFIRDirectivityExample( );

	return 255;
}

void TestThirdOctaveFilterGeneratorFIRIdentity( )
{
	CThirdOctaveGainMagnitudeSpectrum oMags;
	oMags.SetIdentity( );

	ITASampleBuffer oFilter( g_iFilterLength );

	CITAThirdOctaveFIRFilterGenerator oFilterGenerator( g_dSampleRate, g_iFilterLength );
	oFilterGenerator.GenerateFilter( oMags, oFilter.GetData( ) );

	string sFilePath = "ITADSPThirdOctaveFilterGeneratorFIRTest_Identity_FIR.wav";
	writeAudiofile( sFilePath, &oFilter, g_dSampleRate );
	cout << "Exported result to " << sFilePath << endl;
}

void TestThirdOctaveFilterGeneratorFIRZero( )
{
	CThirdOctaveGainMagnitudeSpectrum oMags;
	oMags.SetZero( );

	ITASampleBuffer oFilter( g_iFilterLength );

	CITAThirdOctaveFIRFilterGenerator oFilterGenerator( g_dSampleRate, g_iFilterLength );
	oFilterGenerator.GenerateFilter( oMags, oFilter.GetData( ) );

	string sFilePath = "ITADSPThirdOctaveFilterGeneratorFIRTest_Zero_FIR.wav";
	writeAudiofile( sFilePath, &oFilter, g_dSampleRate );
	cout << "Exported result to " << sFilePath << endl;
}

void TestThirdOctaveFilterGeneratorFIRSingleBands( )
{
	ITASampleFrame oFilter( CThirdOctaveMagnitudeSpectrum::GetNumBands( ), g_iFilterLength, true );

	CITAThirdOctaveFIRFilterGenerator oFilterGenerator( g_dSampleRate, g_iFilterLength );

	CThirdOctaveGainMagnitudeSpectrum oMags;
	for( int i = 0; i < CThirdOctaveMagnitudeSpectrum::GetNumBands( ); i++ )
	{
		oMags.SetZero( );
		oMags[i] = 1.0f;
		oFilterGenerator.GenerateFilter( oMags, oFilter[i].GetData( ) );
	}

	string sFilePath = "ITADSPThirdOctaveFilterGeneratorFIRTest_SingleBands_FIR.wav";
	writeAudiofile( sFilePath, &oFilter, g_dSampleRate );
	cout << "Exported result to " << sFilePath << endl;
}

void TestThirdOctaveFilterGeneratorFIRDirectivityExample( )
{
	std::vector<float> vfDirectivityMags = {
		0.9110f, 0.9110f, 0.9110f, 0.9103f, 0.9092f, 0.9074f, 0.9039f, 0.8980f, 0.8872f, 0.8645f, 0.8262f, 0.7629f, 0.6695f, 0.5598f, 0.4747f, 0.4392f,
		0.5202f, 0.8082f, 0.6256f, 0.2545f, 0.3255f, 0.1991f, 0.2711f, 0.2834f, 0.2163f, 0.1293f, 0.0362f, 0.0666f, 0.0367f, 0.0633f, 0.0633f,
	};

	CThirdOctaveGainMagnitudeSpectrum oMags;
	oMags.SetMagnitudes( vfDirectivityMags );
	ITABase::Utils::JSON::Export( &oMags, "DirectivityExampleTOSpectrum.json" );

	ITASampleBuffer oFilter1( g_iFilterLength, true );
	CITAThirdOctaveFIRFilterGenerator oFilterGenerator1( g_dSampleRate, oFilter1.GetLength( ) );
	oFilterGenerator1.GenerateFilter( oMags, oFilter1, false );
	string sFilePath = "DirectivityExample_FIR_linear_phase.wav";
	writeAudiofile( sFilePath, &oFilter1, g_dSampleRate );
	cout << "Exported result to " << sFilePath << endl;

	ITASampleBuffer oFilter2( g_iFilterLength, true );
	CITAThirdOctaveFIRFilterGenerator oFilterGenerator2( g_dSampleRate, oFilter2.GetLength( ) );
	oFilterGenerator2.GenerateFilter( oMags, oFilter2, true );
	sFilePath = "DirectivityExample_FIR_minimum_phase.wav";
	writeAudiofile( sFilePath, &oFilter2, g_dSampleRate );
	cout << "Exported result to " << sFilePath << endl;
}
