from pathlib import Path

import numpy as np

# get the current script dir
script_dir = Path(__file__).parent


# y = [np.cos(np.pi / 2 * (1 - v)) for v in x]
# y1 = [np.cos(np.pi / 2 * v) for v in x]

# p1 = [-0.8 * np.cos(np.pi / 2 * (1 - v)) ** 2 for v in x]
# p2 = [0.9 * np.cos(np.pi / 2 * v) ** 2 for v in x]
# # add p1 and p2
# p = [a + b for a, b in zip(p1, p2)]

# print(p)


# exit(0)

# Parameters
num_samples = 1000
samples = np.linspace(0, 1, num_samples)


# linear cross fade
def linear_cross_fade(samples):
    return np.vstack([1 - samples, samples]).T


# exponential cross fade
def exponential_cross_fade(samples):
    B = 4
    return np.vstack(
        [
            (np.exp(B * (1 - samples)) - 1) / (np.exp(B) - 1),
            (np.exp(B * samples) - 1) / (np.exp(B) - 1),
        ]
    ).T


# logarithmic cross fade
def logarithmic_cross_fade(samples):
    A = 10
    return np.vstack(
        [
            np.log(1 + A * (1 - samples)) / np.log(1 + A),
            np.log(1 + A * samples) / np.log(1 + A),
        ]
    ).T


# square root cross fade
def square_root_cross_fade(samples):
    return np.vstack([np.sqrt(1 - samples), np.sqrt(samples)]).T


# cubic cross fade
def cubic_cross_fade(samples):
    return np.vstack([(1 - samples) ** 3, samples**3]).T


# quadratic cross fade
def quadratic_cross_fade(samples):
    return np.vstack([(1 - samples) ** 2, samples**2]).T


# cosine cross fade
def cosine_cross_fade(samples):
    return np.vstack([np.cos(np.pi / 2 * samples), np.cos(np.pi / 2 * (1 - samples))]).T


# raised cosine cross fade
def raised_cosine_cross_fade(samples):
    return np.vstack(
        [1 - (0.5 - 0.5 * np.cos(np.pi * samples)), 0.5 - 0.5 * np.cos(np.pi * samples)]
    ).T


# cosine squared cross fade same as raised cosine
def cosine_squared_cross_fade(samples):
    return np.vstack(
        [np.cos(np.pi / 2 * samples) ** 2, 1 - np.cos(np.pi / 2 * samples) ** 2]
    ).T


def generate_stateful_gain_results():
    num = 5
    samples = np.arange(num) / num

    signal_level = 1

    fading_values = raised_cosine_cross_fade(samples)

    base_name = "StatefulSmoothedGain_GainDirection"

    section = "FadeIn"
    result = signal_level * np.sum(fading_values * np.array([0, 1]), axis=1)
    np.savetxt(script_dir / f"{base_name}-{section}.txt", [result], delimiter="\n")

    section = "FadeInNegative"
    result = signal_level * np.sum(fading_values * np.array([0, -1]), axis=1)
    np.savetxt(script_dir / f"{base_name}-{section}.txt", [result], delimiter="\n")

    section = "FadeOut"
    result = signal_level * np.sum(fading_values * np.array([1, 0]), axis=1)
    np.savetxt(script_dir / f"{base_name}-{section}.txt", [result], delimiter="\n")

    section = "FadeOutNegative"
    result = signal_level * np.sum(fading_values * np.array([-1, 0]), axis=1)
    np.savetxt(script_dir / f"{base_name}-{section}.txt", [result], delimiter="\n")

    section = "FadeBetween"
    result = signal_level * np.sum(fading_values * np.array([1, 0.8]), axis=1)
    np.savetxt(script_dir / f"{base_name}-{section}.txt", [result], delimiter="\n")

    section = "FadeBetweenLargeNegative"
    result = signal_level * np.sum(fading_values * np.array([0.9, -0.8]), axis=1)
    np.savetxt(script_dir / f"{base_name}-{section}.txt", [result], delimiter="\n")


def generate_stateful_gain_process_results():
    num = 10
    samples = np.arange(num) / num

    signal_level = 2

    fading_values = raised_cosine_cross_fade(samples)

    base_name = "StatefulSmoothedGain_Process"

    result = signal_level * np.sum(fading_values * np.array([0, 1]), axis=1)
    np.savetxt(script_dir / f"{base_name}.txt", [result], delimiter="\n")

def generate_fading_results():
    num = 10
    samples = np.arange(num) / num
    base_name = "FadingFunctions"

    fading_function = ["linear_cross_fade", "exponential_cross_fade", "logarithmic_cross_fade", "square_root_cross_fade", "cubic_cross_fade", "quadratic_cross_fade", "cosine_cross_fade", "cosine_squared_cross_fade"]

    for func in fading_function:
        result = globals()[func](samples)
        # set all values to 0 that are smaller than 1e-10
        result = np.where(result < 1e-10, 0, result)

        np.savetxt(script_dir / f"{base_name}-{func}-fadeout.txt", result[:,0], delimiter="\n")
        np.savetxt(script_dir / f"{base_name}-{func}-fadein.txt", result[:,1], delimiter="\n")


if __name__ == "__main__":
    generate_stateful_gain_results()
    generate_stateful_gain_process_results()
    generate_fading_results()
