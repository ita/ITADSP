#include "test_utils.h"
#include <ITAStatefulSmoothedGain.h>
#include <ITASampleBuffer.h>
#include <vector>


ITASampleBuffer GetConstantSignal( int nSamples, float signalValue )
{
	auto audioBlock = ITASampleBuffer( nSamples );
	for (int idx=0; idx < nSamples; idx++)
		audioBlock[idx] = signalValue;
	
	return audioBlock;
}
void RequireSameSignal( const ITASampleBuffer& audioBlock, const std::vector<float>& targetSignal, float eps = 20 * std::numeric_limits<float>::epsilon() )
{
	std::vector<float> input;
	input.reserve( audioBlock.GetLength() );
	for (int idx=0; idx < audioBlock.GetLength(); idx++)
	{
		input.push_back( audioBlock[idx] );
	}

	REQUIRE_THAT( input, Catch::Matchers::Approx( targetSignal ).epsilon( eps ) );
}

TEST_CASE( "ITADSP::StatefulSmoothedGain::Process", "[ITADSP][StatefulSmoothedGain]" )
{
	std::string mode = GENERATE("InstantGainChange", "ConstantGain", "LinearInterpolation");
	const auto fadingMode = GENERATE( ITA::DSP::StatefulSmoothedGain::FadingMode::LINEAR, ITA::DSP::StatefulSmoothedGain::FadingMode::COSINE_SQUARE );
	bool bForceInstantGainChange = mode == "InstantGainChange";
	bool bConstantGain = mode == "ConstantGain";

	const double startGain = bConstantGain ? 1.0 : 0.0;
	const double endGain = 1.0;
	const int nSamples = 10;
	const float signalValue = 2.0f;

	std::vector<float> targetSignal( nSamples, signalValue );
	if (!bForceInstantGainChange && !bConstantGain)
	{
		if( fadingMode == ITA::DSP::StatefulSmoothedGain::FadingMode::LINEAR )
		{
			targetSignal = { 0.0f, 0.2f, 0.4f, 0.6f, 0.8f, 1.0f, 1.2f, 1.4f, 1.6f, 1.8f };
		}
		else
		{
			targetSignal = read_result_file<float>("StatefulSmoothedGain_Process.txt");
		}
	}
	
	ITASampleBuffer inputBlock  = GetConstantSignal( nSamples, signalValue );
	ITASampleBuffer outputBlock = ITASampleBuffer( nSamples );

	auto smoothedGain = ITA::DSP::StatefulSmoothedGain( startGain, fadingMode );
	smoothedGain.SetGain( endGain, bForceInstantGainChange );

	SECTION("InputToOutput")
	{
		smoothedGain.Process( inputBlock, outputBlock );
		RequireSameSignal( outputBlock, targetSignal );
	}
	SECTION("InputOverwrite")
	{
		smoothedGain.Process( inputBlock );
		RequireSameSignal( inputBlock, targetSignal );
	}
	SECTION("InputToInput")
	{
		smoothedGain.Process( inputBlock, inputBlock );
		RequireSameSignal( inputBlock, targetSignal );
	}
}

TEST_CASE( "ITADSP::StatefulSmoothedGain::GainDirection", "[ITADSP][StatefulSmoothedGain]" )
{
	const int nSamples = 5;

	ITASampleBuffer inputBlock  = GetConstantSignal( nSamples, 1.f );
	ITASampleBuffer outputBlock = ITASampleBuffer( nSamples );

	auto smoothedGain = ITA::DSP::StatefulSmoothedGain( 0.0 );

	SECTION( "FadeIn" )
	{
		smoothedGain.SetGain( 1.0 );
		smoothedGain.Process( inputBlock, outputBlock );
		RequireSameSignal( outputBlock, read_result_file<float>("StatefulSmoothedGain_GainDirection-FadeIn.txt") );
	}

	SECTION( "FadeIn Negative" )
	{
		smoothedGain.SetGain( -1.0 );
		smoothedGain.Process( inputBlock, outputBlock );
		RequireSameSignal( outputBlock, read_result_file<float>("StatefulSmoothedGain_GainDirection-FadeInNegative.txt") );
	}

	SECTION( "FadeOut" )
	{
		smoothedGain.SetGain( 1.0, true );
		smoothedGain.SetGain( 0.0 );
		smoothedGain.Process( inputBlock, outputBlock );
		RequireSameSignal( outputBlock, read_result_file<float>("StatefulSmoothedGain_GainDirection-FadeOut.txt") );
	}

	SECTION( "FadeOut Negative" )
	{
		smoothedGain.SetGain( -1.0, true );
		smoothedGain.SetGain( 0.0 );
		smoothedGain.Process( inputBlock, outputBlock );
		RequireSameSignal( outputBlock, read_result_file<float>("StatefulSmoothedGain_GainDirection-FadeOutNegative.txt") );
	}

	SECTION( "FadeBetween")
	{
		smoothedGain.SetGain( 1., true );
		smoothedGain.SetGain( 0.8 );
		smoothedGain.Process( inputBlock, outputBlock );
		RequireSameSignal( outputBlock, read_result_file<float>("StatefulSmoothedGain_GainDirection-FadeBetween.txt") );
	}

	SECTION( "FadeBetween Large, Negative" )
	{
		smoothedGain.SetGain( 0.9, true );
		smoothedGain.SetGain( -0.8 );
		smoothedGain.Process( inputBlock, outputBlock );
		RequireSameSignal( outputBlock, read_result_file<float>("StatefulSmoothedGain_GainDirection-FadeBetweenLargeNegative.txt") );
	}
}