/*
 * ----------------------------------------------------------------
 *
 *		ITA Base
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 *
 *
 */

#ifndef IW_ITA_DSP_UNIT_TEST_UTILS
#define IW_ITA_DSP_UNIT_TEST_UTILS

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_translate_exception.hpp>
#include <catch2/generators/catch_generators_all.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>
// #include <fakeit.hpp>
#include <ITAException.h>
#include <exception>
#include <fstream>
#include <limits>


CATCH_TRANSLATE_EXCEPTION( const ITAException& ex )
{
	return ex.ToString( );
}

template<typename T>
std::vector<T> read_result_file( std::string filename )
{
	filename = std::string( ITADSP_TEST_RESOURCE_DIR ) + "/" + filename;

	std::vector<T> result;
	std::ifstream file( filename, std::ios::binary );

	if( !file.is_open( ) )
	{
		throw std::runtime_error( "Could not open file: " + std::string( filename ) );
	}

	T value;
	while( file >> value )
	{
		result.push_back( value );
	}

	return result;
}

#endif // IW_ITA_DSP_UNIT_TEST_UTILS
