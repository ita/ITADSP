#include "ITAFadingFunctions.hpp"
#include "test_utils.h"

using namespace ITA::DSP::FadingFunction;


TEST_CASE( "ITADSP::FadingFunctions", "[ITADSP][FadingFunctions]" )
{
	auto function = GENERATE( FadingFunction::LINEAR, FadingFunction::COSINE, FadingFunction::COSINE_SQUARE, FadingFunction::QUADRATIC, FadingFunction::CUBIC,
	                          FadingFunction::SQUARE_ROOT, FadingFunction::EXPONENTIAL, FadingFunction::LOGARITHMIC );

	DYNAMIC_SECTION( "Fading function: " << static_cast<int>( function ) )
	{
		auto direction = GENERATE( FadingDirection::FADE_IN, FadingDirection::FADE_OUT );

		DYNAMIC_SECTION( "Direction: " << static_cast<int>( direction ) )
		{
			SECTION( "Correct start and end" )
			{
				const auto desired_start_value = direction == FadingDirection::FADE_IN ? 0.0f : 1.0f;
				const auto desired_end_value   = direction == FadingDirection::FADE_IN ? 1.0f : 0.0f;

				const auto actual_start_value = fading_func( 0.f, direction, function );
				const auto actual_end_value   = fading_func( 1.f, direction, function );

				REQUIRE_THAT( actual_start_value, Catch::Matchers::WithinRel( desired_start_value, 1e-6f ) );
				REQUIRE_THAT( actual_end_value, Catch::Matchers::WithinRel( desired_end_value, 1e-6f ) );
			}

			SECTION( "All values are between 0 and 1" )
			{
				const auto n_Samples = 100;

				float prev_value = 0.f;
				if( direction == FadingDirection::FADE_OUT )
				{
					prev_value = 1.f;
				}
				for( auto i = 0; i <= n_Samples; ++i )
				{
					const auto fading_value = static_cast<float>( i ) / static_cast<float>( n_Samples );

					const auto actual_value = fading_func( fading_value, direction, function );

					REQUIRE( actual_value <= 1.f );
					REQUIRE( actual_value >= 0.f );

					if( direction == FadingDirection::FADE_IN )
					{
						REQUIRE( actual_value >= prev_value );
					}
					else
					{
						REQUIRE( actual_value <= prev_value );
					}
					prev_value = actual_value;
				}
			}
		}
	}
}

TEST_CASE( "ITADSP::FadingFunctions::exact_results", "[ITADSP][FadingFunctions]" )
{
	std::unordered_map<FadingFunction, std::string> function_names   = { { FadingFunction::LINEAR, "linear_cross_fade" },
		                                                                 { FadingFunction::COSINE, "cosine_cross_fade" },
		                                                                 { FadingFunction::COSINE_SQUARE, "cosine_squared_cross_fade" },
		                                                                 { FadingFunction::QUADRATIC, "quadratic_cross_fade" },
		                                                                 { FadingFunction::CUBIC, "cubic_cross_fade" },
		                                                                 { FadingFunction::SQUARE_ROOT, "square_root_cross_fade" },
		                                                                 { FadingFunction::EXPONENTIAL, "exponential_cross_fade" },
		                                                                 { FadingFunction::LOGARITHMIC, "logarithmic_cross_fade" } };
	std::unordered_map<FadingDirection, std::string> direction_names = { { FadingDirection::FADE_IN, "fadein" }, { FadingDirection::FADE_OUT, "fadeout" } };

	auto eps        = 30 * std::numeric_limits<float>::epsilon( );
	int num_samples = 10;
	std::vector<float> input( num_samples );
	std::iota( input.begin( ), input.end( ), 0.f );
	std::transform( input.begin( ), input.end( ), input.begin( ), [num_samples]( float x ) { return x / num_samples; } );

	auto function = GENERATE( FadingFunction::LINEAR, FadingFunction::COSINE, FadingFunction::COSINE_SQUARE, FadingFunction::QUADRATIC, FadingFunction::CUBIC,
	                          FadingFunction::SQUARE_ROOT, FadingFunction::EXPONENTIAL, FadingFunction::LOGARITHMIC );

	DYNAMIC_SECTION( "Fading function: " << function_names[function] )
	{
		auto direction = GENERATE( FadingDirection::FADE_IN, FadingDirection::FADE_OUT );

		DYNAMIC_SECTION( "Direction: " << direction_names[direction] )
		{
			std::string file_name = "FadingFunctions-" + function_names[function] + "-" + direction_names[direction] + ".txt";

			std::vector<float> test_values;
			std::transform( input.begin( ), input.end( ), std::back_inserter( test_values ),
			                [function, direction]( float x ) { return fading_func( x, direction, function ); } );
			REQUIRE_THAT( test_values, Catch::Matchers::Approx( read_result_file<float>( file_name ) ).epsilon( eps ) );
		}
	}
}