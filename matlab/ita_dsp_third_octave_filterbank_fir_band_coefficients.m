function Hd = ita_dsp_third_octave_filterbank_fir_band_coefficients( Fs, Fc1, Fc2 )
%ITA_DSP_THIRD_OCTAVE_FILTERBANK_IIR_COEFFICIENTS Returns a discrete-time filter object.
%   Fs Sampling Frequency
%   Fc1 First Cutoff Frequency
%   Fc2 Second Cutoff Frequency

N   = 10;     % Order
d = fdesign.bandpass( 'Fst1,Fp1,Fp2,Fst2,Ast1,Ap,Ast2', 1/4, 3/8, 5/8, 6/8, 60, 1, 60, Fs );
Hd = design(d,'equiripple');
